# Sceme Test utils

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
Powered by [Taquito](https://tezostaquito.io/). 

Sceme Test utils runs the test scenarios on HangzhouNet.

You can freely edit src/acc.json to set your own [faucet account](https://teztnets.xyz/).

## Init

To install dependencies 
```
npm i
```

## Launch test UI

To launch UI 
```
npm start
```
