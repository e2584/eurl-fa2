import React from "react";
import './App.css';
import { TezosToolkit } from '@taquito/taquito';
import { InMemorySigner } from '@taquito/signer';
import { MichelsonMap } from '@taquito/taquito';
import { RpcClient } from '@taquito/rpc';
import { KeyStoreUtils } from 'conseiljs-softsigner';
import { encodeExpr, b58decode } from '@taquito/utils';

const owner_contract = require('./contracts/owner.json');
const admin_contract = require('./contracts/administrator.json');
const reserve_contract = require('./contracts/reserve.json');
const master_minter_contract = require('./contracts/master_minter.json');
const fees_contract = require('./contracts/fees_manager.json');
const eurl_contract = require('./contracts/eurl.json');

const client = new RpcClient('https://hangzhounet.api.tez.ie', 'NetXZSsxBpMQeAT');

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      network: 'hangzhounet',
      node: 'https://hangzhounet.api.tez.ie',
      explorer: 'https://hangzhou2net.tzkt.io/',
      success: {},
      fails: {},
      processing: false,
      error: null,
      info: null,
      complete: false,
      accounts: {
        new_admin: 
          {secret: '-',
          pkh: '-'},
        new_master_minter: 
          {secret: '-',
          pkh: '-'},
        signer_1: 
          {secret: '-',
          pkh: '-'},
        signer_2: 
          {secret: '-',
          pkh: '-'},
        signer_3: 
          {secret: '-',
          pkh: '-'},
        signer_4: 
          {secret: '-',
          pkh: '-'},
        alice: 
          {secret: '-',
          pkh: '-'},
        bob: 
          {secret: '-',
          pkh: '-'},
        carl: 
          {secret: '-',
          pkh: '-'},
        dana: 
          {secret: '-',
          pkh: '-'},
        eric: 
          {secret: '-',
          pkh: '-'},
        relay: 
          {secret: '-',
          pkh: '-'},
        fees_faucet: 
          {secret: '-',
          pkh: '-'},
        new_fees_faucet: 
          {secret: '-',
          pkh: '-'},
        delegated_minter: 
          {secret: '-',
          pkh: '-'},
      }
    };
  };

  componentDidMount = async() => {
    let accounts = this.state.accounts;
    for (var key of Object.keys(accounts)) {
      const mnemonic = KeyStoreUtils.generateMnemonic();
      const keystore = await KeyStoreUtils.restoreIdentityFromMnemonic(mnemonic, '');
      accounts[key].secret = keystore.secretKey;
      accounts[key].pkh = keystore.publicKeyHash;
    }
    this.setState({accounts: accounts})
  }

  startProcess = async() => {
    this.setState({processing: true});
    console.log(JSON.stringify(this.state.accounts));
    const Tezos = new TezosToolkit(this.state.node);
    let success = {}, fails = {}, operation = null, tzBalance = 0;
    
    // SET FAUCET (EDIT to SET your own wallet)
    Tezos.setProvider({ signer: await InMemorySigner.fromSecretKey("edsk3CGpzAAVAYYcbLMY1HNbB3dfDZokZQxed5rVEsstrS1MLGppEp") });

    // ORIGINATION
    const emptyMap = new MichelsonMap();
    // Originate Owner
    console.log('Originate Owner')
    operation = await Tezos.contract.originate({ code: owner_contract, storage: {
      authOps: ["create", "set_master_minter", "set_administrator", "update_metadata"], 
      openProposals: 0,
      proposals: emptyMap, 
      restriction: 3, 
      signers: [this.state.accounts.signer_1.pkh, this.state.accounts.signer_2.pkh, this.state.accounts.signer_3.pkh], 
    }});
    await operation.confirmation();
    success['pub_owner'] = operation.hash;
    const owner = await (await operation.contract()).address;
    this.setState({success: success});
    // Originate Admin
    operation = await Tezos.contract.originate({ code: admin_contract, storage: {
      authOps: ["set_fees_faucet", "set_fees_manager", 
        "set_lock", "set_rights_manager", "updateGasFee", "updateGaslessFee", "updateStorageFee", "updateThreshold"],
      limit: 2, 
      openProposals: 0,
      proposals: emptyMap, 
      restriction: 40, 
      signers: [this.state.accounts.signer_1.pkh, this.state.accounts.signer_2.pkh, this.state.accounts.signer_3.pkh], 
    }});
    await operation.confirmation();
    success['pub_admin'] = operation.hash;
    const admin = await (await operation.contract()).address;
    this.setState({success: success});
    // Originate Reserve
    operation = await Tezos.contract.originate({ code: reserve_contract, storage: {
      limit: 2, 
      openProposals: 0,
      proposals: emptyMap, 
      restriction: 120, 
      signers: [this.state.accounts.signer_1.pkh, this.state.accounts.signer_2.pkh, this.state.accounts.signer_3.pkh], 
    }});
    await operation.confirmation();
    success['pub_reserve'] = operation.hash;
    const reserve = await (await operation.contract()).address;
    this.setState({success: success});
    // Originate Master minter
    operation = await Tezos.contract.originate({ code: master_minter_contract, storage: {
      authOps: ["mint", "burn", "add_minter", "remove_minter", "update_allowance"],
      controllers: [this.state.accounts.signer_1.pkh, this.state.accounts.signer_2.pkh], 
      limit: 1, 
      openProposals: 0,
      operators: [this.state.accounts.signer_3.pkh, this.state.accounts.signer_4.pkh], 
      proposals: emptyMap, 
      restriction: 200, 
    }});
    await operation.confirmation();
    success['pub_master_minter'] = operation.hash;
    const master_minter = await (await operation.contract()).address;
    this.setState({success: success});
    // Originate Fees manager
    operation = await Tezos.contract.originate({ code: fees_contract, storage: {
      administrator: admin,
      gasFee: 2,
      gaslessFee: 2,
      storageFee: 140000,
      threshold: 30000,
    }});
    await operation.confirmation();
    success['pub_fees'] = operation.hash;
    const feesManager = await (await operation.contract()).address;
    this.setState({success: success});
    // Originate Eurl FA2 token
    const mintedMap = new MichelsonMap();
    const metadataMap = new MichelsonMap();
    const tokenMetadataMap = new MichelsonMap();
    const tokenInfoMap = new MichelsonMap();
    mintedMap.set('0','0');
    metadataMap.set('','697066733a2f2f516d6366476468623555747a6d696b50546d51594e64526e586642596b4a443155657a4267616a733138796e554e');
    tokenInfoMap.set('decimals', '36');
    tokenInfoMap.set('is_boolean_amount', '46616c7365');
    tokenInfoMap.set('is_transferable', '54727565');
    tokenInfoMap.set('name', '4c756768204575726f2070656767656420737461626c65636f696e');
    tokenInfoMap.set('should_prefer_symbol', '54727565');
    tokenInfoMap.set('symbol', '4555524c');
    tokenInfoMap.set('thumbnailUri', '697066733a2f2f516d63717359516e3870547851723350316459706759785161364751506d6f425453575138627075464575617165');
    tokenMetadataMap.set('0',{token_id: '0', token_info: tokenInfoMap});
    operation = await Tezos.contract.originate({ code: eurl_contract, storage: {
      administrator: admin,
      all_tokens: 1,
      default_expiry: 345600,
      fees_faucet: this.state.accounts.fees_faucet.pkh,
      fees_manager: feesManager,
      ledger: emptyMap,
      master_minted: mintedMap,
      master_minter: master_minter,
      max_expiry: 2678400,
      metadata: metadataMap,
      minters: emptyMap,
      operators: emptyMap,
      owner: owner,
      pause: false,
      permits: emptyMap,
      rights_manager: null,
      token_metadata: tokenMetadataMap,
      total_supply: mintedMap,
      transfer_operation: 0,
    }});
    await operation.confirmation();
    success['pub_eurl'] = operation.hash;
    const eurl = await (await operation.contract()).address;
    this.setState({success: success});
    this.setState({info: `EURL FA2 token published`});

    // PROVISION
    // Provisioning signer 1 with 5ꜩ
    operation = await Tezos.contract.transfer({ to: this.state.accounts.signer_1.pkh, amount: 5 });
    await operation.confirmation();
    success['signer_1-5'] = operation.hash;
    this.setState({success: success});
    tzBalance = await Tezos.tz.getBalance(this.state.accounts.signer_1.pkh);
    this.setState({info: `signer_1 balance: ${tzBalance.toNumber() / 1000000}ꜩ`});
    // Provisioning signer 2 with 5ꜩ
    operation = await Tezos.contract.transfer({ to: this.state.accounts.signer_2.pkh, amount: 5 });
    await operation.confirmation();
    success['signer_2-5'] = operation.hash;
    this.setState({success: success});
    tzBalance = await Tezos.tz.getBalance(this.state.accounts.signer_2.pkh);
    this.setState({info: `signer_2 balance: ${tzBalance.toNumber() / 1000000}ꜩ`});
    // Provisioning signer 3 with 5ꜩ
    operation = await Tezos.contract.transfer({ to: this.state.accounts.signer_3.pkh, amount: 5 });
    await operation.confirmation();
    success['signer_3-5'] = operation.hash;
    this.setState({success: success});
    tzBalance = await Tezos.tz.getBalance(this.state.accounts.signer_3.pkh);
    this.setState({info: `signer_3 balance: ${tzBalance.toNumber() / 1000000}ꜩ`});
    // Provisioning signer 4 with 5ꜩ
    operation = await Tezos.contract.transfer({ to: this.state.accounts.signer_4.pkh, amount: 5 });
    await operation.confirmation();
    success['signer_4-5'] = operation.hash;
    this.setState({success: success});
    tzBalance = await Tezos.tz.getBalance(this.state.accounts.signer_4.pkh);
    this.setState({info: `signer_4 balance: ${tzBalance.toNumber() / 1000000}ꜩ`});
    // Provisioning new admin with 5ꜩ
    operation = await Tezos.contract.transfer({ to: this.state.accounts.new_admin.pkh, amount: 5 });
    await operation.confirmation();
    success['new_admin-5'] = operation.hash;
    this.setState({success: success});
    tzBalance = await Tezos.tz.getBalance(this.state.accounts.new_admin.pkh);
    this.setState({info: `New admin balance: ${tzBalance.toNumber() / 1000000}ꜩ`});
    // Provisioning new master minter with 5ꜩ
    operation = await Tezos.contract.transfer({ to: this.state.accounts.new_master_minter.pkh, amount: 5 });
    await operation.confirmation();
    success['new_master_minter-5'] = operation.hash;
    this.setState({success: success});
    tzBalance = await Tezos.tz.getBalance(this.state.accounts.new_master_minter.pkh);
    this.setState({info: `New master minter balance: ${tzBalance.toNumber() / 1000000}ꜩ`});
    // Provisioning alice with 5ꜩ
    operation = await Tezos.contract.transfer({ to: this.state.accounts.alice.pkh, amount: 5 });
    await operation.confirmation();
    success['alice-5'] = operation.hash;
    this.setState({success: success});
    tzBalance = await Tezos.tz.getBalance(this.state.accounts.alice.pkh);
    this.setState({info: `Alice balance: ${tzBalance.toNumber() / 1000000}ꜩ`});
    // Provisioning bob with 5ꜩ
    operation = await Tezos.contract.transfer({ to: this.state.accounts.bob.pkh, amount: 5 });
    await operation.confirmation();
    success['bob-5'] = operation.hash;
    this.setState({success: success});
    tzBalance = await Tezos.tz.getBalance(this.state.accounts.bob.pkh);
    this.setState({info: `Bob balance: ${tzBalance.toNumber() / 1000000}ꜩ`});
    // Provisioning carl with 5ꜩ
    operation = await Tezos.contract.transfer({ to: this.state.accounts.carl.pkh, amount: 5 });
    await operation.confirmation();
    success['carl-5'] = operation.hash;
    this.setState({success: success});
    tzBalance = await Tezos.tz.getBalance(this.state.accounts.carl.pkh);
    this.setState({info: `Carl balance: ${tzBalance.toNumber() / 1000000}ꜩ`});
    // Provisioning dana with 5ꜩ
    operation = await Tezos.contract.transfer({ to: this.state.accounts.dana.pkh, amount: 5 });
    await operation.confirmation();
    success['dana-5'] = operation.hash;
    this.setState({success: success});
    tzBalance = await Tezos.tz.getBalance(this.state.accounts.dana.pkh);
    this.setState({info: `Dana balance: ${tzBalance.toNumber() / 1000000}ꜩ`});
    // Provisioning relay with 5ꜩ
    operation = await Tezos.contract.transfer({ to: this.state.accounts.relay.pkh, amount: 5 });
    await operation.confirmation();
    success['relay-5'] = operation.hash;
    this.setState({success: success});
    tzBalance = await Tezos.tz.getBalance(this.state.accounts.relay.pkh);
    this.setState({info: `Relay balance: ${tzBalance.toNumber() / 1000000}ꜩ`});
    // Provisioning fees faucet with 5ꜩ
    operation = await Tezos.contract.transfer({ to: this.state.accounts.fees_faucet.pkh, amount: 5 });
    await operation.confirmation();
    success['fees_faucet-5'] = operation.hash;
    this.setState({success: success});
    tzBalance = await Tezos.tz.getBalance(this.state.accounts.fees_faucet.pkh);
    this.setState({info: `Fees faucet balance: ${tzBalance.toNumber() / 1000000}ꜩ`});
    // Provisioning new fees faucet with 5ꜩ
    operation = await Tezos.contract.transfer({ to: this.state.accounts.new_fees_faucet.pkh, amount: 5 });
    await operation.confirmation();
    success['new_fees_faucet-5'] = operation.hash;
    this.setState({success: success});
    tzBalance = await Tezos.tz.getBalance(this.state.accounts.new_fees_faucet.pkh);
    this.setState({info: `new Fees faucet balance: ${tzBalance.toNumber() / 1000000}ꜩ`});
    // Provisioning new delegated minter with 5ꜩ
    operation = await Tezos.contract.transfer({ to: this.state.accounts.delegated_minter.pkh, amount: 5 });
    await operation.confirmation();
    success['delegated_minter-5'] = operation.hash;
    this.setState({success: success});
    tzBalance = await Tezos.tz.getBalance(this.state.accounts.delegated_minter.pkh);
    this.setState({info: `Delegated minter balance: ${tzBalance.toNumber() / 1000000}ꜩ`});

    // SETUP
    const TqtoEurl = new TezosToolkit(this.state.node);
    const contractEurl = await TqtoEurl.contract.at(eurl);
    console.log(Object.keys(contractEurl.methods));
    const TqtoAdmin = new TezosToolkit(this.state.node);
    const contractAdmin = await TqtoAdmin.contract.at(admin);
    const TqtoOwner = new TezosToolkit(this.state.node);
    const contractOwner = await TqtoOwner.contract.at(owner);
    const TqtoReserve = new TezosToolkit(this.state.node);
    const contractReserve = await TqtoReserve.contract.at(reserve);
    const TqtoMinter = new TezosToolkit(this.state.node);
    const contractMinter = await TqtoMinter.contract.at(master_minter);

    // MINTING
    // mint1: Bob tries to mint 10000EURL to reserve
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.bob.secret) });
    try{
      operation = await contractEurl.methods.mint(reserve, 10000000000, 0).send();
      await operation.confirmation();
      success['mint1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['mint1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // mint2: Master minter mints 10000EURL to reserve
    TqtoMinter.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_3.secret) });
    try{
      operation = await contractMinter.methods.createProposal(reserve, 10000000000, eurl, "mint", "mint2", 0).send();  
      await operation.confirmation();
      success['mint2-1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['mint2-1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    TqtoMinter.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_1.secret) });
    try{
      operation = await contractMinter.methods.accept("mint2").send();  
      await operation.confirmation();
      success['mint2-2'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['mint2-2'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // TRANSFERS

    // tx1: Bob tries to transfer from reserve to Alice
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.bob.secret) });
    try{
      operation = await contractEurl.methods.transfer([
        {
          from_: reserve,
          txs: [
            {
              to_: this.state.accounts.alice.pkh,
              token_id: 0,
              amount: 5000000000,
            },
          ],
        },
      ]).send();
      await operation.confirmation();
      success['tx1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['tx1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // tx2: Reserve tries to over transfer to Alice
    TqtoReserve.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_1.secret) });
    console.log(reserve)
    console.log(await TqtoReserve.signer.publicKeyHash());

    try{
      operation = await contractReserve.methods.createProposal(10001000000, eurl, reserve, "tx2", this.state.accounts.alice.pkh, 0).send();  
      await operation.confirmation();
      success['tx2-1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['tx2-1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    TqtoReserve.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_2.secret) });
    try{
      operation = await contractReserve.methods.accept("tx2").send();  
      await operation.confirmation();
      success['tx2-2'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['tx2-2'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // tx3: Reserve transfers 5000EURL to Alice
    TqtoReserve.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_1.secret) });
    try{
      operation = await contractReserve.methods.createProposal(5000000000, eurl, reserve, "tx3", this.state.accounts.alice.pkh, 0).send();  
      await operation.confirmation();
      success['tx3-1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['tx3-1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    TqtoReserve.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_2.secret) });
    try{
      operation = await contractReserve.methods.accept("tx3").send();  
      await operation.confirmation();
      success['tx3-2'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['tx3-2'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // tx4: Alice transfers 1000EURL to Bob & Carl
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.alice.secret) });
    try{
      operation = await contractEurl.methods.transfer([
        {
          from_: this.state.accounts.alice.pkh,
          txs: [
            {
              to_: this.state.accounts.bob.pkh,
              token_id: 0,
              amount: 1000000000,
            },
            {
              to_: this.state.accounts.carl.pkh,
              token_id: 0,
              amount: 1000000000,
            },
          ],
        },
      ]).send();
      await operation.confirmation();
      success['tx4'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['tx4'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // check1: Fees faucet balance is 1.4EURL?
    let storage = await contractEurl.storage();
    let balance = parseFloat((await storage.ledger.get({0: this.state.accounts.fees_faucet.pkh, 1: '0'})).balance);
    this.setState({info: `Fees faucet balance : ${balance? balance / 1000000 : 0}`});
    if(balance && (balance / 1000000 === 1.4)){
      success['check1'] = this.state.accounts.fees_faucet.pkh;
      this.setState({success: success});
    }else{
      fails['check1'] = `Fees faucet balance : ${balance? balance / 1000000 : 0}`;
      this.setState({fails: fails});
    }
    // tx5: Admin forces transfer of 500EURL from Carl to Alice
    TqtoAdmin.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_1.secret) });
    try{
      operation = await contractAdmin.methods.createTransferProposal(500000000, eurl, this.state.accounts.carl.pkh, "tx5", this.state.accounts.alice.pkh, 0).send();  
      await operation.confirmation();
      success['tx5-1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['tx5-1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    TqtoAdmin.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_2.secret) });
    try{
      operation = await contractAdmin.methods.accept("tx5").send();  
      await operation.confirmation();
      success['tx5-2'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['tx5-2'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // check2: Alice balance is 3499,6EURL?
    storage = await contractEurl.storage();
    balance = parseFloat((await storage.ledger.get({0: this.state.accounts.alice.pkh, 1: '0'})).balance);
    this.setState({info: `Alice's balance : ${balance? balance / 1000000 : 0}`});
    if(balance && (balance / 1000000 === 3499.6)){
      success['check2'] = this.state.accounts.alice.pkh;
      this.setState({success: success});
    }else{
      fails['check2'] = `Alice's balance : ${balance? balance / 1000000 : 0}`;
      this.setState({fails: fails});
    }
    // check3: Bob balance is 1000EURL?
    storage = await contractEurl.storage();
    balance = parseFloat((await storage.ledger.get({0: this.state.accounts.bob.pkh, 1: '0'})).balance);
    this.setState({info: `Bob's balance : ${balance? balance / 1000000 : 0}`});
    if(balance && (balance / 1000000 === 1000)){
      success['check3'] = this.state.accounts.bob.pkh;
      this.setState({success: success});
    }else{
      fails['check3'] = `Bob's balance : ${balance? balance / 1000000 : 0}`;
      this.setState({fails: fails});
    }
    // check4: Carl balance is 500EURL?
    storage = await contractEurl.storage();
    balance = parseFloat((await storage.ledger.get({0: this.state.accounts.carl.pkh, 1: '0'})).balance);
    this.setState({info: `Carl's balance : ${balance? balance / 1000000 : 0}`});
    if(balance && (balance / 1000000 === 500)){
      success['check4'] = this.state.accounts.carl.pkh;
      this.setState({success: success});
    }else{
      fails['check4'] = `Carl's balance : ${balance? balance / 1000000 : 0}`;
      this.setState({fails: fails});
    }
    // check5: Reserve balance is 4999EURL?
    storage = await contractEurl.storage();
    balance = parseFloat((await storage.ledger.get({0: reserve, 1: '0'})).balance);
    this.setState({info: `Reserve's balance : ${balance? balance / 1000000 : 0}`});
    if(balance && (balance / 1000000 === 4999)){
      success['check5'] = reserve;
      this.setState({success: success});
    }else{
      fails['check5'] = `Reserve's balance : ${balance? balance / 1000000 : 0}`;
      this.setState({fails: fails});
    }

    // OPERATORS
    // op1: Alice adds Dana as operator
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.alice.secret) });
    try{
      operation = await contractEurl.methods.update_operators([{'add_operator': {owner: this.state.accounts.alice.pkh, operator: this.state.accounts.dana.pkh, token_id: 0}}]).send();
      await operation.confirmation();
      success['op1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['op1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // op2: Bob adds Dana as operator
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.bob.secret) });
    try{
      operation = await contractEurl.methods.update_operators([{'add_operator': {owner: this.state.accounts.bob.pkh, operator: this.state.accounts.dana.pkh, token_id: 0}}]).send();
      await operation.confirmation();
      success['op2'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['op2'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // op3: Carl tries to transfer for Alice
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.carl.secret) });
    try{
      operation = await contractEurl.methods.transfer([
        {
          from_: this.state.accounts.alice.pkh,
          txs: [
            {
              to_: this.state.accounts.carl.pkh,
              token_id: 0,
              amount: 200000000,
            },
          ],
        },
      ]).send();
      await operation.confirmation();
      success['op3'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['op3'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // op4: Dana transfers for Alice and Bob 200EURL each
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.dana.secret) });
    try{
      operation = await contractEurl.methods.transfer([
        {
          from_: this.state.accounts.alice.pkh,
          txs: [
            {
              to_: this.state.accounts.carl.pkh,
              token_id: 0,
              amount: 100000000,
            },
          ],
        },
        {
          from_: this.state.accounts.bob.pkh,
          txs: [
            {
              to_: this.state.accounts.carl.pkh,
              token_id: 0,
              amount: 100000000,
            },
          ],
        },
      ]).send();
      await operation.confirmation();
      success['op4'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['op4'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // op5: Alice removes Dana as operator
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.alice.secret) });
    try{
      operation = await contractEurl.methods.update_operators([{'remove_operator': {owner: this.state.accounts.alice.pkh, operator: this.state.accounts.dana.pkh, token_id: 0}}]).send();
      await operation.confirmation();
      success['op5'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['op5'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // op6: Dana tries to transfer for Alice
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.dana.secret) });
    try{
      operation = await contractEurl.methods.transfer([
        {
          from_: this.state.accounts.alice.pkh,
          txs: [
            {
              to_: this.state.accounts.carl.pkh,
              token_id: 0,
              amount: 100000000,
            },
          ],
        },
      ]).send();
      await operation.confirmation();
      success['op6'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['op6'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }

    // LOCK / UNLOCK
    // lck1: Carl tries to lock Bob
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.carl.secret) });
    try{
      operation = await contractEurl.methods.set_lock(this.state.accounts.bob.pkh, true, 0).send();
      await operation.confirmation();
      success['lck1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['lck1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // lck2: Admin locks Bob
    TqtoAdmin.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_1.secret) });
    try{
      operation = await contractAdmin.methods.createAddressProposal(this.state.accounts.bob.pkh, eurl, "set_lock","lck2", true, 0).send();  

      await operation.confirmation();
      success['lck2-1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['lck2-1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    TqtoAdmin.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_2.secret) });
    try{
      operation = await contractAdmin.methods.accept("lck2").send();  
      await operation.confirmation();
      success['lck2-2'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['lck2-2'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // lck3: Bob tries to transfer to Alice
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.bob.secret) });
    try{
      operation = await contractEurl.methods.transfer([
        {
          from_: this.state.accounts.bob.pkh,
          txs: [
            {
              to_: this.state.accounts.alice.pkh,
              token_id: 0,
              amount: 200000000,
            },
          ],
        },
      ]).send();
      await operation.confirmation();
      success['lck3'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['lck3'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // lck4: Alice transfers to Bob 200EURL
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.alice.secret) });
    try{
      operation = await contractEurl.methods.transfer([
        {
          from_: this.state.accounts.alice.pkh,
          txs: [
            {
              to_: this.state.accounts.bob.pkh,
              token_id: 0,
              amount: 200000000,
            },
          ],
        },
      ]).send();
      await operation.confirmation();
      success['lck4'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['lck4'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // lck5: Admin forces transfers from Bob to Alice for 200EURL
    TqtoAdmin.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_1.secret) });
    try{
      operation = await contractAdmin.methods.createTransferProposal(200000000, eurl, this.state.accounts.bob.pkh, "lck5", this.state.accounts.alice.pkh, 0).send();  
      await operation.confirmation();
      success['lck5-1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['lck5-1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    TqtoAdmin.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_2.secret) });
    try{
      operation = await contractAdmin.methods.accept("lck5").send();  
      await operation.confirmation();
      success['lck5-2'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['lck5-2'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // lck6: Admin unlocks Bob
    TqtoAdmin.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_1.secret) });
    try{
      operation = await contractAdmin.methods.createAddressProposal(this.state.accounts.bob.pkh, eurl, "set_lock","lck6", false, 0).send();  

      await operation.confirmation();
      success['lck6-1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['lck6-1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    TqtoAdmin.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_2.secret) });
    try{
      operation = await contractAdmin.methods.accept("lck6").send();  
      await operation.confirmation();
      success['lck6-2'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['lck6-2'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // lck7: Bob transfers 100EURL to Alice
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.bob.secret) });
    try{
      operation = await contractEurl.methods.transfer([
        {
          from_: this.state.accounts.bob.pkh,
          txs: [
            {
              to_: this.state.accounts.alice.pkh,
              token_id: 0,
              amount: 100000000,
            },
          ],
        },
      ]).send();
      await operation.confirmation();
      success['lck7'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['lck7'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    
    // PAUSE
    // p1: Bob tries to pause transfers
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.bob.secret) });
    try{
      operation = await contractEurl.methods.set_pause(true).send();
      await operation.confirmation();
      success['p1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['p1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // p2: Admin pauses transfers
    TqtoAdmin.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_1.secret) });
    try{
      operation = await contractAdmin.methods.createPauseProposal(eurl, "p2", true, 0).send();  
      await operation.confirmation();
      success['p2-1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['p2-1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    TqtoAdmin.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_2.secret) });
    try{
      operation = await contractAdmin.methods.accept("p2").send();  
      await operation.confirmation();
      success['p2-2'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['p2-2'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // p3: Alice tries to transfer
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.alice.secret) });
    try{
      operation = await contractEurl.methods.transfer([
        {
          from_: this.state.accounts.alice.pkh,
          txs: [
            {
              to_: this.state.accounts.bob.pkh,
              token_id: 0,
              amount: 200000000,
            },
          ],
        },
      ]).send();
      await operation.confirmation();
      success['p3'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['p3'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // p4: Dana tries to transfer for Bob
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.dana.secret) });
    try{
      operation = await contractEurl.methods.transfer([
        {
          from_: this.state.accounts.bob.pkh,
          txs: [
            {
              to_: this.state.accounts.alice.pkh,
              token_id: 0,
              amount: 100000000,
            },
          ],
        },
      ]).send();
      await operation.confirmation();
      success['p4'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['p4'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // p5: Admin forces transfers from Alice to Bob for 200EURL
    TqtoAdmin.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_1.secret) });
    try{
      operation = await contractAdmin.methods.createTransferProposal(200000000, eurl, this.state.accounts.alice.pkh, "p5", this.state.accounts.bob.pkh, 0).send();  
      await operation.confirmation();
      success['p5-1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['p5-1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    TqtoAdmin.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_2.secret) });
    try{
      operation = await contractAdmin.methods.accept("p5").send();  
      await operation.confirmation();
      success['p5-2'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['p5-2'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // p6: Admin unpauses transfers
    TqtoAdmin.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_1.secret) });
    try{
      operation = await contractAdmin.methods.createPauseProposal(eurl, "p6", false, 0).send();  
      await operation.confirmation();
      success['p6-1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['p6-1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    TqtoAdmin.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_2.secret) });
    try{
      operation = await contractAdmin.methods.accept("p6").send();  
      await operation.confirmation();
      success['p6-2'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['p6-2'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // p7: Alice transfers 200EURL to Bob
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.alice.secret) });
    try{
      operation = await contractEurl.methods.transfer([
        {
          from_: this.state.accounts.alice.pkh,
          txs: [
            {
              to_: this.state.accounts.bob.pkh,
              token_id: 0,
              amount: 200000000,
            },
          ],
        },
      ]).send();
      await operation.confirmation();
      success['p7'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['p7'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }

    // MINTERS
    // m1: Bob tries to add a delegated minter
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.bob.secret) });
    try{
      operation = await contractEurl.methods.add_minter(this.state.accounts.delegated_minter.pkh, 1000000000, 0).send();
      await operation.confirmation();
      success['m1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['m1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // m2: Master minter adds a delegated minter with an allowance of 1000EURL
    TqtoMinter.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_4.secret) });
    try{
      operation = await contractMinter.methods.createProposal(this.state.accounts.delegated_minter.pkh, 1000000000, eurl, "add_minter", "m2", 0).send();  
      await operation.confirmation();
      success['m2-1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['m2-1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    TqtoMinter.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_1.secret) });
    try{
      operation = await contractMinter.methods.accept("m2").send();  
      await operation.confirmation();
      success['m2-2'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['m2-2'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // m3: Delegated minter tries over mint
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.delegated_minter.secret) });
    try{
      operation = await contractEurl.methods.mint(this.state.accounts.carl.pkh, 1010000000, 0).send();
      await operation.confirmation();
      success['m3'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['m3'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // m4: Delegated minter mints 800EURL to Carl
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.delegated_minter.secret) });
    try{
      operation = await contractEurl.methods.mint(this.state.accounts.carl.pkh, 800000000, 0).send();
      await operation.confirmation();
      success['m4'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['m4'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // m5: Delegated minter tries to overmint 201EURL
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.delegated_minter.secret) });
    try{
      operation = await contractEurl.methods.mint(this.state.accounts.carl.pkh, 201000000, 0).send();
      await operation.confirmation();
      success['m5'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['m5'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // m6: Delegated minter tries to update his allowance
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.delegated_minter.secret) });
    try{
      operation = await contractEurl.methods.update_allowance(this.state.accounts.delegated_minter.pkh, 500000000, 0).send();
      await operation.confirmation();
      success['m6'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['m6'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // m7: Master minter set delegated minter's allowance to 500EURL
    TqtoMinter.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_4.secret) });
    try{
      operation = await contractMinter.methods.createProposal(this.state.accounts.delegated_minter.pkh, 500000000, eurl, "update_allowance", "m7", 0).send();  
      await operation.confirmation();
      success['m7-1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['m7-1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    TqtoMinter.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_1.secret) });
    try{
      operation = await contractMinter.methods.accept("m7").send();  
      await operation.confirmation();
      success['m7-2'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['m7-2'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // m8: Delegated minter mints 400EURL to Carl
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.delegated_minter.secret) });
    try{
      operation = await contractEurl.methods.mint(this.state.accounts.carl.pkh, 400000000, 0).send();
      await operation.confirmation();
      success['m8'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['m8'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // m9: Delegated minter tries to overburn
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.delegated_minter.secret) });
    try{
      operation = await contractEurl.methods.burn(this.state.accounts.carl.pkh, 1600000000, 0).send();
      await operation.confirmation();
      success['m9'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['m9'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // m10: Delegated minter burns 1200EURL
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.delegated_minter.secret) });
    try{
      operation = await contractEurl.methods.burn(this.state.accounts.carl.pkh, 1200000000, 0).send();
      await operation.confirmation();
      success['m10'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['m10'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // m11: Master minter tries to overburn Carl
    TqtoMinter.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_4.secret) });
    try{
      operation = await contractMinter.methods.createProposal(this.state.accounts.carl.pkh, 4000000000, eurl, "burn", "m11", 0).send();  
      await operation.confirmation();
      success['m11-1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['m11-1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    TqtoMinter.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_1.secret) });
    try{
      operation = await contractMinter.methods.accept("m11").send();  
      await operation.confirmation();
      success['m11-2'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['m11-2'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // m12: Master minter burns 100EURL from Carl
    TqtoMinter.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_4.secret) });
    try{
      operation = await contractMinter.methods.createProposal(this.state.accounts.carl.pkh, 100000000, eurl, "burn", "m12", 0).send();  
      await operation.confirmation();
      success['m12-1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['m12-1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    TqtoMinter.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_1.secret) });
    try{
      operation = await contractMinter.methods.accept("m12").send();  
      await operation.confirmation();
      success['m12-2'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['m12-2'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // m13: Master minter removes delegated minter
    TqtoMinter.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_4.secret) });
    try{
      operation = await contractMinter.methods.createProposal(this.state.accounts.delegated_minter.pkh, 0, eurl, "remove_minter", "m13", 0).send();  
      await operation.confirmation();
      success['m13-1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['m13-1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    TqtoMinter.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_1.secret) });
    try{
      operation = await contractMinter.methods.accept("m13").send();  
      await operation.confirmation();
      success['m13-2'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['m13-2'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // m14: Delegated minter tries to mint 50EURL
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.delegated_minter.secret) });
    try{
      operation = await contractEurl.methods.mint(this.state.accounts.carl.pkh, 50000000, 0).send();
      await operation.confirmation();
      success['m14'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['m14'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }

    // OWNER
    // own1: Bob tries to create a new token
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.bob.secret) });
    const new_tokenInfoMap = new MichelsonMap();
    new_tokenInfoMap.set('decimals', '36');
    new_tokenInfoMap.set('is_boolean_amount', '46616c7365');
    new_tokenInfoMap.set('is_transferable', '54727565');
    new_tokenInfoMap.set('name', '536f6d65206f74686572206575726f20636f696e');
    new_tokenInfoMap.set('should_prefer_symbol', '54727565');
    new_tokenInfoMap.set('symbol', '4d455552');
    new_tokenInfoMap.set('thumbnailUri', '697066733a2f2f516d63717359516e3870547851723350316459706759785161364751506d6f425453575138627075464575617165');
    try{
      operation = await contractEurl.methods.create(new_tokenInfoMap, 1).send();
      await operation.confirmation();
      success['own1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['own1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // own2: Owner creates new Token MEUR
    TqtoOwner.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_2.secret) });
    try{
      operation = await contractOwner.methods.manageToken(eurl, new_tokenInfoMap, "create", "own2", 1).send();  
      await operation.confirmation();
      success['own2-1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['own2-1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    TqtoOwner.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_1.secret) });
    try{
      operation = await contractOwner.methods.accept("own2").send();  
      await operation.confirmation();
      success['own2-2'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['own2-2'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    TqtoOwner.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_3.secret) });
    try{
      operation = await contractOwner.methods.accept("own2").send();  
      await operation.confirmation();
      success['own2-3'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['own2-3'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // own3: Master minter mints 1000MEUR
    TqtoMinter.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_4.secret) });
    try{
      operation = await contractMinter.methods.createProposal(this.state.accounts.alice.pkh, 1000000000, eurl, "mint", "own3", 1).send();  
      await operation.confirmation();
      success['own3-1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['own3-1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    TqtoMinter.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_1.secret) });
    try{
      operation = await contractMinter.methods.accept("own3").send();  
      await operation.confirmation();
      success['own3-2'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['own3-2'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // own4: Alice transfers 500MEUR to Bob
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.alice.secret) });
    try{
      operation = await contractEurl.methods.transfer([
        {
          from_: this.state.accounts.alice.pkh,
          txs: [
            {
              to_: this.state.accounts.bob.pkh,
              token_id: 1,
              amount: 500000000,
            },
          ],
        },
      ]).send();
      await operation.confirmation();
      success['own4'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['own4'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // own5: Bob tries to set new master minter
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.bob.secret) });
    try{
      operation = await contractEurl.methods.set_master_minter(this.state.accounts.new_master_minter.pkh).send();
      await operation.confirmation();
      success['own5'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['own5'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // own6: Owner sets new master minter
    TqtoOwner.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_2.secret) });
    try{
      operation = await contractOwner.methods.createProposal(this.state.accounts.new_master_minter.pkh, eurl, 'set_master_minter', 'own6').send();  
      await operation.confirmation();
      success['own6-1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['own6-1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    TqtoOwner.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_1.secret) });
    try{
      operation = await contractOwner.methods.accept("own6").send();  
      await operation.confirmation();
      success['own6-2'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['own6-2'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    TqtoOwner.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_3.secret) });
    try{
      operation = await contractOwner.methods.accept("own6").send();  
      await operation.confirmation();
      success['own6-3'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['own6-3'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // own7: Master minter tries to mint 1000EURL
    TqtoMinter.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_4.secret) });
    try{
      operation = await contractMinter.methods.createProposal(this.state.accounts.alice.pkh, 1000000000, eurl, "mint", "own7", 0).send();  
      await operation.confirmation();
      success['own7-1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['own7-1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    TqtoMinter.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_1.secret) });
    try{
      operation = await contractMinter.methods.accept("own7").send();  
      await operation.confirmation();
      success['own7-2'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['own7-2'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // own8: New master minter mints 1000EURL to Alice
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.new_master_minter.secret) });
    try{
      operation = await contractEurl.methods.mint(this.state.accounts.alice.pkh, 1000000000, 0).send();
      await operation.confirmation();
      success['own8'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['own8'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // own9: New master minter tries to change admin
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.new_master_minter.secret) });
    try{
      operation = await contractEurl.methods.set_administrator(this.state.accounts.new_admin.pkh).send();
      await operation.confirmation();
      success['own9'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['own9'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // own10: Owner set new administrator
    TqtoOwner.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_2.secret) });
    try{
      operation = await contractOwner.methods.createProposal(this.state.accounts.new_admin.pkh, eurl, 'set_administrator', 'own10').send();  
      await operation.confirmation();
      success['own10-1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['own10-1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    TqtoOwner.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_1.secret) });
    try{
      operation = await contractOwner.methods.accept("own10").send();  
      await operation.confirmation();
      success['own10-2'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['own10-2'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    TqtoOwner.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_3.secret) });
    try{
      operation = await contractOwner.methods.accept("own10").send();  
      await operation.confirmation();
      success['own10-3'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['own10-3'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // own11: Admin tries set new fees manager
    TqtoAdmin.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_1.secret) });
    try{
      operation = await contractAdmin.methods.createAddressProposal("KT1Nu1daXTArSfvYDBM9wN4eZ7wGNA19x9wP", eurl, "set_fees_manager","own11", true, 0).send();  

      await operation.confirmation();
      success['own11-1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['own11-1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    TqtoAdmin.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_2.secret) });
    try{
      operation = await contractAdmin.methods.accept("own11").send();  
      await operation.confirmation();
      success['own11-2'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['own11-2'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    } 
    // own12: Owner updates Token MEUR to KEUR
    const new_meta = new MichelsonMap();
    new_meta.set('decimals', '36');
    new_meta.set('is_boolean_amount', '46616c7365');
    new_meta.set('is_transferable', '54727565');
    new_meta.set('name', '536f6d65206f74686572206575726f20636f696e');
    new_meta.set('should_prefer_symbol', '54727565');
    new_meta.set('symbol', '4b455552');
    new_meta.set('thumbnailUri', '697066733a2f2f516d63717359516e3870547851723350316459706759785161364751506d6f425453575138627075464575617165');
    TqtoOwner.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_2.secret) });
    try{
      operation = await contractOwner.methods.manageToken(eurl, new_meta, "update_metadata", "own12", 1).send();  
      await operation.confirmation();
      success['own12-1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['own12-1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    TqtoOwner.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_1.secret) });
    try{
      operation = await contractOwner.methods.accept("own12").send();  
      await operation.confirmation();
      success['own12-2'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['own12-2'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    TqtoOwner.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.signer_3.secret) });
    try{
      operation = await contractOwner.methods.accept("own12").send();  
      await operation.confirmation();
      success['own12-3'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['own12-3'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // Originate Fees manager
    operation = await Tezos.contract.originate({ code: fees_contract, storage: {
      administrator: this.state.accounts.new_admin.pkh,
      gasFee: 1,
      gaslessFee: 5,
      storageFee: 200000,
      threshold: 30000,
    }});
    await operation.confirmation();
    success['pub_new_fees'] = operation.hash;
    const newfeesManager = await (await operation.contract()).address;
    this.setState({success: success});
    // own13: New administrator sets new fees manager
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.new_admin.secret) });
    try{
      operation = await contractEurl.methods.set_fees_manager(newfeesManager).send();
      await operation.confirmation();
      success['own13'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['own13'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }

    // GASLESS TRANSFERS
    // gas1: Relay tries to broadcast gasless transfer with wrong signature
    let packedData = await client.packData(
      {
        "data":{
           "prim":"Pair",
           "args":[
              {
                 "string": eurl
              },
              {
                 "prim":"Pair",
                 "args":[
                    {
                       "int":"0"
                    },
                    {
                       "prim":"Pair",
                       "args":[
                          {
                             "string": this.state.accounts.alice.pkh
                          },
                          [
                             {
                                "prim":"Pair",
                                "args":[
                                   {
                                      "string": this.state.accounts.bob.pkh
                                   },
                                   {
                                      "prim":"Pair",
                                      "args":[
                                         {
                                            "int":"0"
                                         },
                                         {
                                            "int":"5000000"
                                         }
                                      ]
                                   }
                                ]
                             },
                             {
                                "prim":"Pair",
                                "args":[
                                   {
                                      "string": this.state.accounts.carl.pkh
                                   },
                                   {
                                      "prim":"Pair",
                                      "args":[
                                         {
                                            "int":"0"
                                         },
                                         {
                                            "int":"4000000"
                                         }
                                      ]
                                   }
                                ]
                             }
                          ]
                       ]
                    }
                 ]
              }
           ]
        },
        "type":{
           "prim":"pair",
           "args":[
              {
                 "prim":"address"
              },
              {
                 "prim":"pair",
                 "args":[
                    {
                       "prim":"nat"
                    },
                    {
                       "prim":"pair",
                       "args":[
                          {
                             "prim":"address"
                          },
                          {
                             "prim":"list",
                             "args":[
                                {
                                   "prim":"pair",
                                   "args":[
                                      {
                                         "prim":"address"
                                      },
                                      {
                                         "prim":"nat"
                                      },
                                      {
                                         "prim":"nat"
                                      }
                                   ]
                                }
                             ]
                          }
                       ]
                    }
                 ]
              }
           ]
        }
     }
    );
    let signer = new InMemorySigner(this.state.accounts.alice.secret);
    let txs = [
      {
        to_: this.state.accounts.bob.pkh,
        token_id: 0,
        amount: 5000000,
      },
      {
        to_: this.state.accounts.carl.pkh,
        token_id: 0,
        amount: 4000000,
      },
    ];
    let sig = await signer.sign(packedData.packed);
    let transfers = [{
      from_: "edpkvThfdv8Efh1MuqSTUk5EnUFCTjqN6kXDCNXpQ8udN3cKRhNDr2", 
      txs: txs, 
      sig: sig.prefixSig
    }]
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.relay.secret) });
    try{
      operation = await contractEurl.methods.transfer_gasless(true, false, transfers).send();
      await operation.confirmation();
      success['gas1'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['gas1'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // gas2: Relay broadcasts gasless transfer from Alice to Bob & Carl for 5 and 4 EURL respectively
    transfers = [{
      from_: await signer.publicKey(), 
      txs: txs, 
      sig: sig.prefixSig
    }]
    console.log(JSON.stringify(transfers))
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.relay.secret) });
    try{
      console.log(packedData.packed)
      console.log(await signer.publicKey())
      console.log(sig.prefixSig)
      operation = await contractEurl.methods.transfer_gasless(true, false, transfers).send();
      await operation.confirmation();
      success['gas2'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['gas2'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // gas3: Relay tries to broadcast the same transfer
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.relay.secret) });
    try{
      operation = await contractEurl.methods.transfer_gasless(true, false, transfers).send();
      await operation.confirmation();
      success['gas3'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['gas3'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // gas4: Relay tries to broadcast a gasless transfer with over funds
    packedData = await client.packData(
      {
        "data":{
           "prim":"Pair",
           "args":[
              {
                 "string": eurl
              },
              {
                 "prim":"Pair",
                 "args":[
                    {
                       "int":"1"
                    },
                    {
                       "prim":"Pair",
                       "args":[
                          {
                             "string": this.state.accounts.alice.pkh
                          },
                          [
                             {
                                "prim":"Pair",
                                "args":[
                                   {
                                      "string": this.state.accounts.bob.pkh
                                   },
                                   {
                                      "prim":"Pair",
                                      "args":[
                                         {
                                            "int":"0"
                                         },
                                         {
                                            "int":"10000000000"
                                         }
                                      ]
                                   }
                                ]
                             }
                          ]
                       ]
                    }
                 ]
              }
           ]
        },
        "type":{
           "prim":"pair",
           "args":[
              {
                 "prim":"address"
              },
              {
                 "prim":"pair",
                 "args":[
                    {
                       "prim":"nat"
                    },
                    {
                       "prim":"pair",
                       "args":[
                          {
                             "prim":"address"
                          },
                          {
                             "prim":"list",
                             "args":[
                                {
                                   "prim":"pair",
                                   "args":[
                                      {
                                         "prim":"address"
                                      },
                                      {
                                         "prim":"nat"
                                      },
                                      {
                                         "prim":"nat"
                                      }
                                   ]
                                }
                             ]
                          }
                       ]
                    }
                 ]
              }
           ]
        }
     }
    );
    signer = new InMemorySigner(this.state.accounts.alice.secret);
    txs = [
      {
        to_: this.state.accounts.bob.pkh,
        token_id: 0,
        amount: 10000000000,
      }
    ];
    sig = await signer.sign(packedData.packed);
    transfers = [{
      from_: await signer.publicKey(), 
      txs: txs, 
      sig: sig.prefixSig
    }]
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.relay.secret) });
    try{
      operation = await contractEurl.methods.transfer_gasless(true, false, transfers).send();
      await operation.confirmation();
      success['gas4'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['gas4'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // gas5: Relay broadcasts gasless transfer from Alice to Eric (new account) for 100EURL
    packedData = await client.packData(
      {
        "data":{
           "prim":"Pair",
           "args":[
              {
                 "string": eurl
              },
              {
                 "prim":"Pair",
                 "args":[
                    {
                       "int":"1"
                    },
                    {
                       "prim":"Pair",
                       "args":[
                          {
                             "string": this.state.accounts.alice.pkh
                          },
                          [
                             {
                                "prim":"Pair",
                                "args":[
                                   {
                                      "string": this.state.accounts.eric.pkh
                                   },
                                   {
                                      "prim":"Pair",
                                      "args":[
                                         {
                                            "int":"0"
                                         },
                                         {
                                            "int":"100000000"
                                         }
                                      ]
                                   }
                                ]
                             }
                          ]
                       ]
                    }
                 ]
              }
           ]
        },
        "type":{
           "prim":"pair",
           "args":[
              {
                 "prim":"address"
              },
              {
                 "prim":"pair",
                 "args":[
                    {
                       "prim":"nat"
                    },
                    {
                       "prim":"pair",
                       "args":[
                          {
                             "prim":"address"
                          },
                          {
                             "prim":"list",
                             "args":[
                                {
                                   "prim":"pair",
                                   "args":[
                                      {
                                         "prim":"address"
                                      },
                                      {
                                         "prim":"nat"
                                      },
                                      {
                                         "prim":"nat"
                                      }
                                   ]
                                }
                             ]
                          }
                       ]
                    }
                 ]
              }
           ]
        }
     }
    );
    signer = new InMemorySigner(this.state.accounts.alice.secret);
    txs = [
      {
        to_: this.state.accounts.eric.pkh,
        token_id: 0,
        amount: 100000000,
      }
    ];
    sig = await signer.sign(packedData.packed);
    transfers = [{
      from_: await signer.publicKey(), 
      txs: txs, 
      sig: sig.prefixSig
    }]
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.relay.secret) });
    try{
      operation = await contractEurl.methods.transfer_gasless(true, false, transfers).send();
      await operation.confirmation();
      success['gas5'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['gas5'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // gas6: Relay tries to gasless transfer from Alice to Eric (new account) for 100EURL supporting fees
    packedData = await client.packData(
      {
        "data":{
           "prim":"Pair",
           "args":[
              {
                 "string": eurl
              },
              {
                 "prim":"Pair",
                 "args":[
                    {
                       "int":"2"
                    },
                    {
                       "prim":"Pair",
                       "args":[
                          {
                             "string": this.state.accounts.alice.pkh
                          },
                          [
                             {
                                "prim":"Pair",
                                "args":[
                                   {
                                      "string": this.state.accounts.eric.pkh
                                   },
                                   {
                                      "prim":"Pair",
                                      "args":[
                                         {
                                            "int":"0"
                                         },
                                         {
                                            "int":"100000000"
                                         }
                                      ]
                                   }
                                ]
                             }
                          ]
                       ]
                    }
                 ]
              }
           ]
        },
        "type":{
           "prim":"pair",
           "args":[
              {
                 "prim":"address"
              },
              {
                 "prim":"pair",
                 "args":[
                    {
                       "prim":"nat"
                    },
                    {
                       "prim":"pair",
                       "args":[
                          {
                             "prim":"address"
                          },
                          {
                             "prim":"list",
                             "args":[
                                {
                                   "prim":"pair",
                                   "args":[
                                      {
                                         "prim":"address"
                                      },
                                      {
                                         "prim":"nat"
                                      },
                                      {
                                         "prim":"nat"
                                      }
                                   ]
                                }
                             ]
                          }
                       ]
                    }
                 ]
              }
           ]
        }
     }
    );
    signer = new InMemorySigner(this.state.accounts.alice.secret);
    txs = [
      {
        to_: this.state.accounts.eric.pkh,
        token_id: 0,
        amount: 100000000,
      }
    ];
    sig = await signer.sign(packedData.packed);
    transfers = [{
      from_: await signer.publicKey(), 
      txs: txs, 
      sig: sig.prefixSig
    }]
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.relay.secret) });
    try{
      operation = await contractEurl.methods.transfer_gasless(false, true, transfers).send();
      await operation.confirmation();
      success['gas6'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['gas6'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // gas7: New master minter mints 10EURL to Relay
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.new_master_minter.secret) });
    try{
      operation = await contractEurl.methods.mint(this.state.accounts.relay.pkh, 10000000, 0).send();
      await operation.confirmation();
      success['gas7'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['gas7'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }
    // gas8: Relay broadcast gasless transfer from Alice to Eric for 100EURL supporting fees
    TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.relay.secret) });
    try{
      operation = await contractEurl.methods.transfer_gasless(false, true, transfers).send();
      await operation.confirmation();
      success['gas8'] = operation.hash;
      this.setState({success: success});
    }catch(e){
      fails['gas8'] = e.message;
      this.setState({fails: fails});
      this.setState({info: e.message});
    }

    // PERMITS
    // permit01: Relay set a permit from Alice, for a transfer of 20 to Bob
    packedData = await client.packData(
      { "data":
          { "prim": "Pair",
              "args":
              [ { "int": "20000000" },
                { "prim": "Pair",
                "args":
                [ { "string": this.state.accounts.alice.pkh },
                  { "prim": "Pair",
                  "args":
                  [ { "string": this.state.accounts.bob.pkh },
                    { "int": "0" } ] } ] } ] },
        "type":
        { "prim": "pair",
            "args":
            [ { "prim": "nat" },
              { "prim": "pair",
              "args":
              [ { "prim": "address" },
                { "prim": "pair",
                "args":
                [ { "prim": "address" },
                  { "prim": "nat" } ] } ] } ] }
      });
      let paramsHash = encodeExpr(packedData.packed);
      paramsHash = b58decode(paramsHash)
      paramsHash = paramsHash.substring(4, paramsHash.length - 2)
      let b = await client.packData(
        { "data":
            { "prim": "Pair",
                "args":
                [ { "prim": "Pair",
                    "args":
                    [ { "string": "NetXz969SFaFn8k" },
                      { "string": eurl } ] },
                  { "prim": "Pair",
                  "args":
                  [ { "int": "3" },
                    { "bytes": paramsHash } ] } ] },
          "type":
          { "prim": "pair",
              "args":
              [ { "prim": "pair",
                "args":
                [ { "prim": "chain_id" },
                  { "prim": "address"} ] } ,
                { "prim": "pair",
                  "args":
                  [ { "prim": "nat" },
                    { "prim": "bytes"} ] } ,
              ] }
        });
        signer = new InMemorySigner(this.state.accounts.alice.secret);
        sig = await signer.sign(b.packed);
        TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.relay.secret) });
        try{
          operation = await contractEurl.methods.permit(await signer.publicKey(), sig.prefixSig, paramsHash).send();
          await operation.confirmation();
          success['permit01'] = operation.hash;
          this.setState({success: success});
        }catch(e){
          fails['permit01'] = e.message;
          this.setState({fails: fails});
          this.setState({info: e.message});
        }
        // permit02: Relay tries to consume permit (wrong amount)
        TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.relay.secret) });
        try{
          operation = await contractEurl.methods.transfer([
            {
              from_: this.state.accounts.alice.pkh,
              txs: [
                {
                  to_: this.state.accounts.bob.pkh,
                  token_id: 0,
                  amount: 200000000,
                },
              ],
            },
          ]).send();
          await operation.confirmation();
          success['permit02'] = operation.hash;
          this.setState({success: success});
        }catch(e){
          fails['permit02'] = e.message;
          this.setState({fails: fails});
          this.setState({info: e.message});
        }
        // permit03: Relay consumes permit
        TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.relay.secret) });
        try{
          operation = await contractEurl.methods.transfer([
            {
              from_: this.state.accounts.alice.pkh,
              txs: [
                {
                  to_: this.state.accounts.bob.pkh,
                  token_id: 0,
                  amount: 20000000,
                },
              ],
            },
          ]).send();
          await operation.confirmation();
          success['permit03'] = operation.hash;
          this.setState({success: success});
        }catch(e){
          fails['permit03'] = e.message;
          this.setState({fails: fails});
          this.setState({info: e.message});
        }

        // permit04: Relay set a permit from Alice, for a transfer of 10 to Bob
        packedData = await client.packData(
          { "data":
              { "prim": "Pair",
                  "args":
                  [ { "int": "10000000" },
                    { "prim": "Pair",
                    "args":
                    [ { "string": this.state.accounts.alice.pkh },
                      { "prim": "Pair",
                      "args":
                      [ { "string": this.state.accounts.bob.pkh },
                        { "int": "0" } ] } ] } ] },
            "type":
            { "prim": "pair",
                "args":
                [ { "prim": "nat" },
                  { "prim": "pair",
                  "args":
                  [ { "prim": "address" },
                    { "prim": "pair",
                    "args":
                    [ { "prim": "address" },
                      { "prim": "nat" } ] } ] } ] }
          });
      paramsHash = encodeExpr(packedData.packed);
      paramsHash = b58decode(paramsHash)
      paramsHash = paramsHash.substring(4, paramsHash.length - 2)
      b = await client.packData(
      { "data":
          { "prim": "Pair",
              "args":
              [ { "prim": "Pair",
                  "args":
                  [ { "string": "NetXz969SFaFn8k" },
                    { "string": eurl } ] },
                { "prim": "Pair",
                "args":
                [ { "int": "4" },
                  { "bytes": paramsHash } ] } ] },
        "type":
        { "prim": "pair",
            "args":
            [ { "prim": "pair",
              "args":
              [ { "prim": "chain_id" },
                { "prim": "address"} ] } ,
              { "prim": "pair",
                "args":
                [ { "prim": "nat" },
                  { "prim": "bytes"} ] } ,
            ] }
      });
      signer = new InMemorySigner(this.state.accounts.alice.secret);
      sig = await signer.sign(b.packed);
      TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.relay.secret) });
      try{
        operation = await contractEurl.methods.permit(await signer.publicKey(), sig.prefixSig, paramsHash).send();
        await operation.confirmation();
        success['permit04'] = operation.hash;
        this.setState({success: success});
      }catch(e){
        fails['permit04'] = e.message;
        this.setState({fails: fails});
        this.setState({info: e.message});
      }
      // permit05: Bob tries to set new expirency permit for Alice
      TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.bob.secret) });
      try{
        operation = await contractEurl.methods.set_expiry(paramsHash, 2000).send();
        await operation.confirmation();
        success['permit05'] = operation.hash;
        this.setState({success: success});
      }catch(e){
        fails['permit05'] = e.message;
        this.setState({fails: fails});
        this.setState({info: e.message});
      }
      // permit06: Alice sets new expirency permit
      TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.alice.secret) });
      try{
        operation = await contractEurl.methods.set_expiry(paramsHash, 2000).send();
        await operation.confirmation();
        success['permit06'] = operation.hash;
        this.setState({success: success});
      }catch(e){
        fails['permit06'] = e.message;
        this.setState({fails: fails});
        this.setState({info: e.message});
      }
      // permit07: Relay consumes permit
      TqtoEurl.setProvider({ signer: await InMemorySigner.fromSecretKey(this.state.accounts.relay.secret) });
      try{
        operation = await contractEurl.methods.transfer([
          {
            from_: this.state.accounts.alice.pkh,
            txs: [
              {
                to_: this.state.accounts.bob.pkh,
                token_id: 0,
                amount: 10000000,
              },
            ],
          },
        ]).send();
        await operation.confirmation();
        success['permit07'] = operation.hash;
        this.setState({success: success});
      }catch(e){
        fails['permit07'] = e.message;
        this.setState({fails: fails});
        this.setState({info: e.message});
      }
      
      this.setState({complete: true});
  }

  render = () => (
    <div className="App">
      <h2>
        Sceme test utility
      </h2>
      <p><small>Node: <strong>{this.state.node}</strong></small></p>
      {!this.state.processing && !this.state.complete && <button onClick={() => {this.startProcess()}}>Start</button>}
      {this.state.complete && <p><strong>Completed</strong></p>}
      {this.state.processing && !this.state.complete && <p>Processing...</p>}
      {this.state.error && <p className="t-fail">{this.state.error}</p>}
      {this.state.info && !this.state.complete && <p className="t-info">{this.state.info}</p>}
      <h3>1. Setting accounts and contract</h3>
      <p>
        Publising owner
        <br/>
        <small>status: {this.state.success['pub_owner']? <strong className="t-success">Success</strong> : this.state.fails['pub_owner']? <strong className="t-fail">Failed</strong> : 'awaiting'} {this.state.success['pub_owner'] && <a href={`${this.state.explorer}${this.state.success['pub_owner']}`} target="_blank" rel="noreferrer">{this.state.success['pub_owner']}</a>}</small>
      </p>
      <p>
        Publising admin
        <br/>
        <small>status: {this.state.success['pub_admin']? <strong className="t-success">Success</strong> : this.state.fails['pub_admin']? <strong className="t-fail">Failed</strong> : 'awaiting'} {this.state.success['pub_admin'] && <a href={`${this.state.explorer}${this.state.success['pub_admin']}`} target="_blank" rel="noreferrer">{this.state.success['pub_admin']}</a>}</small>
      </p>
      <p>
        Publising reserve
        <br/>
        <small>status: {this.state.success['pub_reserve']? <strong className="t-success">Success</strong> : this.state.fails['pub_reserve']? <strong className="t-fail">Failed</strong> : 'awaiting'} {this.state.success['pub_reserve'] && <a href={`${this.state.explorer}${this.state.success['pub_reserve']}`} target="_blank" rel="noreferrer">{this.state.success['pub_reserve']}</a>}</small>
      </p>
      <p>
        Publising master minter
        <br/>
        <small>status: {this.state.success['pub_master_minter']? <strong className="t-success">Success</strong> : this.state.fails['pub_master_minter']? <strong className="t-fail">Failed</strong> : 'awaiting'} {this.state.success['pub_master_minter'] && <a href={`${this.state.explorer}${this.state.success['pub_master_minter']}`} target="_blank" rel="noreferrer">{this.state.success['pub_master_minter']}</a>}</small>
      </p>
      <p>
        Publising fees manager
        <br/>
        <small>status: {this.state.success['pub_fees']? <strong className="t-success">Success</strong> : this.state.fails['pub_fees']? <strong className="t-fail">Failed</strong> : 'awaiting'} {this.state.success['pub_fees'] && <a href={`${this.state.explorer}${this.state.success['pub_fees']}`} target="_blank" rel="noreferrer">{this.state.success['pub_fees']}</a>}</small>
      </p>
      <p>
        Publising FA2 Lugh - EURL
        <br/>
        <small>status: {this.state.success['pub_eurl']? <strong className="t-success">Success</strong> : this.state.fails['pub_eurl']? <strong className="t-fail">Failed</strong> : 'awaiting'} {this.state.success['pub_eurl'] && <a href={`${this.state.explorer}${this.state.success['pub_eurl']}`} target="_blank" rel="noreferrer">{this.state.success['pub_eurl']}</a>}</small>
      </p>
      <p>
        Provisioning signer 1 with 5ꜩ
        <br/>
        <small>status: {this.state.success['signer_1-5']? <strong className="t-success">Success</strong> : this.state.fails['signer_1-5']? <strong className="t-fail">Failed</strong> : 'awaiting'} {this.state.success['signer_1-5'] && <a href={`${this.state.explorer}${this.state.success['signer_1-5']}`} target="_blank" rel="noreferrer">{this.state.success['signer_1-5']}</a>}</small>
      </p>
      <p>
        Provisioning signer 2 with 5ꜩ
        <br/>
        <small>status: {this.state.success['signer_2-5']? <strong className="t-success">Success</strong> : this.state.fails['signer_2-5']? <strong className="t-fail">Failed</strong> : 'awaiting'} {this.state.success['signer_2-5'] && <a href={`${this.state.explorer}${this.state.success['signer_2-5']}`} target="_blank" rel="noreferrer">{this.state.success['signer_2-5']}</a>}</small>
      </p>
      <p>
        Provisioning signer 3 with 5ꜩ
        <br/>
        <small>status: {this.state.success['signer_3-5']? <strong className="t-success">Success</strong> : this.state.fails['signer_3-5']? <strong className="t-fail">Failed</strong> : 'awaiting'} {this.state.success['signer_3-5'] && <a href={`${this.state.explorer}${this.state.success['signer_3-5']}`} target="_blank" rel="noreferrer">{this.state.success['signer_3-5']}</a>}</small>
      </p>
      <p>
        Provisioning signer 4 with 5ꜩ
        <br/>
        <small>status: {this.state.success['signer_4-5']? <strong className="t-success">Success</strong> : this.state.fails['signer_4-5']? <strong className="t-fail">Failed</strong> : 'awaiting'} {this.state.success['signer_4-5'] && <a href={`${this.state.explorer}${this.state.success['signer_4-5']}`} target="_blank" rel="noreferrer">{this.state.success['signer_4-5']}</a>}</small>
      </p>
      <p>
        Provisioning new admin with 5ꜩ
        <br/>
        <small>status: {this.state.success['new_admin-5']? <strong className="t-success">Success</strong> : this.state.fails['new_admin-5']? <strong className="t-fail">Failed</strong> : 'awaiting'} {this.state.success['new_admin-5'] && <a href={`${this.state.explorer}${this.state.success['new_admin-5']}`} target="_blank" rel="noreferrer">{this.state.success['new_admin-5']}</a>}</small>
      </p>
      <p>
        Provisioning new master minter with 5ꜩ
        <br/>
        <small>status: {this.state.success['new_master_minter-5']? <strong className="t-success">Success</strong> : this.state.fails['new_master_minter-5']? <strong className="t-fail">Failed</strong> : 'awaiting'} {this.state.success['new_master_minter-5'] && <a href={`${this.state.explorer}${this.state.success['new_master_minter-5']}`} target="_blank" rel="noreferrer">{this.state.success['new_master_minter-5']}</a>}</small>
      </p>
      <p>
        Provisioning alice with 5ꜩ
        <br/>
        <small>status: {this.state.success['alice-5']? <strong className="t-success">Success</strong> : this.state.fails['alice-5']? <strong className="t-fail">Failed</strong> : 'awaiting'} {this.state.success['alice-5'] && <a href={`${this.state.explorer}${this.state.success['alice-5']}`} target="_blank" rel="noreferrer">{this.state.success['alice-5']}</a>}</small>
      </p>
      <p>
        Provisioning bob with 5ꜩ
        <br/>
        <small>status: {this.state.success['bob-5']? <strong className="t-success">Success</strong> : this.state.fails['bob-5']? <strong className="t-fail">Failed</strong> : 'awaiting'} {this.state.success['bob-5'] && <a href={`${this.state.explorer}${this.state.success['bob-5']}`} target="_blank" rel="noreferrer">{this.state.success['bob-5']}</a>}</small>
      </p>
      <p>
        Provisioning carl with 5ꜩ
        <br/>
        <small>status: {this.state.success['carl-5']? <strong className="t-success">Success</strong> : this.state.fails['carl-5']? <strong className="t-fail">Failed</strong> : 'awaiting'} {this.state.success['carl-5'] && <a href={`${this.state.explorer}${this.state.success['carl-5']}`} target="_blank" rel="noreferrer">{this.state.success['carl-5']}</a>}</small>
      </p>
      <p>
        Provisioning dana with 5ꜩ
        <br/>
        <small>status: {this.state.success['dana-5']? <strong className="t-success">Success</strong> : this.state.fails['dana-5']? <strong className="t-fail">Failed</strong> : 'awaiting'} {this.state.success['dana-5'] && <a href={`${this.state.explorer}${this.state.success['dana-5']}`} target="_blank" rel="noreferrer">{this.state.success['dana-5']}</a>}</small>
      </p>
      <p>
        Provisioning relay with 5ꜩ
        <br/>
        <small>status: {this.state.success['relay-5']? <strong className="t-success">Success</strong> : this.state.fails['relay-5']? <strong className="t-fail">Failed</strong> : 'awaiting'} {this.state.success['relay-5'] && <a href={`${this.state.explorer}${this.state.success['relay-5']}`} target="_blank" rel="noreferrer">{this.state.success['relay-5']}</a>}</small>
      </p>
      <p>
        Provisioning fees faucet with 5ꜩ
        <br/>
        <small>status: {this.state.success['fees_faucet-5']? <strong className="t-success">Success</strong> : this.state.fails['fees_faucet-5']? <strong className="t-fail">Failed</strong> : 'awaiting'} {this.state.success['fees_faucet-5'] && <a href={`${this.state.explorer}${this.state.success['fees_faucet-5']}`} target="_blank" rel="noreferrer">{this.state.success['fees_faucet-5']}</a>}</small>
      </p>
      <p>
        Provisioning new fees faucet with 5ꜩ
        <br/>
        <small>status: {this.state.success['new_fees_faucet-5']? <strong className="t-success">Success</strong> : this.state.fails['new_fees_faucet-5']? <strong className="t-fail">Failed</strong> : 'awaiting'} {this.state.success['new_fees_faucet-5'] && <a href={`${this.state.explorer}${this.state.success['new_fees_faucet-5']}`} target="_blank" rel="noreferrer">{this.state.success['new_fees_faucet-5']}</a>}</small>
      </p>
      <p>
        Provisioning delegated minter with 5ꜩ
        <br/>
        <small>status: {this.state.success['delegated_minter-5']? <strong className="t-success">Success</strong> : this.state.fails['delegated_minter-5']? <strong className="t-fail">Failed</strong> : 'awaiting'} {this.state.success['delegated_minter-5'] && <a href={`${this.state.explorer}${this.state.success['delegated_minter-5']}`} target="_blank" rel="noreferrer">{this.state.success['delegated_minter-5']}</a>}</small>
      </p>
      <h3>2. Minting</h3>
      <p>
        mint1: Bob tries to mint 10000EURL to reserve
        <br/>
        <small>status: {this.state.success['mint1']? <strong className="t-success">Success</strong> : this.state.fails['mint1']? <strong className="t-fail">Failed : {this.state.fails['mint1']}</strong> : 'awaiting'} {this.state.success['mint1'] && <a href={`${this.state.explorer}${this.state.success['mint1']}`} target="_blank" rel="noreferrer">{this.state.success['mint1']}</a>}</small>
      </p>
      <p>
        mint2: Master minter mints 10000EURL to reserve
        <br/>
        <small>Signer 3 creates proposal</small><br/>
        <small>status: {this.state.success['mint2-1']? <strong className="t-success">Success</strong> : this.state.fails['mint2-1']? <strong className="t-fail">Failed : {this.state.fails['mint2-1']}</strong> : 'awaiting'} {this.state.success['mint2-1'] && <a href={`${this.state.explorer}${this.state.success['mint2-1']}`} target="_blank" rel="noreferrer">{this.state.success['mint2-1']}</a>}</small>
        <br/>
        <small>Signer 1 accepts proposal</small><br/>
        <small>status: {this.state.success['mint2-2']? <strong className="t-success">Success</strong> : this.state.fails['mint2-2']? <strong className="t-fail">Failed : {this.state.fails['mint2-2']}</strong> : 'awaiting'} {this.state.success['mint2-2'] && <a href={`${this.state.explorer}${this.state.success['mint2-2']}`} target="_blank" rel="noreferrer">{this.state.success['mint2-2']}</a>}</small>
      </p>
      <h3>3. Transfers</h3>
      <p>
        tx1: Bob tries to transfer from reserve to Alice
        <br/>
        <small>status: {this.state.success['tx1']? <strong className="t-success">Success</strong> : this.state.fails['tx1']? <strong className="t-fail">Failed : {this.state.fails['tx1']}</strong> : 'awaiting'} {this.state.success['tx1'] && <a href={`${this.state.explorer}${this.state.success['tx1']}`} target="_blank" rel="noreferrer">{this.state.success['tx1']}</a>}</small>
      </p>
      <p>
        tx2: Reserve tries to over transfer to Alice
        <br/>
        <small>Signer 1 creates proposal</small><br/>
        <small>status: {this.state.success['tx2-1']? <strong className="t-success">Success</strong> : this.state.fails['tx2-1']? <strong className="t-fail">Failed : {this.state.fails['tx2-1']}</strong> : 'awaiting'} {this.state.success['tx2-1'] && <a href={`${this.state.explorer}${this.state.success['tx2-1']}`} target="_blank" rel="noreferrer">{this.state.success['tx2-1']}</a>}</small>
        <br/>
        <small>Signer 2 accepts proposal</small><br/>
        <small>status: {this.state.success['tx2-2']? <strong className="t-success">Success</strong> : this.state.fails['tx2-2']? <strong className="t-fail">Failed : {this.state.fails['tx2-2']}</strong> : 'awaiting'} {this.state.success['tx2-2'] && <a href={`${this.state.explorer}${this.state.success['tx2-2']}`} target="_blank" rel="noreferrer">{this.state.success['tx2-2']}</a>}</small>
      </p>
      <p>
        tx3: Reserve transfers 5000EURL to Alice
        <br/>
        <small>Signer 1 creates proposal</small><br/>
        <small>status: {this.state.success['tx3-1']? <strong className="t-success">Success</strong> : this.state.fails['tx3-1']? <strong className="t-fail">Failed : {this.state.fails['tx3-1']}</strong> : 'awaiting'} {this.state.success['tx3-1'] && <a href={`${this.state.explorer}${this.state.success['tx3-1']}`} target="_blank" rel="noreferrer">{this.state.success['tx3-1']}</a>}</small>
        <br/>
        <small>Signer 2 accepts proposal</small><br/>
        <small>status: {this.state.success['tx3-2']? <strong className="t-success">Success</strong> : this.state.fails['tx3-2']? <strong className="t-fail">Failed : {this.state.fails['tx3-2']}</strong> : 'awaiting'} {this.state.success['tx3-2'] && <a href={`${this.state.explorer}${this.state.success['tx3-2']}`} target="_blank" rel="noreferrer">{this.state.success['tx3-2']}</a>}</small>
      </p>
      <p>
        tx4: Alice transfers 1000EURL to Bob & Carl
        <br/>
        <small>status: {this.state.success['tx4']? <strong className="t-success">Success</strong> : this.state.fails['tx4']? <strong className="t-fail">Failed : {this.state.fails['tx4']}</strong> : 'awaiting'} {this.state.success['tx4'] && <a href={`${this.state.explorer}${this.state.success['tx4']}`} target="_blank" rel="noreferrer">{this.state.success['tx4']}</a>}</small>
      </p>
      <p>
        check1: Fees faucet balance is 1.4EURL?
        <br/>
        <small>status: {this.state.success['check1']? <strong className="t-success">Success</strong> : this.state.fails['check1']? <strong className="t-fail">Failed : {this.state.fails['check1']}</strong> : 'awaiting'} {this.state.success['check1'] && <a href={`${this.state.explorer}${this.state.success['check1']}`} target="_blank" rel="noreferrer">{this.state.success['check1']}</a>}</small>
      </p>
      <p>
        tx5: Admin forces transfer of 500EURL from Carl to Alice
        <br/>
        <small>Signer 1 creates proposal</small><br/>
        <small>status: {this.state.success['tx5-1']? <strong className="t-success">Success</strong> : this.state.fails['tx5-1']? <strong className="t-fail">Failed : {this.state.fails['tx5-1']}</strong> : 'awaiting'} {this.state.success['tx5-1'] && <a href={`${this.state.explorer}${this.state.success['tx5-1']}`} target="_blank" rel="noreferrer">{this.state.success['tx5-1']}</a>}</small>
        <br/>
        <small>Signer 2 accepts proposal</small><br/>
        <small>status: {this.state.success['tx5-2']? <strong className="t-success">Success</strong> : this.state.fails['tx5-2']? <strong className="t-fail">Failed : {this.state.fails['tx5-2']}</strong> : 'awaiting'} {this.state.success['tx5-2'] && <a href={`${this.state.explorer}${this.state.success['tx5-2']}`} target="_blank" rel="noreferrer">{this.state.success['tx5-2']}</a>}</small>
      </p>
      <p>
        check2: Alice balance is 3499,6EURL?
        <br/>
        <small>status: {this.state.success['check2']? <strong className="t-success">Success</strong> : this.state.fails['check2']? <strong className="t-fail">Failed : {this.state.fails['check2']}</strong> : 'awaiting'} {this.state.success['check2'] && <a href={`${this.state.explorer}${this.state.success['check2']}`} target="_blank" rel="noreferrer">{this.state.success['check2']}</a>}</small>
      </p>
      <p>
        check3: Bob balance is 1000EURL?
        <br/>
        <small>status: {this.state.success['check3']? <strong className="t-success">Success</strong> : this.state.fails['check3']? <strong className="t-fail">Failed : {this.state.fails['check3']}</strong> : 'awaiting'} {this.state.success['check3'] && <a href={`${this.state.explorer}${this.state.success['check3']}`} target="_blank" rel="noreferrer">{this.state.success['check3']}</a>}</small>
      </p>
      <p>
        check4: Carl balance is 500EURL?
        <br/>
        <small>status: {this.state.success['check4']? <strong className="t-success">Success</strong> : this.state.fails['check4']? <strong className="t-fail">Failed : {this.state.fails['check4']}</strong> : 'awaiting'} {this.state.success['check4'] && <a href={`${this.state.explorer}${this.state.success['check4']}`} target="_blank" rel="noreferrer">{this.state.success['check4']}</a>}</small>
      </p>
      <p>
        check5: Reserve balance is 4999EURL?
        <br/>
        <small>status: {this.state.success['check5']? <strong className="t-success">Success</strong> : this.state.fails['check5']? <strong className="t-fail">Failed : {this.state.fails['check5']}</strong> : 'awaiting'} {this.state.success['check5'] && <a href={`${this.state.explorer}${this.state.success['check5']}`} target="_blank" rel="noreferrer">{this.state.success['check5']}</a>}</small>
      </p>
      <h3>4. Operators</h3>
      <p>op1: Alice adds Dana as operator
        <br/>
        <small>status: {this.state.success['op1']? <strong className="t-success">Success</strong> : this.state.fails['op1']? <strong className="t-fail">Failed : {this.state.fails['op1']}</strong> : 'awaiting'} {this.state.success['op1'] && <a href={`${this.state.explorer}${this.state.success['op1']}`} target="_blank" rel="noreferrer">{this.state.success['op1']}</a>}</small>
      </p>
      <p>op2: Bob adds Dana as operator
        <br/>
        <small>status: {this.state.success['op2']? <strong className="t-success">Success</strong> : this.state.fails['op2']? <strong className="t-fail">Failed : {this.state.fails['op2']}</strong> : 'awaiting'} {this.state.success['op2'] && <a href={`${this.state.explorer}${this.state.success['op2']}`} target="_blank" rel="noreferrer">{this.state.success['op2']}</a>}</small>
      </p>
      <p>op3: Carl tries to transfer for Alice
        <br/>
        <small>status: {this.state.success['op3']? <strong className="t-success">Success</strong> : this.state.fails['op3']? <strong className="t-fail">Failed : {this.state.fails['op3']}</strong> : 'awaiting'} {this.state.success['op3'] && <a href={`${this.state.explorer}${this.state.success['op3']}`} target="_blank" rel="noreferrer">{this.state.success['op3']}</a>}</small>
      </p>
      <p>op4: Dana transfers for Alice and Bob 200EURL each
        <br/>
        <small>status: {this.state.success['op4']? <strong className="t-success">Success</strong> : this.state.fails['op4']? <strong className="t-fail">Failed : {this.state.fails['op4']}</strong> : 'awaiting'} {this.state.success['op4'] && <a href={`${this.state.explorer}${this.state.success['op4']}`} target="_blank" rel="noreferrer">{this.state.success['op4']}</a>}</small>
      </p>
      <p>op5: Alice removes Dana as operator
        <br/>
        <small>status: {this.state.success['op5']? <strong className="t-success">Success</strong> : this.state.fails['op5']? <strong className="t-fail">Failed : {this.state.fails['op5']}</strong> : 'awaiting'} {this.state.success['op5'] && <a href={`${this.state.explorer}${this.state.success['op5']}`} target="_blank" rel="noreferrer">{this.state.success['op5']}</a>}</small>
      </p>
      <p>op6: Dana tries to transfer for Alice
        <br/>
        <small>status: {this.state.success['op6']? <strong className="t-success">Success</strong> : this.state.fails['op6']? <strong className="t-fail">Failed : {this.state.fails['op6']}</strong> : 'awaiting'} {this.state.success['op6'] && <a href={`${this.state.explorer}${this.state.success['op6']}`} target="_blank" rel="noreferrer">{this.state.success['op6']}</a>}</small>
      </p>

      <h3>5. Lock / unlock</h3>
      <p>lck1: Carl tries to lock Bob
        <br/>
        <small>status: {this.state.success['lck1']? <strong className="t-success">Success</strong> : this.state.fails['lck1']? <strong className="t-fail">Failed : {this.state.fails['lck1']}</strong> : 'awaiting'} {this.state.success['lck1'] && <a href={`${this.state.explorer}${this.state.success['lck1']}`} target="_blank" rel="noreferrer">{this.state.success['lck1']}</a>}</small>
      </p>
      <p>lck2: Admin locks Bob
        <br/>
        <small>Signer 1 creates proposal</small><br/>
        <small>status: {this.state.success['lck2-1']? <strong className="t-success">Success</strong> : this.state.fails['lck2-1']? <strong className="t-fail">Failed : {this.state.fails['lck2-1']}</strong> : 'awaiting'} {this.state.success['lck2-1'] && <a href={`${this.state.explorer}${this.state.success['lck2-1']}`} target="_blank" rel="noreferrer">{this.state.success['lck2-1']}</a>}</small>
        <br/>
        <small>Signer 2 accepts proposal</small><br/>
        <small>status: {this.state.success['lck2-2']? <strong className="t-success">Success</strong> : this.state.fails['lck2-2']? <strong className="t-fail">Failed : {this.state.fails['lck2-2']}</strong> : 'awaiting'} {this.state.success['lck2-2'] && <a href={`${this.state.explorer}${this.state.success['lck2-2']}`} target="_blank" rel="noreferrer">{this.state.success['lck2-2']}</a>}</small>
      </p>
      <p>lck3: Bob tries to transfer to Alice
        <br/>
        <small>status: {this.state.success['lck3']? <strong className="t-success">Success</strong> : this.state.fails['lck3']? <strong className="t-fail">Failed : {this.state.fails['lck3']}</strong> : 'awaiting'} {this.state.success['lck3'] && <a href={`${this.state.explorer}${this.state.success['lck3']}`} target="_blank" rel="noreferrer">{this.state.success['lck3']}</a>}</small>
      </p>
      <p>lck4: Alice transfers to Bob 200EURL
        <br/>
        <small>status: {this.state.success['lck4']? <strong className="t-success">Success</strong> : this.state.fails['lck4']? <strong className="t-fail">Failed : {this.state.fails['lck4']}</strong> : 'awaiting'} {this.state.success['lck4'] && <a href={`${this.state.explorer}${this.state.success['lck4']}`} target="_blank" rel="noreferrer">{this.state.success['lck4']}</a>}</small>
      </p>
      <p>lck5: Admin forces transfers from Bob to Alice for 200EURL
        <br/>
        <small>Signer 1 creates proposal</small><br/>
        <small>status: {this.state.success['lck5-1']? <strong className="t-success">Success</strong> : this.state.fails['lck5-1']? <strong className="t-fail">Failed : {this.state.fails['lck5-1']}</strong> : 'awaiting'} {this.state.success['lck5-1'] && <a href={`${this.state.explorer}${this.state.success['lck5-1']}`} target="_blank" rel="noreferrer">{this.state.success['lck5-1']}</a>}</small>
        <br/>
        <small>Signer 2 accepts proposal</small><br/>
        <small>status: {this.state.success['lck5-2']? <strong className="t-success">Success</strong> : this.state.fails['lck5-2']? <strong className="t-fail">Failed : {this.state.fails['lck5-2']}</strong> : 'awaiting'} {this.state.success['lck5-2'] && <a href={`${this.state.explorer}${this.state.success['lck5-2']}`} target="_blank" rel="noreferrer">{this.state.success['lck5-2']}</a>}</small>
      </p>
      <p>lck6: Admin unlocks Bob
        <br/>
        <small>Signer 1 creates proposal</small><br/>
        <small>status: {this.state.success['lck6-1']? <strong className="t-success">Success</strong> : this.state.fails['lck6-1']? <strong className="t-fail">Failed : {this.state.fails['lck6-1']}</strong> : 'awaiting'} {this.state.success['lck6-1'] && <a href={`${this.state.explorer}${this.state.success['lck6-1']}`} target="_blank" rel="noreferrer">{this.state.success['lck6-1']}</a>}</small>
        <br/>
        <small>Signer 2 accepts proposal</small><br/>
        <small>status: {this.state.success['lck6-2']? <strong className="t-success">Success</strong> : this.state.fails['lck6-2']? <strong className="t-fail">Failed : {this.state.fails['lck6-2']}</strong> : 'awaiting'} {this.state.success['lck6-2'] && <a href={`${this.state.explorer}${this.state.success['lck6-2']}`} target="_blank" rel="noreferrer">{this.state.success['lck6-2']}</a>}</small>
      </p>
      <p>lck7: Bob transfers 100EURL to Alice
        <br/>
        <small>status: {this.state.success['lck7']? <strong className="t-success">Success</strong> : this.state.fails['lck7']? <strong className="t-fail">Failed : {this.state.fails['lck7']}</strong> : 'awaiting'} {this.state.success['lck7'] && <a href={`${this.state.explorer}${this.state.success['lck7']}`} target="_blank" rel="noreferrer">{this.state.success['lck7']}</a>}</small>
      </p>
    
      <h3>6. Pause</h3>
      <p>p1: Bob tries to pause transfers
        <br/>
        <small>status: {this.state.success['p1']? <strong className="t-success">Success</strong> : this.state.fails['p1']? <strong className="t-fail">Failed : {this.state.fails['p1']}</strong> : 'awaiting'} {this.state.success['p1'] && <a href={`${this.state.explorer}${this.state.success['p1']}`} target="_blank" rel="noreferrer">{this.state.success['p1']}</a>}</small>
      </p>
      <p>p2: Admin pauses transfers
        <br/>
        <small>Signer 1 creates proposal</small><br/>
        <small>status: {this.state.success['p2-1']? <strong className="t-success">Success</strong> : this.state.fails['p2-1']? <strong className="t-fail">Failed : {this.state.fails['p2-1']}</strong> : 'awaiting'} {this.state.success['p2-1'] && <a href={`${this.state.explorer}${this.state.success['p2-1']}`} target="_blank" rel="noreferrer">{this.state.success['p2-1']}</a>}</small>
        <br/>
        <small>Signer 2 accepts proposal</small><br/>
        <small>status: {this.state.success['p2-2']? <strong className="t-success">Success</strong> : this.state.fails['p2-2']? <strong className="t-fail">Failed : {this.state.fails['p2-2']}</strong> : 'awaiting'} {this.state.success['p2-2'] && <a href={`${this.state.explorer}${this.state.success['p2-2']}`} target="_blank" rel="noreferrer">{this.state.success['p2-2']}</a>}</small>
      </p>
      <p>p3: Alice tries to transfer
        <br/>
        <small>status: {this.state.success['p3']? <strong className="t-success">Success</strong> : this.state.fails['p3']? <strong className="t-fail">Failed : {this.state.fails['p3']}</strong> : 'awaiting'} {this.state.success['p3'] && <a href={`${this.state.explorer}${this.state.success['p3']}`} target="_blank" rel="noreferrer">{this.state.success['p3']}</a>}</small>
      </p>
      <p>p4: Dana tries to transfer for Bob
        <br/>
        <small>status: {this.state.success['p4']? <strong className="t-success">Success</strong> : this.state.fails['p4']? <strong className="t-fail">Failed : {this.state.fails['p4']}</strong> : 'awaiting'} {this.state.success['p4'] && <a href={`${this.state.explorer}${this.state.success['p4']}`} target="_blank" rel="noreferrer">{this.state.success['p4']}</a>}</small>
      </p>
      <p>p5: Admin forces transfers from Alice to Bob for 200EURL
        <br/>
        <small>Signer 1 creates proposal</small><br/>
        <small>status: {this.state.success['p5-1']? <strong className="t-success">Success</strong> : this.state.fails['p5-1']? <strong className="t-fail">Failed : {this.state.fails['p5-1']}</strong> : 'awaiting'} {this.state.success['p5-1'] && <a href={`${this.state.explorer}${this.state.success['p5-1']}`} target="_blank" rel="noreferrer">{this.state.success['p5-1']}</a>}</small>
        <br/>
        <small>Signer 2 accepts proposal</small><br/>
        <small>status: {this.state.success['p5-2']? <strong className="t-success">Success</strong> : this.state.fails['p5-2']? <strong className="t-fail">Failed : {this.state.fails['p5-2']}</strong> : 'awaiting'} {this.state.success['p5-2'] && <a href={`${this.state.explorer}${this.state.success['p5-2']}`} target="_blank" rel="noreferrer">{this.state.success['p5-2']}</a>}</small>
      </p>
      <p>p6: Admin unpauses transfers
        <br/>
        <small>Signer 1 creates proposal</small><br/>
        <small>status: {this.state.success['p6-1']? <strong className="t-success">Success</strong> : this.state.fails['p6-1']? <strong className="t-fail">Failed : {this.state.fails['p6-1']}</strong> : 'awaiting'} {this.state.success['p6-1'] && <a href={`${this.state.explorer}${this.state.success['p6-1']}`} target="_blank" rel="noreferrer">{this.state.success['p6-1']}</a>}</small>
        <br/>
        <small>Signer 2 accepts proposal</small><br/>
        <small>status: {this.state.success['p6-2']? <strong className="t-success">Success</strong> : this.state.fails['p6-2']? <strong className="t-fail">Failed : {this.state.fails['p6-2']}</strong> : 'awaiting'} {this.state.success['p6-2'] && <a href={`${this.state.explorer}${this.state.success['p6-2']}`} target="_blank" rel="noreferrer">{this.state.success['p6-2']}</a>}</small>
      </p>
      <p>p7: Alice transfers 200EURL to Bob
        <br/>
        <small>status: {this.state.success['p7']? <strong className="t-success">Success</strong> : this.state.fails['p7']? <strong className="t-fail">Failed : {this.state.fails['p7']}</strong> : 'awaiting'} {this.state.success['p7'] && <a href={`${this.state.explorer}${this.state.success['p7']}`} target="_blank" rel="noreferrer">{this.state.success['p7']}</a>}</small>
      </p>

      <h3>7. Minters</h3>
      <p>m1: Bob tries to add a delegated minter
        <br/>
        <small>status: {this.state.success['m1']? <strong className="t-success">Success</strong> : this.state.fails['m1']? <strong className="t-fail">Failed : {this.state.fails['m1']}</strong> : 'awaiting'} {this.state.success['m1'] && <a href={`${this.state.explorer}${this.state.success['m1']}`} target="_blank" rel="noreferrer">{this.state.success['m1']}</a>}</small>
      </p>
      <p>m2: Master minter adds a delegated minter with an allowance of 1000EURL
        <br/>
        <small>Signer 3 creates proposal</small><br/>
        <small>status: {this.state.success['m2-1']? <strong className="t-success">Success</strong> : this.state.fails['m2-1']? <strong className="t-fail">Failed : {this.state.fails['m2-1']}</strong> : 'awaiting'} {this.state.success['m2-1'] && <a href={`${this.state.explorer}${this.state.success['m2-1']}`} target="_blank" rel="noreferrer">{this.state.success['m2-1']}</a>}</small>
        <br/>
        <small>Signer 1 accepts proposal</small><br/>
        <small>status: {this.state.success['m2-2']? <strong className="t-success">Success</strong> : this.state.fails['m2-2']? <strong className="t-fail">Failed : {this.state.fails['m2-2']}</strong> : 'awaiting'} {this.state.success['m2-2'] && <a href={`${this.state.explorer}${this.state.success['m2-2']}`} target="_blank" rel="noreferrer">{this.state.success['m2-2']}</a>}</small>
      </p>
      <p>m3: Delegated minter tries over mint
        <br/>
        <small>status: {this.state.success['m3']? <strong className="t-success">Success</strong> : this.state.fails['m3']? <strong className="t-fail">Failed : {this.state.fails['m3']}</strong> : 'awaiting'} {this.state.success['m3'] && <a href={`${this.state.explorer}${this.state.success['m3']}`} target="_blank" rel="noreferrer">{this.state.success['m3']}</a>}</small>
      </p>
      <p>m4: Delegated minter mints 800EURL to Carl
        <br/>
        <small>status: {this.state.success['m4']? <strong className="t-success">Success</strong> : this.state.fails['m4']? <strong className="t-fail">Failed : {this.state.fails['m4']}</strong> : 'awaiting'} {this.state.success['m4'] && <a href={`${this.state.explorer}${this.state.success['m4']}`} target="_blank" rel="noreferrer">{this.state.success['m4']}</a>}</small>
      </p>
      <p>m5: Delegated minter tries to overmint 201EURL
        <br/>
        <small>status: {this.state.success['m5']? <strong className="t-success">Success</strong> : this.state.fails['m5']? <strong className="t-fail">Failed : {this.state.fails['m5']}</strong> : 'awaiting'} {this.state.success['m5'] && <a href={`${this.state.explorer}${this.state.success['m5']}`} target="_blank" rel="noreferrer">{this.state.success['m5']}</a>}</small>
      </p>
      <p>m6: Delegated minter tries to update his allowance
        <br/>
        <small>status: {this.state.success['m6']? <strong className="t-success">Success</strong> : this.state.fails['m6']? <strong className="t-fail">Failed : {this.state.fails['m6']}</strong> : 'awaiting'} {this.state.success['m6'] && <a href={`${this.state.explorer}${this.state.success['m6']}`} target="_blank" rel="noreferrer">{this.state.success['m6']}</a>}</small>
      </p>
      <p>m7: Master minter set delegated minter's allowance to 500EURL
        <br/>
        <small>Signer 3 creates proposal</small><br/>
        <small>status: {this.state.success['m7-1']? <strong className="t-success">Success</strong> : this.state.fails['m7-1']? <strong className="t-fail">Failed : {this.state.fails['m7-1']}</strong> : 'awaiting'} {this.state.success['m7-1'] && <a href={`${this.state.explorer}${this.state.success['m7-1']}`} target="_blank" rel="noreferrer">{this.state.success['m7-1']}</a>}</small>
        <br/>
        <small>Signer 1 accepts proposal</small><br/>
        <small>status: {this.state.success['m7-2']? <strong className="t-success">Success</strong> : this.state.fails['m7-2']? <strong className="t-fail">Failed : {this.state.fails['m7-2']}</strong> : 'awaiting'} {this.state.success['m7-2'] && <a href={`${this.state.explorer}${this.state.success['m7-2']}`} target="_blank" rel="noreferrer">{this.state.success['m7-2']}</a>}</small>
      </p>
      <p>m8: Delegated minter mints 400EURL to Carl
        <br/>
        <small>status: {this.state.success['m8']? <strong className="t-success">Success</strong> : this.state.fails['m8']? <strong className="t-fail">Failed : {this.state.fails['m8']}</strong> : 'awaiting'} {this.state.success['m8'] && <a href={`${this.state.explorer}${this.state.success['m8']}`} target="_blank" rel="noreferrer">{this.state.success['m8']}</a>}</small>
      </p>
      <p>m9: Delegated minter tries to overburn
        <br/>
        <small>status: {this.state.success['m9']? <strong className="t-success">Success</strong> : this.state.fails['m9']? <strong className="t-fail">Failed : {this.state.fails['m9']}</strong> : 'awaiting'} {this.state.success['m9'] && <a href={`${this.state.explorer}${this.state.success['m9']}`} target="_blank" rel="noreferrer">{this.state.success['m9']}</a>}</small>
      </p>
      <p>m10: Delegated minter burns 1200EURL
        <br/>
        <small>status: {this.state.success['m10']? <strong className="t-success">Success</strong> : this.state.fails['m10']? <strong className="t-fail">Failed : {this.state.fails['m10']}</strong> : 'awaiting'} {this.state.success['m10'] && <a href={`${this.state.explorer}${this.state.success['m10']}`} target="_blank" rel="noreferrer">{this.state.success['m10']}</a>}</small>
      </p>
      <p>m11: Master minter tries to overburn Carl
        <br/>
        <small>Signer 3 creates proposal</small><br/>
        <small>status: {this.state.success['m11-1']? <strong className="t-success">Success</strong> : this.state.fails['m11-1']? <strong className="t-fail">Failed : {this.state.fails['m11-1']}</strong> : 'awaiting'} {this.state.success['m11-1'] && <a href={`${this.state.explorer}${this.state.success['m11-1']}`} target="_blank" rel="noreferrer">{this.state.success['m11-1']}</a>}</small>
        <br/>
        <small>Signer 1 accepts proposal</small><br/>
        <small>status: {this.state.success['m11-2']? <strong className="t-success">Success</strong> : this.state.fails['m11-2']? <strong className="t-fail">Failed : {this.state.fails['m11-2']}</strong> : 'awaiting'} {this.state.success['m11-2'] && <a href={`${this.state.explorer}${this.state.success['m11-2']}`} target="_blank" rel="noreferrer">{this.state.success['m11-2']}</a>}</small>
      </p>
      <p>m12: Master minter burns 100EURL from Carl
        <br/>
        <small>Signer 3 creates proposal</small><br/>
        <small>status: {this.state.success['m12-1']? <strong className="t-success">Success</strong> : this.state.fails['m12-1']? <strong className="t-fail">Failed : {this.state.fails['m12-1']}</strong> : 'awaiting'} {this.state.success['m12-1'] && <a href={`${this.state.explorer}${this.state.success['m12-1']}`} target="_blank" rel="noreferrer">{this.state.success['m12-1']}</a>}</small>
        <br/>
        <small>Signer 1 accepts proposal</small><br/>
        <small>status: {this.state.success['m12-2']? <strong className="t-success">Success</strong> : this.state.fails['m12-2']? <strong className="t-fail">Failed : {this.state.fails['m12-2']}</strong> : 'awaiting'} {this.state.success['m12-2'] && <a href={`${this.state.explorer}${this.state.success['m12-2']}`} target="_blank" rel="noreferrer">{this.state.success['m12-2']}</a>}</small>
      </p>
      <p>m13: Master minter removes delegated minter
        <br/>
        <small>Signer 3 creates proposal</small><br/>
        <small>status: {this.state.success['m13-1']? <strong className="t-success">Success</strong> : this.state.fails['m13-1']? <strong className="t-fail">Failed : {this.state.fails['m13-1']}</strong> : 'awaiting'} {this.state.success['m13-1'] && <a href={`${this.state.explorer}${this.state.success['m13-1']}`} target="_blank" rel="noreferrer">{this.state.success['m13-1']}</a>}</small>
        <br/>
        <small>Signer 1 accepts proposal</small><br/>
        <small>status: {this.state.success['m13-2']? <strong className="t-success">Success</strong> : this.state.fails['m13-2']? <strong className="t-fail">Failed : {this.state.fails['m13-2']}</strong> : 'awaiting'} {this.state.success['m13-2'] && <a href={`${this.state.explorer}${this.state.success['m13-2']}`} target="_blank" rel="noreferrer">{this.state.success['m13-2']}</a>}</small>
      </p>
      <p>m14: Delegated minter tries to mint 50EURL
        <br/>
        <small>status: {this.state.success['m14']? <strong className="t-success">Success</strong> : this.state.fails['m14']? <strong className="t-fail">Failed : {this.state.fails['m14']}</strong> : 'awaiting'} {this.state.success['m14'] && <a href={`${this.state.explorer}${this.state.success['m14']}`} target="_blank" rel="noreferrer">{this.state.success['m14']}</a>}</small>
      </p>

      <h3>8. Owner</h3>
      <p>own1: Bob tries to create a new token
        <br/>
        <small>status: {this.state.success['own1']? <strong className="t-success">Success</strong> : this.state.fails['own1']? <strong className="t-fail">Failed : {this.state.fails['own1']}</strong> : 'awaiting'} {this.state.success['own1'] && <a href={`${this.state.explorer}${this.state.success['own1']}`} target="_blank" rel="noreferrer">{this.state.success['own1']}</a>}</small>
      </p>
      <p>own2: Owner creates new Token MEUR
        <br/>
        <small>Signer 2 creates proposal</small><br/>
        <small>status: {this.state.success['own2-1']? <strong className="t-success">Success</strong> : this.state.fails['own2-1']? <strong className="t-fail">Failed : {this.state.fails['own2-1']}</strong> : 'awaiting'} {this.state.success['own2-1'] && <a href={`${this.state.explorer}${this.state.success['own2-1']}`} target="_blank" rel="noreferrer">{this.state.success['own2-1']}</a>}</small>
        <br/>
        <small>Signer 1 accepts proposal</small><br/>
        <small>status: {this.state.success['own2-2']? <strong className="t-success">Success</strong> : this.state.fails['own2-2']? <strong className="t-fail">Failed : {this.state.fails['own2-2']}</strong> : 'awaiting'} {this.state.success['own2-2'] && <a href={`${this.state.explorer}${this.state.success['own2-2']}`} target="_blank" rel="noreferrer">{this.state.success['own2-2']}</a>}</small>
        <br/>
        <small>Signer 3 accepts proposal</small><br/>
        <small>status: {this.state.success['own2-3']? <strong className="t-success">Success</strong> : this.state.fails['own2-3']? <strong className="t-fail">Failed : {this.state.fails['own2-3']}</strong> : 'awaiting'} {this.state.success['own2-3'] && <a href={`${this.state.explorer}${this.state.success['own2-3']}`} target="_blank" rel="noreferrer">{this.state.success['own2-3']}</a>}</small>
      </p>
      <p>own3: Master minter mints 1000MEUR
        <br/>
        <small>Signer 3 creates proposal</small><br/>
        <small>status: {this.state.success['own3-1']? <strong className="t-success">Success</strong> : this.state.fails['own3-1']? <strong className="t-fail">Failed : {this.state.fails['own3-1']}</strong> : 'awaiting'} {this.state.success['own3-1'] && <a href={`${this.state.explorer}${this.state.success['own3-1']}`} target="_blank" rel="noreferrer">{this.state.success['own3-1']}</a>}</small>
        <br/>
        <small>Signer 1 accepts proposal</small><br/>
        <small>status: {this.state.success['own3-2']? <strong className="t-success">Success</strong> : this.state.fails['own3-2']? <strong className="t-fail">Failed : {this.state.fails['own3-2']}</strong> : 'awaiting'} {this.state.success['own3-2'] && <a href={`${this.state.explorer}${this.state.success['own3-2']}`} target="_blank" rel="noreferrer">{this.state.success['own3-2']}</a>}</small>
      </p>
      <p>own4: Alice transfers 500MEUR to Bob
        <br/>
        <small>status: {this.state.success['own4']? <strong className="t-success">Success</strong> : this.state.fails['own4']? <strong className="t-fail">Failed : {this.state.fails['own4']}</strong> : 'awaiting'} {this.state.success['own4'] && <a href={`${this.state.explorer}${this.state.success['own4']}`} target="_blank" rel="noreferrer">{this.state.success['own4']}</a>}</small>
      </p>
      <p>own5: Bob tries to set new master minter
        <br/>
        <small>status: {this.state.success['own5']? <strong className="t-success">Success</strong> : this.state.fails['own5']? <strong className="t-fail">Failed : {this.state.fails['own5']}</strong> : 'awaiting'} {this.state.success['own5'] && <a href={`${this.state.explorer}${this.state.success['own5']}`} target="_blank" rel="noreferrer">{this.state.success['own5']}</a>}</small>
      </p>
      <p>own6: Owner sets new master minter
        <br/>
        <small>Signer 2 creates proposal</small><br/>
        <small>status: {this.state.success['own6-1']? <strong className="t-success">Success</strong> : this.state.fails['own6-1']? <strong className="t-fail">Failed : {this.state.fails['own6-1']}</strong> : 'awaiting'} {this.state.success['own6-1'] && <a href={`${this.state.explorer}${this.state.success['own6-1']}`} target="_blank" rel="noreferrer">{this.state.success['own6-1']}</a>}</small>
        <br/>
        <small>Signer 1 accepts proposal</small><br/>
        <small>status: {this.state.success['own6-2']? <strong className="t-success">Success</strong> : this.state.fails['own6-2']? <strong className="t-fail">Failed : {this.state.fails['own6-2']}</strong> : 'awaiting'} {this.state.success['own6-2'] && <a href={`${this.state.explorer}${this.state.success['own6-2']}`} target="_blank" rel="noreferrer">{this.state.success['own6-2']}</a>}</small>
        <br/>
        <small>Signer 3 accepts proposal</small><br/>
        <small>status: {this.state.success['own6-3']? <strong className="t-success">Success</strong> : this.state.fails['own6-3']? <strong className="t-fail">Failed : {this.state.fails['own6-3']}</strong> : 'awaiting'} {this.state.success['own6-3'] && <a href={`${this.state.explorer}${this.state.success['own6-3']}`} target="_blank" rel="noreferrer">{this.state.success['own6-3']}</a>}</small>
      </p>
      <p>own7: Master minter tries to mint 1000EURL
        <br/>
        <small>Signer 3 creates proposal</small><br/>
        <small>status: {this.state.success['own7-1']? <strong className="t-success">Success</strong> : this.state.fails['own7-1']? <strong className="t-fail">Failed : {this.state.fails['own7-1']}</strong> : 'awaiting'} {this.state.success['own7-1'] && <a href={`${this.state.explorer}${this.state.success['own7-1']}`} target="_blank" rel="noreferrer">{this.state.success['own7-1']}</a>}</small>
        <br/>
        <small>Signer 1 accepts proposal</small><br/>
        <small>status: {this.state.success['own7-2']? <strong className="t-success">Success</strong> : this.state.fails['own7-2']? <strong className="t-fail">Failed : {this.state.fails['own7-2']}</strong> : 'awaiting'} {this.state.success['own7-2'] && <a href={`${this.state.explorer}${this.state.success['own7-2']}`} target="_blank" rel="noreferrer">{this.state.success['own7-2']}</a>}</small>
      </p>
      <p>own8: New master minter mints 1000EURL to Alice
        <br/>
        <small>status: {this.state.success['own8']? <strong className="t-success">Success</strong> : this.state.fails['own8']? <strong className="t-fail">Failed : {this.state.fails['own8']}</strong> : 'awaiting'} {this.state.success['own8'] && <a href={`${this.state.explorer}${this.state.success['own8']}`} target="_blank" rel="noreferrer">{this.state.success['own8']}</a>}</small>
      </p>
      <p>own9: New master minter tries to change admin
        <br/>
        <small>status: {this.state.success['own9']? <strong className="t-success">Success</strong> : this.state.fails['own9']? <strong className="t-fail">Failed : {this.state.fails['own9']}</strong> : 'awaiting'} {this.state.success['own9'] && <a href={`${this.state.explorer}${this.state.success['own9']}`} target="_blank" rel="noreferrer">{this.state.success['own9']}</a>}</small>
      </p>
      <p>own10: Owner set new administrator
        <br/>
        <small>Signer 2 creates proposal</small><br/>
        <small>status: {this.state.success['own10-1']? <strong className="t-success">Success</strong> : this.state.fails['own10-1']? <strong className="t-fail">Failed : {this.state.fails['own10-1']}</strong> : 'awaiting'} {this.state.success['own10-1'] && <a href={`${this.state.explorer}${this.state.success['own10-1']}`} target="_blank" rel="noreferrer">{this.state.success['own10-1']}</a>}</small>
        <br/>
        <small>Signer 1 accepts proposal</small><br/>
        <small>status: {this.state.success['own10-2']? <strong className="t-success">Success</strong> : this.state.fails['own10-2']? <strong className="t-fail">Failed : {this.state.fails['own10-2']}</strong> : 'awaiting'} {this.state.success['own10-2'] && <a href={`${this.state.explorer}${this.state.success['own10-2']}`} target="_blank" rel="noreferrer">{this.state.success['own10-2']}</a>}</small>
        <br/>
        <small>Signer 3 accepts proposal</small><br/>
        <small>status: {this.state.success['own10-3']? <strong className="t-success">Success</strong> : this.state.fails['own10-3']? <strong className="t-fail">Failed : {this.state.fails['own10-3']}</strong> : 'awaiting'} {this.state.success['own10-3'] && <a href={`${this.state.explorer}${this.state.success['own10-3']}`} target="_blank" rel="noreferrer">{this.state.success['own10-3']}</a>}</small>
      </p>
      <p>own11: Administrator tries set new fees manager
        <br/>
        <small>Signer 1 creates proposal</small><br/>
        <small>status: {this.state.success['own11-1']? <strong className="t-success">Success</strong> : this.state.fails['own11-1']? <strong className="t-fail">Failed : {this.state.fails['own11-1']}</strong> : 'awaiting'} {this.state.success['own11-1'] && <a href={`${this.state.explorer}${this.state.success['own11-1']}`} target="_blank" rel="noreferrer">{this.state.success['own11-1']}</a>}</small>
        <br/>
        <small>Signer 2 accepts proposal</small><br/>
        <small>status: {this.state.success['own11-2']? <strong className="t-success">Success</strong> : this.state.fails['own11-2']? <strong className="t-fail">Failed : {this.state.fails['own11-2']}</strong> : 'awaiting'} {this.state.success['own11-2'] && <a href={`${this.state.explorer}${this.state.success['own11-2']}`} target="_blank" rel="noreferrer">{this.state.success['own11-2']}</a>}</small>
      </p>
      <p>own12: Owner updates Token MEUR to KEUR
        <br/>
        <small>Signer 2 creates proposal</small><br/>
        <small>status: {this.state.success['own12-1']? <strong className="t-success">Success</strong> : this.state.fails['own12-1']? <strong className="t-fail">Failed : {this.state.fails['own12-1']}</strong> : 'awaiting'} {this.state.success['own12-1'] && <a href={`${this.state.explorer}${this.state.success['own12-1']}`} target="_blank" rel="noreferrer">{this.state.success['own12-1']}</a>}</small>
        <br/>
        <small>Signer 1 accepts proposal</small><br/>
        <small>status: {this.state.success['own12-2']? <strong className="t-success">Success</strong> : this.state.fails['own12-2']? <strong className="t-fail">Failed : {this.state.fails['own12-2']}</strong> : 'awaiting'} {this.state.success['own12-2'] && <a href={`${this.state.explorer}${this.state.success['own12-2']}`} target="_blank" rel="noreferrer">{this.state.success['own12-2']}</a>}</small>
        <br/>
        <small>Signer 3 accepts proposal</small><br/>
        <small>status: {this.state.success['own12-3']? <strong className="t-success">Success</strong> : this.state.fails['own12-3']? <strong className="t-fail">Failed : {this.state.fails['own12-3']}</strong> : 'awaiting'} {this.state.success['own12-3'] && <a href={`${this.state.explorer}${this.state.success['own12-3']}`} target="_blank" rel="noreferrer">{this.state.success['own12-3']}</a>}</small>
      </p>
      <p>
        Publising new fees manager
        <br/>
        <small>status: {this.state.success['pub_new_fees']? <strong className="t-success">Success</strong> : this.state.fails['pub_new_fees']? <strong className="t-fail">Failed</strong> : 'awaiting'} {this.state.success['pub_new_fees'] && <a href={`${this.state.explorer}${this.state.success['pub_new_fees']}`} target="_blank" rel="noreferrer">{this.state.success['pub_new_fees']}</a>}</small>
      </p>
      <p>own13: New administrator sets new fees manager
        <br/>
        <small>status: {this.state.success['own13']? <strong className="t-success">Success</strong> : this.state.fails['own13']? <strong className="t-fail">Failed : {this.state.fails['own13']}</strong> : 'awaiting'} {this.state.success['own13'] && <a href={`${this.state.explorer}${this.state.success['own13']}`} target="_blank" rel="noreferrer">{this.state.success['own13']}</a>}</small>
      </p>

      <h3>9. Gasless transfers</h3>
      <p>gas1: Relay tries to broadcast gasless transfer with wrong signature
        <br/>
        <small>status: {this.state.success['gas1']? <strong className="t-success">Success</strong> : this.state.fails['gas1']? <strong className="t-fail">Failed : {this.state.fails['gas1']}</strong> : 'awaiting'} {this.state.success['gas1'] && <a href={`${this.state.explorer}${this.state.success['gas1']}`} target="_blank" rel="noreferrer">{this.state.success['gas1']}</a>}</small>
      </p>
      <p>gas2: Relay broadcasts gasless transfer from Alice to Bob & Carl for 5 and 4 EURL respectively
        <br/>
        <small>status: {this.state.success['gas2']? <strong className="t-success">Success</strong> : this.state.fails['gas2']? <strong className="t-fail">Failed : {this.state.fails['gas2']}</strong> : 'awaiting'} {this.state.success['gas2'] && <a href={`${this.state.explorer}${this.state.success['gas2']}`} target="_blank" rel="noreferrer">{this.state.success['gas2']}</a>}</small>
      </p>
      <p>gas3: Relay tries to broadcast the same transfer
        <br/>
        <small>status: {this.state.success['gas3']? <strong className="t-success">Success</strong> : this.state.fails['gas3']? <strong className="t-fail">Failed : {this.state.fails['gas3']}</strong> : 'awaiting'} {this.state.success['gas3'] && <a href={`${this.state.explorer}${this.state.success['gas3']}`} target="_blank" rel="noreferrer">{this.state.success['gas3']}</a>}</small>
      </p>
      <p>gas4: Relay tries to broadcast a gasless transfer with over funds
        <br/>
        <small>status: {this.state.success['gas4']? <strong className="t-success">Success</strong> : this.state.fails['gas4']? <strong className="t-fail">Failed : {this.state.fails['gas4']}</strong> : 'awaiting'} {this.state.success['gas4'] && <a href={`${this.state.explorer}${this.state.success['gas4']}`} target="_blank" rel="noreferrer">{this.state.success['gas4']}</a>}</small>
      </p>
      <p>gas5: Relay broadcasts gasless transfer from Alice to Eric (new account) for 100EURL
        <br/>
        <small>status: {this.state.success['gas5']? <strong className="t-success">Success</strong> : this.state.fails['gas5']? <strong className="t-fail">Failed : {this.state.fails['gas5']}</strong> : 'awaiting'} {this.state.success['gas5'] && <a href={`${this.state.explorer}${this.state.success['gas5']}`} target="_blank" rel="noreferrer">{this.state.success['gas5']}</a>}</small>
      </p>
      <p>gas6: Relay tries to gasless transfer from Alice to Eric (new account) for 100EURL supporting fees
        <br/>
        <small>status: {this.state.success['gas6']? <strong className="t-success">Success</strong> : this.state.fails['gas6']? <strong className="t-fail">Failed : {this.state.fails['gas6']}</strong> : 'awaiting'} {this.state.success['gas6'] && <a href={`${this.state.explorer}${this.state.success['gas6']}`} target="_blank" rel="noreferrer">{this.state.success['gas6']}</a>}</small>
      </p>
      <p>gas7: New master minter mints 10EURL to Relay
        <br/>
        <small>status: {this.state.success['gas7']? <strong className="t-success">Success</strong> : this.state.fails['gas7']? <strong className="t-fail">Failed : {this.state.fails['gas7']}</strong> : 'awaiting'} {this.state.success['gas7'] && <a href={`${this.state.explorer}${this.state.success['gas7']}`} target="_blank" rel="noreferrer">{this.state.success['gas7']}</a>}</small>
      </p>
      <p>gas8: Relay broadcast gasless transfer from Alice to Eric for 100EURL supporting fees
        <br/>
        <small>status: {this.state.success['gas8']? <strong className="t-success">Success</strong> : this.state.fails['gas8']? <strong className="t-fail">Failed : {this.state.fails['gas8']}</strong> : 'awaiting'} {this.state.success['gas8'] && <a href={`${this.state.explorer}${this.state.success['gas8']}`} target="_blank" rel="noreferrer">{this.state.success['gas8']}</a>}</small>
      </p>

      <h3>10. Permits</h3>
      <p>permit01: Relay set a permit from Alice, for a transfer of 20 to Bob
        <br/>
        <small>status: {this.state.success['permit01']? <strong className="t-success">Success</strong> : this.state.fails['permit01']? <strong className="t-fail">Failed : {this.state.fails['permit01']}</strong> : 'awaiting'} {this.state.success['permit01'] && <a href={`${this.state.explorer}${this.state.success['permit01']}`} target="_blank" rel="noreferrer">{this.state.success['permit01']}</a>}</small>
      </p>
      <p>permit02: Relay tries to consume permit (wrong amount)
        <br/>
        <small>status: {this.state.success['permit02']? <strong className="t-success">Success</strong> : this.state.fails['permit02']? <strong className="t-fail">Failed : {this.state.fails['permit02']}</strong> : 'awaiting'} {this.state.success['permit02'] && <a href={`${this.state.explorer}${this.state.success['permit02']}`} target="_blank" rel="noreferrer">{this.state.success['permit02']}</a>}</small>
      </p>
      <p>permit03: Relay consumes permit
        <br/>
        <small>status: {this.state.success['permit03']? <strong className="t-success">Success</strong> : this.state.fails['permit03']? <strong className="t-fail">Failed : {this.state.fails['permit03']}</strong> : 'awaiting'} {this.state.success['permit03'] && <a href={`${this.state.explorer}${this.state.success['permit03']}`} target="_blank" rel="noreferrer">{this.state.success['permit03']}</a>}</small>
      </p>
      <p>permit04: Relay set a permit from Alice, for a transfer of 10 to Bob
        <br/>
        <small>status: {this.state.success['permit04']? <strong className="t-success">Success</strong> : this.state.fails['permit04']? <strong className="t-fail">Failed : {this.state.fails['permit04']}</strong> : 'awaiting'} {this.state.success['permit04'] && <a href={`${this.state.explorer}${this.state.success['permit04']}`} target="_blank" rel="noreferrer">{this.state.success['permit04']}</a>}</small>
      </p>
      <p>permit05: Bob tries to set new expirency permit for Alice
        <br/>
        <small>status: {this.state.success['permit05']? <strong className="t-success">Success</strong> : this.state.fails['permit05']? <strong className="t-fail">Failed : {this.state.fails['permit05']}</strong> : 'awaiting'} {this.state.success['permit05'] && <a href={`${this.state.explorer}${this.state.success['permit05']}`} target="_blank" rel="noreferrer">{this.state.success['permit05']}</a>}</small>
      </p>
      <p>permit06: Alice sets new expirency permit
        <br/>
        <small>status: {this.state.success['permit06']? <strong className="t-success">Success</strong> : this.state.fails['permit06']? <strong className="t-fail">Failed : {this.state.fails['permit06']}</strong> : 'awaiting'} {this.state.success['permit06'] && <a href={`${this.state.explorer}${this.state.success['permit06']}`} target="_blank" rel="noreferrer">{this.state.success['permit06']}</a>}</small>
      </p>
      <p>permit07: Relay consumes permit
        <br/>
        <small>status: {this.state.success['permit07']? <strong className="t-success">Success</strong> : this.state.fails['permit07']? <strong className="t-fail">Failed : {this.state.fails['permit07']}</strong> : 'awaiting'} {this.state.success['permit07'] && <a href={`${this.state.explorer}${this.state.success['permit07']}`} target="_blank" rel="noreferrer">{this.state.success['permit07']}</a>}</small>
      </p>
    </div>
  );
}

export default App;
