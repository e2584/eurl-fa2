import smartpy as sp

# A collection of error messages used in the contract.
class Owner_Error:
    def make(s): return ("OWNER_" + s)

    unreferencedSigner              = make("UnreferencedSigner")
    alreadyReferenced               = make("ProposalIdAlreadyReferenced")
    unreferencedProposal            = make("ProposalIdUnreferenced")
    openLimit                       = make("OpenProposalsLimitReached")
    proposalClosed                  = make("ProposalAlreadyClosed")
    unauthorizedOp                  = make("OperationUnauthorized")
    proposalAccepted                = make("ProposalAlreadyAccepted")

class Owner(sp.Contract):
    def __init__(self, signers):
        self.init_type(t = sp.TRecord(signers = sp.TSet(sp.TAddress), proposals = sp.TBigMap(sp.TString, 
            sp.TRecord(address = sp.TAddress, operation = sp.TString, contractAddr = sp.TAddress, approvals = sp.TSet(sp.TAddress), 
            rejects = sp.TSet(sp.TAddress), status = sp.TBool, meta_data = sp.TMap(sp.TString, sp.TBytes), 
            token_id = sp.TNat)), authOps = sp.TSet(sp.TString), restriction =  sp.TNat, 
            openProposals = sp.TNat))
        self.init(signers = signers, proposals = sp.big_map(), authOps = {"create", "set_master_minter", "set_administrator", "set_owner", "update_metadata"}, 
                  restriction = 3, openProposals = 0)
        
    '''
    Verification utils
    '''
    
    def checkSigner(self):
        sp.verify(self.data.signers.contains(sp.sender), message = Owner_Error.unreferencedSigner)
    
    def checkNewProposal(self, proposalId):
        sp.verify(~self.data.proposals.contains(proposalId), message = Owner_Error.alreadyReferenced)
    
    def checkProposal(self, proposalId):
        sp.verify(self.data.proposals.contains(proposalId), message = Owner_Error.unreferencedProposal)
        
    def checkRestriction(self):
        sp.verify(self.data.restriction > self.data.openProposals, message = Owner_Error.openLimit)
    
    def checkOpen(self, proposalId):
        sp.verify(~self.data.proposals[proposalId].status, message = Owner_Error.proposalClosed)
    
    def checkOperation(self, operation):
        sp.verify(self.data.authOps.contains(operation), message = Owner_Error.unauthorizedOp)
        
    '''
    Main entrypoints
    '''

    @sp.entry_point
    def createProposal(self, params):
        sp.set_type(params, sp.TRecord(address = sp.TAddress, contractAddr = sp.TAddress, operation = sp.TString, 
                                       proposalId = sp.TString))
        self.checkSigner()
        self.checkRestriction()
        self.checkOperation(params.operation)
        self.checkNewProposal(params.proposalId)
        self.data.proposals[params.proposalId] = sp.record(address = params.address, operation = params.operation, 
                                                 contractAddr = params.contractAddr, approvals = sp.set(t = sp.TAddress), 
                                                 rejects = sp.set(t = sp.TAddress), status = False, meta_data = {}, 
                                                 token_id = 0)
        self.data.proposals[params.proposalId].approvals.add(sp.sender)
        self.data.openProposals += 1

    @sp.entry_point
    def manageToken(self, params):
        sp.set_type(params, sp.TRecord(contractAddr = sp.TAddress, operation = sp.TString, proposalId = sp.TString, 
                                       meta_data = sp.TMap(sp.TString, sp.TBytes), token_id = sp.TNat))
        self.checkSigner()
        self.checkRestriction()
        self.checkOperation(params.operation)
        self.checkNewProposal(params.proposalId)
        self.data.proposals[params.proposalId] = sp.record(address = sp.sender, operation = params.operation, 
                                                           contractAddr = params.contractAddr, approvals = sp.set(t = sp.TAddress), 
                                                           rejects = sp.set(t = sp.TAddress), status = False, 
                                                           meta_data = params.meta_data, token_id = params.token_id)
        self.data.proposals[params.proposalId].approvals.add(sp.sender)
        self.data.openProposals += 1

    @sp.entry_point
    def accept(self, params):
        sp.set_type(params, sp.TString)
        self.checkSigner()
        self.checkProposal(params)
        self.checkOpen(params)
        sp.verify(~self.data.proposals[params].approvals.contains(sp.sender), message = Owner_Error.proposalAccepted)
        self.data.proposals[params].approvals.add(sp.sender)
        sp.if sp.len(self.data.proposals[params].approvals) == sp.len(self.data.signers):
            sp.if self.data.proposals[params].operation == "set_master_minter":
                self._processOperation(params, "set_master_minter")
            sp.else:
                sp.if self.data.proposals[params].operation == "set_administrator":
                    self._processOperation(params, "set_administrator")
                sp.else:
                    sp.if self.data.proposals[params].operation == "update_metadata":
                        self._processToken(params, "update_metadata")
                    sp.else:
                        self._processToken(params, "create")
            self.data.proposals[params].status = True
            self.data.openProposals = sp.as_nat(self.data.openProposals - 1)

    @sp.entry_point
    def reject(self, params):
        sp.set_type(params, sp.TString)
        self.checkSigner()
        self.checkProposal(params)
        self.checkOpen(params)
        self.data.proposals[params].rejects.add(sp.sender)
        sp.if self.data.proposals[params].approvals.contains(sp.sender):
            self.data.proposals[params].approvals.remove(sp.sender)
        self.data.proposals[params].status = True
        self.data.openProposals = sp.as_nat(self.data.openProposals - 1)
        
    '''
    Lugh coin operations
    '''
    
    def _processOperation(self, proposalId, ePoint):
        transferParamsRecord = sp.record(address = self.data.proposals[proposalId].address)
        c = sp.contract(
                t = sp.TRecord(address = sp.TAddress), 
                address = self.data.proposals[proposalId].contractAddr, 
                entry_point = ePoint
                ).open_some()
        sp.transfer(transferParamsRecord, sp.mutez(0), c)
    
    def _processToken(self, proposalId, ePoint):
        transferParamsRecord = sp.record(meta_data = self.data.proposals[proposalId].meta_data, token_id = self.data.proposals[proposalId].token_id)
        c = sp.contract(
                t = sp.TRecord(meta_data = sp.TMap(sp.TString, sp.TBytes), token_id = sp.TNat), 
                address = self.data.proposals[proposalId].contractAddr, 
                entry_point = ePoint
                ).open_some()
        sp.transfer(transferParamsRecord, sp.mutez(0), c)

if "templates" not in __name__:
    @sp.add_test(name = "MultiSig")
    def test():

        scenario = sp.test_scenario()
        scenario.h1("Owner Contract")

        # sp.test_account generates ED25519 key-pairs deterministically:
        firstSigner = sp.test_account("firstSigner")
        secondSigner = sp.test_account("secondSigner")
        thirdSigner = sp.test_account("thirdSigner")
        new_minter = sp.test_account("newMinter")
        new_administrator = sp.test_account("newAdministrator")
        new_reserve = sp.test_account("newReserve")
        contractAddr = sp.address("KT1-distantContractToCall-1234")

        # Let's display the accounts:
        scenario.h2("Accounts")
        scenario.show([firstSigner, secondSigner, thirdSigner, new_minter, new_administrator, new_reserve])

        c1 = Owner(signers = sp.set([firstSigner.address, secondSigner.address, thirdSigner.address]))

        scenario += c1
        
        scenario.h2("#o01 - firstSigner tries to create a transfer proposal")
        scenario += c1.createProposal(proposalId = "om01", address = new_minter.address, operation = "set_whatever", contractAddr = contractAddr).run(sender = firstSigner, valid = False)
        scenario.h2("#o02 - firstSigner creates a set_master_minter proposal")
        scenario += c1.createProposal(proposalId = "om01", address = new_minter.address, operation = "set_master_minter", contractAddr = contractAddr).run(sender = firstSigner)
        scenario.h2("#o03 - firstSigner accepts proposal")
        scenario += c1.accept("om01").run(sender = firstSigner, valid=False)
        scenario.h2("#o04 - secondSigner accepts unreferenced proposal")
        scenario += c1.accept("om02").run(sender = secondSigner, valid=False)
        scenario.h2("#o05 - secondSigner accepts proposal")
        scenario += c1.accept("om01").run(sender = secondSigner)
        scenario.h2("#o06 - thirdSigner accepts proposal")
        scenario += c1.accept("om01").run(sender = thirdSigner)
        scenario.h2("#o07 - secondSigner tries to create a set_administrator proposal")
        scenario += c1.createProposal(proposalId = "om01", address = new_administrator.address, operation = "set_administrator", contractAddr = contractAddr).run(sender = secondSigner, valid = False)
        scenario.h2("#o08 - secondSigner creates a set_administrator proposal")
        scenario += c1.createProposal(proposalId = "oa01", address = new_administrator.address, operation = "set_administrator", contractAddr = contractAddr).run(sender = secondSigner)
        scenario.h2("#o09 - firstSigner rejects proposal")
        scenario += c1.reject("oa01").run(sender = firstSigner)
        scenario.h2("#o10 - thirdSigner accepts proposal")
        scenario += c1.accept("oa01").run(sender = thirdSigner, valid=False)
        scenario.h2("#o11 - firstSigner accepts proposal")
        scenario += c1.accept("oa01").run(sender = firstSigner, valid=False)
        scenario.h2("#o15 - firstSigner creates a set_master_minter proposal")
        scenario += c1.createProposal(proposalId = "om02", address = new_administrator.address, operation = "set_master_minter", contractAddr = contractAddr).run(sender = firstSigner)
        scenario.h2("#o16 - secondSigner rejects proposal")
        scenario += c1.reject("om02").run(sender = secondSigner)
        scenario.h2("#o17 - thirdSigner rejects proposal")
        scenario += c1.reject("om02").run(sender = thirdSigner, valid=False)
        scenario.h2("#o18 - firstSigner rejects proposal")
        scenario += c1.reject("om02").run(sender = firstSigner, valid=False)
        
        scenario.h1("Tests overflowding")
        scenario.h2("#o19 - firstSigner creates a first set_master_minter proposal")
        scenario += c1.createProposal(proposalId = "over01", address = new_minter.address, operation = "set_master_minter", contractAddr = contractAddr).run(sender = firstSigner)
        scenario.h2("#o20 - firstSigner creates a second set_master_minter proposal")
        scenario += c1.createProposal(proposalId = "over02", address = new_minter.address, operation = "set_master_minter", contractAddr = contractAddr).run(sender = firstSigner)
        scenario.h2("#o21 - firstSigner creates a third set_master_minter proposal")
        scenario += c1.createProposal(proposalId = "over03", address = new_minter.address, operation = "set_master_minter", contractAddr = contractAddr).run(sender = firstSigner)
        scenario.h2("#o22 - firstSigner creates a fourth set_master_minter proposal")
        scenario += c1.createProposal(proposalId = "over04", address = new_minter.address, operation = "set_master_minter", contractAddr = contractAddr).run(sender = firstSigner, valid = False)

        token_meta_data = sp.map(l = {
            "decimals" : sp.utils.bytes_of_string("%d" % 6),
            "is_boolean_amount" : sp.utils.bytes_of_string("%r" % False),
            "is_transferable" : sp.utils.bytes_of_string("%r" % True),
            "name" : sp.utils.bytes_of_string("Some other euro coin"),
            "should_prefer_symbol": sp.utils.bytes_of_string("%r" % True),
            "symbol" : sp.utils.bytes_of_string("KEUR"),
            "thumbnailUri" : sp.utils.bytes_of_string("ipfs://QmcqsYQn8pTxQr3P1dYpgYxQa6GQPmoBTSWQ8bpuFEuaqe")
        })


        scenario.h2("#o23 - thirdSigner rejects proposal")
        scenario += c1.reject("over03").run(sender = thirdSigner)

        scenario.h2("#cr01 - firstSigner creates a new token proposal")
        scenario += c1.manageToken(proposalId = "cr01", contractAddr = contractAddr, operation = "create", meta_data = token_meta_data, token_id = 1).run(sender = firstSigner)
        scenario.h2("#cr02 - secondSigner accepts proposal")
        scenario += c1.accept("cr01").run(sender = secondSigner)
        scenario.h2("#cr03 - thirdSigner accepts proposal")
        scenario += c1.accept("cr01").run(sender = thirdSigner)