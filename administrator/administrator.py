import smartpy as sp

# A collection of error messages used in the contract.
class Administrator_Error:
    def make(s): return ("ADMINISTRATOR_" + s)

    unreferencedSigner              = make("UnreferencedSigner")
    alreadyReferenced               = make("ProposalIdAlreadyReferenced")
    unreferencedProposal            = make("ProposalIdUnreferenced")
    openLimit                       = make("OpenProposalsLimitReached")
    proposalClosed                  = make("ProposalAlreadyClosed")
    unauthorizedOp                  = make("OperationUnauthorized")
    proposalAccepted                = make("ProposalAlreadyAccepted")

class Administrator(sp.Contract):
    def __init__(self, signers):
        self.init_type(t = sp.TRecord(signers = sp.TSet(sp.TAddress), proposals = sp.TBigMap(sp.TString, 
            sp.TRecord(f = sp.TAddress, amount = sp.TNat, t = sp.TAddress, state = sp.TBool, 
            operation = sp.TString, contractAddr = sp.TAddress, approvals = sp.TSet(sp.TAddress), 
            rejects = sp.TSet(sp.TAddress), tokenId = sp.TNat, status = sp.TBool)), 
            authOps = sp.TSet(sp.TString), limit =  sp.TNat, restriction =  sp.TNat, 
            openProposals = sp.TNat))
        self.init(signers = signers, proposals = sp.big_map(), authOps = {"set_fees_faucet", "set_fees_manager", 
            "set_lock", "set_rights_manager", "updateGasFee", "updateGaslessFee", "updateStorageFee", "updateThreshold"}, 
            limit = 2, restriction = 40, openProposals = 0)
        
    '''
    Verification utils
    '''
    
    def checkSigner(self):
        sp.verify(self.data.signers.contains(sp.sender), message = Administrator_Error.unreferencedSigner)
    
    def checkNewProposal(self, proposalId):
        sp.verify(~self.data.proposals.contains(proposalId), message = Administrator_Error.alreadyReferenced)
    
    def checkProposal(self, proposalId):
        sp.verify(self.data.proposals.contains(proposalId), message = Administrator_Error.unreferencedProposal)
    
    def checkRestriction(self):
        sp.verify(self.data.restriction > self.data.openProposals, message = Administrator_Error.openLimit)
    
    def checkOpen(self, proposalId):
        sp.verify(~self.data.proposals[proposalId].status, message = Administrator_Error.proposalClosed)
    
    def checkOperation(self, operation):
        sp.verify(self.data.authOps.contains(operation), message = Administrator_Error.unauthorizedOp)

    def checkConditions(self, proposalId):
        self.checkSigner()
        self.checkRestriction()
        self.checkNewProposal(proposalId)

    def closeCreation(self, proposalId):
        self.data.proposals[proposalId].approvals.add(sp.sender)
        self.data.openProposals += 1
        
    '''
    Main entrypoints
    '''

    @sp.entry_point
    def createAddressProposal(self, params):
        sp.set_type(params, sp.TRecord(address = sp.TAddress, contractAddr = sp.TAddress, 
            operation = sp.TString, proposalId = sp.TString, state = sp.TBool, tokenId = sp.TNat))
        self.checkConditions(params.proposalId)
        self.checkOperation(params.operation)
        self.data.proposals[params.proposalId] = sp.record(f = sp.sender, amount = 0, 
            t = params.address, state = params.state, operation = params.operation, 
            contractAddr = params.contractAddr, approvals = sp.set(t = sp.TAddress), 
            rejects = sp.set(t = sp.TAddress), tokenId = params.tokenId, status = False)
        self.closeCreation(params.proposalId)

    @sp.entry_point
    def createPauseProposal(self, params):
        sp.set_type(params, sp.TRecord(contractAddr = sp.TAddress, proposalId = sp.TString, 
            state = sp.TBool, tokenId = sp.TNat))
        self.checkConditions(params.proposalId)
        self.data.proposals[params.proposalId] = sp.record(f = sp.sender, amount = 0, 
            t = params.contractAddr, state = params.state, operation = "setPause", 
            contractAddr = params.contractAddr, approvals = sp.set(t = sp.TAddress), 
            rejects = sp.set(t = sp.TAddress), tokenId = params.tokenId, status = False)
        self.closeCreation(params.proposalId)

    @sp.entry_point
    def createTransferProposal(self, params):
        sp.set_type(params, sp.TRecord(contractAddr = sp.TAddress, proposalId = sp.TString, 
            f = sp.TAddress, t = sp.TAddress, amount = sp.TNat, tokenId = sp.TNat))
        self.checkConditions(params.proposalId)
        self.data.proposals[params.proposalId] = sp.record(f = params.f, amount = params.amount, 
            t = params.t, contractAddr = params.contractAddr, state = True, operation = "transfer", 
            approvals = sp.set(t = sp.TAddress), rejects = sp.set(t = sp.TAddress), 
            tokenId = params.tokenId, status = False)
        self.closeCreation(params.proposalId)

    @sp.entry_point
    def createFeesManagerProposal(self, params):
        sp.set_type(params, sp.TRecord(contractAddr = sp.TAddress, operation = sp.TString, 
            proposalId = sp.TString, value = sp.TNat, tokenId = sp.TNat))
        self.checkConditions(params.proposalId)
        self.checkOperation(params.operation)
        self.data.proposals[params.proposalId] = sp.record(f = sp.sender, amount = params.value, 
            t = params.contractAddr, contractAddr = params.contractAddr, state = True, 
            operation = params.operation, approvals = sp.set(t = sp.TAddress), 
            rejects = sp.set(t = sp.TAddress), tokenId = params.tokenId, status = False)
        self.closeCreation(params.proposalId)

    @sp.entry_point
    def createSafeFeeProposal(self, params):
        sp.set_type(params, sp.TRecord(contractAddr = sp.TAddress, proposalId = sp.TString, value = sp.TNat))
        self.checkConditions(params.proposalId)
        self.data.proposals[params.proposalId] = sp.record(f = sp.sender, amount = params.value, 
            t = params.contractAddr, contractAddr = params.contractAddr, state = True, 
            operation = "set_safe_fee", approvals = sp.set(t = sp.TAddress), 
            rejects = sp.set(t = sp.TAddress), tokenId = 0, status = False)
        self.closeCreation(params.proposalId)

    @sp.entry_point
    def createSetOptionProposal(self, params):
        sp.set_type(params, sp.TRecord(address = sp.TAddress, contractAddr = sp.TAddress, 
            operation = sp.TString, proposalId = sp.TString))
        self.checkConditions(params.proposalId)
        self.checkOperation(params.operation)
        self.data.proposals[params.proposalId] = sp.record(f = sp.sender, amount = 0, 
            t = params.address, state = True, operation = params.operation, 
            contractAddr = params.contractAddr, approvals = sp.set(t = sp.TAddress), 
            rejects = sp.set(t = sp.TAddress), tokenId = 0, status = False)
        self.closeCreation(params.proposalId)

    @sp.entry_point
    def createRemoveOptionProposal(self, params):
        sp.set_type(params, sp.TRecord(contractAddr = sp.TAddress, 
            operation = sp.TString, proposalId = sp.TString))
        self.checkConditions(params.proposalId)
        self.checkOperation(params.operation)
        self.data.proposals[params.proposalId] = sp.record(f = sp.sender, amount = 0, 
            t = params.contractAddr, state = False, operation = params.operation, 
            contractAddr = params.contractAddr, approvals = sp.set(t = sp.TAddress), 
            rejects = sp.set(t = sp.TAddress), tokenId = 0, status = False)
        self.closeCreation(params.proposalId)

    @sp.entry_point
    def accept(self, params):
        sp.set_type(params, sp.TString)
        self.checkSigner()
        self.checkProposal(params)
        self.checkOpen(params)
        sp.verify(~self.data.proposals[params].approvals.contains(sp.sender), message = Administrator_Error.proposalAccepted)
        self.data.proposals[params].approvals.add(sp.sender)
        sp.if sp.len(self.data.proposals[params].approvals) == self.data.limit:
            sp.if self.data.proposals[params].operation == "set_fees_manager":
                sp.if self.data.proposals[params].state:
                    self._setOptionalAddress(params, "set_fees_manager")
                sp.else:
                    self._removeOptionalAddress(params, "set_fees_manager")
            sp.else:
                sp.if self.data.proposals[params].operation == "set_fees_faucet":
                    self._processAddress(params, "set_fees_faucet")
                sp.else:
                    sp.if self.data.proposals[params].operation == "set_rights_manager":
                        sp.if self.data.proposals[params].state:
                            self._setOptionalAddress(params, "set_rights_manager")
                        sp.else:
                            self._removeOptionalAddress(params, "set_rights_manager")
                    sp.else:
                        sp.if self.data.proposals[params].operation == "set_lock":
                            self._setLock(params)
                        sp.else:
                            sp.if self.data.proposals[params].operation == "transfer":
                                self._transfer(params)
                            sp.else:
                                sp.if self.data.proposals[params].operation == "updateGasFee":
                                    self._processFee(params, "updateGasFee")
                                sp.else:
                                    sp.if self.data.proposals[params].operation == "updateGaslessFee":
                                        self._processFee(params, "updateGaslessFee")
                                    sp.else:
                                        sp.if self.data.proposals[params].operation == "updateStorageFee":
                                            self._processFee(params, "updateStorageFee")
                                        sp.else:
                                            sp.if self.data.proposals[params].operation == "updateThreshold":
                                                self._processFee(params, "updateThreshold")
                                            sp.else:
                                                sp.if self.data.proposals[params].operation == "set_safe_fee":
                                                    self._processSetSafeFee(params)
                                                sp.else:
                                                    self._setPause(params)
            self.data.proposals[params].status = True
            self.data.openProposals = sp.as_nat(self.data.openProposals - 1)

    @sp.entry_point
    def reject(self, params):
        sp.set_type(params, sp.TString)
        self.checkSigner()
        self.checkProposal(params)
        self.checkOpen(params)
        self.data.proposals[params].rejects.add(sp.sender)
        sp.if self.data.proposals[params].approvals.contains(sp.sender):
            self.data.proposals[params].approvals.remove(sp.sender)
        self.data.proposals[params].status = True
        self.data.openProposals = sp.as_nat(self.data.openProposals - 1)
        
    '''
    Contract operations
    '''
    
    def _processAddress(self, proposalId, ePoint):
        transferParamsRecord = sp.record(address = self.data.proposals[proposalId].t)
        c = sp.contract(
                t = sp.TRecord(address = sp.TAddress), 
                address = self.data.proposals[proposalId].contractAddr, 
                entry_point = ePoint
                ).open_some()
        sp.transfer(transferParamsRecord, sp.mutez(0), c)
    
    def _setOptionalAddress(self, proposalId, ePoint):
        transferParamsRecord = sp.record(address = sp.some(self.data.proposals[proposalId].t))
        c = sp.contract(
                t = sp.TRecord(address = sp.TOption(sp.TAddress)), 
                address = self.data.proposals[proposalId].contractAddr, 
                entry_point = ePoint
                ).open_some()
        sp.transfer(transferParamsRecord, sp.mutez(0), c)
    
    def _removeOptionalAddress(self, proposalId, ePoint):
        transferParamsRecord = sp.record(address = sp.none)
        c = sp.contract(
                t = sp.TRecord(address = sp.TOption(sp.TAddress)),
                address = self.data.proposals[proposalId].contractAddr, 
                entry_point = ePoint
                ).open_some()
        sp.transfer(transferParamsRecord, sp.mutez(0), c)
    
    def _processFee(self, proposalId, ePoint):
        transferParamsRecord = sp.record(tokenId = self.data.proposals[proposalId].tokenId, 
                                         value = self.data.proposals[proposalId].amount)
        c = sp.contract(
                t = sp.TRecord(tokenId = sp.TNat, value = sp.TNat), 
                address = self.data.proposals[proposalId].contractAddr, 
                entry_point = ePoint
                ).open_some()
        sp.transfer(transferParamsRecord, sp.mutez(0), c)
    
    def _processSetSafeFee(self, proposalId):
        transferParamsRecord = self.data.proposals[proposalId].amount
        c = sp.contract(
                t = sp.TNat, 
                address = self.data.proposals[proposalId].contractAddr, 
                entry_point = "set_safe_fee"
                ).open_some()
        sp.transfer(transferParamsRecord, sp.mutez(0), c)
    
    def _setLock(self, proposalId):
        transferParamsRecord = sp.record(address = self.data.proposals[proposalId].t, 
                                         lock = self.data.proposals[proposalId].state, 
                                         token_id = self.data.proposals[proposalId].tokenId)
        c = sp.contract(
                t = sp.TRecord(address = sp.TAddress, lock = sp.TBool, token_id = sp.TNat), 
                address = self.data.proposals[proposalId].contractAddr, 
                entry_point = "set_lock"
                ).open_some()
        sp.transfer(transferParamsRecord, sp.mutez(0), c)
    
    def _setPause(self, proposalId):
        transferParamsRecord = sp.record(pause = self.data.proposals[proposalId].state)
        c = sp.contract(
                t = sp.TRecord(pause = sp.TBool), 
                address = self.data.proposals[proposalId].contractAddr, 
                entry_point = "set_pause"
                ).open_some()
        sp.transfer(transferParamsRecord, sp.mutez(0), c)
    
    def _transfer(self, proposalId):
        tx_type = sp.TRecord(amount = sp.TNat,
                             to_ = sp.TAddress,
                             token_id = sp.TNat)
        tx_type = tx_type.layout(
                ("to_", ("token_id", "amount"))
            )
        transfer_type = sp.TRecord(from_ = sp.TAddress,
                                   txs = sp.TList(tx_type))
        transfers_type = sp.TRecord(item = sp.TList(transfer_type))
        transferParamsRecord = sp.record(item = [sp.record(
                                            from_ = self.data.proposals[proposalId].f, 
                                            txs = [sp.record(
                                                              amount = self.data.proposals[proposalId].amount, 
                                                              to_ = self.data.proposals[proposalId].t, 
                                                              token_id = self.data.proposals[proposalId].tokenId
                                                  )]
                                         )])
        c = sp.contract(
                t = transfers_type, 
                address = self.data.proposals[proposalId].contractAddr, 
                entry_point = "transfer"
                ).open_some()
        sp.transfer(transferParamsRecord, sp.mutez(0), c)

if "templates" not in __name__:
    @sp.add_test(name = "Administrator")
    def test():

        scenario = sp.test_scenario()
        scenario.h1("Administrator Contract")

        # sp.test_account generates ED25519 key-pairs deterministically:
        firstSigner = sp.test_account("firstSigner")
        secondSigner = sp.test_account("secondSigner")
        thirdSigner = sp.test_account("thirdSigner")
        alice = sp.test_account("Alice")
        bob = sp.test_account("Bob")
        john = sp.test_account("John")
        reserve = sp.test_account("Reserve")
        contractAddr = sp.address("KT1-distantContractToCall-1234")
        rightsAddr = sp.address("KT1-rightsManager-1234")

        # Let's display the accounts:
        scenario.h2("Accounts")
        scenario.show([firstSigner, secondSigner, thirdSigner, alice, bob, reserve])

        c1 = Administrator(signers = sp.set([firstSigner.address, secondSigner.address, thirdSigner.address]))

        scenario += c1
        
        scenario.h2("#a01 - firstSigner tries to create a setAdministrator proposal")
        scenario += c1.createAddressProposal(proposalId = "af01", address = alice.address, state = True, operation = "set_dministrator", contractAddr = contractAddr, tokenId = 0).run(sender = firstSigner, valid = False)
        scenario.h2("#a02 - firstSigner creates a setFeesFaucet proposal")
        scenario += c1.createAddressProposal(proposalId = "af01", address = alice.address, operation = "set_fees_faucet", state = True, contractAddr = contractAddr, tokenId = 0).run(sender = firstSigner)
        scenario.h2("#a03 - firstSigner accepts proposal")
        scenario += c1.accept("af01").run(sender = firstSigner, valid=False)
        scenario.h2("#a04 - secondSigner rejects proposal")
        scenario += c1.reject("af01").run(sender = secondSigner)
        scenario.h2("#a05 - thirdSigner accepts proposal")
        scenario += c1.accept("af01").run(sender = thirdSigner, valid=False)
        scenario.h2("#a06 - thirdSigner tries to create a setLock proposal")
        scenario += c1.createAddressProposal(proposalId = "af01", address = bob.address, state = False, operation = "set_lock", contractAddr = contractAddr, tokenId = 0).run(sender = thirdSigner, valid = False)
        scenario.h2("#a07 - thirdSigner creates setLock a proposal")
        scenario += c1.createAddressProposal(proposalId = "al01", address = bob.address, state = False, operation = "set_lock", contractAddr = contractAddr, tokenId = 0).run(sender = thirdSigner)
        scenario.h2("#a08 - secondSigner accepts proposal")
        scenario += c1.accept("al01").run(sender = secondSigner)
        scenario.h2("#a09 - firstSigner accepts proposal")
        scenario += c1.accept("al01").run(sender = firstSigner, valid=False)
        scenario.h2("#a10 - firstSigner creates a transfer proposal")
        scenario += c1.createTransferProposal(proposalId = "at01", f = alice.address, t = bob.address, amount = 10000, contractAddr = contractAddr, tokenId = 0).run(sender = firstSigner)
        scenario.h2("#a11 - secondSigner rejects proposal")
        scenario += c1.reject("at01").run(sender = secondSigner)
        scenario.h2("#a12 - firstSigner rejects proposal")
        scenario += c1.reject("at01").run(sender = firstSigner, valid=False)
        scenario.h2("#a13 - firstSigner creates a transfer proposal")
        scenario += c1.createTransferProposal(proposalId = "at02", f = bob.address, t = reserve.address, amount = 5000, contractAddr = contractAddr, tokenId = 0).run(sender = firstSigner)
        scenario.h2("#a14 - secondSigner accepts proposal")
        scenario += c1.accept("at02").run(sender = secondSigner)
        scenario.h2("#a15 - secondSigner creates a pause proposal")
        scenario += c1.createPauseProposal(proposalId = "ap01", state = True, contractAddr = contractAddr, tokenId = 0).run(sender = secondSigner)
        scenario.h2("#a16 - thirdSigner accepts proposal")
        scenario += c1.accept("ap01").run(sender = thirdSigner)
        scenario.h2("#a17 - firstSigner creates a transfer proposal for not referenced address")
        scenario += c1.createTransferProposal(proposalId = "at03", f = john.address, t = reserve.address, amount = 5000, contractAddr = contractAddr, tokenId = 0).run(sender = firstSigner)
        scenario.h2("#a18 - secondSigner accepts proposal")
        scenario += c1.accept("at03").run(sender = secondSigner)
        scenario.h2("#a19 - firstSigner creates an over transfer proposal for Bob")
        scenario += c1.createTransferProposal(proposalId = "at04", f = bob.address, t = reserve.address, amount = 900000, contractAddr = contractAddr, tokenId = 0).run(sender = firstSigner)
        scenario.h2("#a20 - secondSigner accepts proposal")
        scenario += c1.accept("at04").run(sender = secondSigner)
        scenario.h2("#a21 - firstSigner creates a removal of fees manager")
        scenario += c1.createRemoveOptionProposal(proposalId = "rm01", contractAddr = contractAddr, operation = "set_fees_manager").run(sender = firstSigner)
        scenario.h2("#a22 - secondSigner accepts proposal")
        scenario += c1.accept("rm01").run(sender = secondSigner)
        scenario.h2("#a21 - firstSigner creates a set of rights manager")
        scenario += c1.createSetOptionProposal(address = rightsAddr, proposalId = "sr01", contractAddr = contractAddr, operation = "set_rights_manager").run(sender = firstSigner)
        scenario.h2("#a22 - secondSigner accepts proposal")
        scenario += c1.accept("sr01").run(sender = secondSigner)