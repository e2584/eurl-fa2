import smartpy as sp

# A collection of error messages used in the contract.
class MasterMinter_Error:
    def make(s): return ("MASTERMINTER_" + s)

    unreferencedSigner              = make("UnreferencedSigner")
    alreadyReferenced               = make("ProposalIdAlreadyReferenced")
    unreferencedProposal            = make("ProposalIdUnreferenced")
    openLimit                       = make("OpenProposalsLimitReached")
    proposalClosed                  = make("ProposalAlreadyClosed")
    unauthorizedOp                  = make("OperationUnauthorized")
    proposalAccepted                = make("ProposalAlreadyAccepted")

class MasterMinter(sp.Contract):
    def __init__(self, controllers, operators):
        self.init_type(t = sp.TRecord(controllers = sp.TSet(sp.TAddress), operators = sp.TSet(sp.TAddress), 
                       proposals = sp.TBigMap(sp.TString, sp.TRecord(address = sp.TAddress, amount = sp.TNat, 
                       operation = sp.TString, contractAddr = sp.TAddress, opApprovals = sp.TSet(sp.TAddress), 
                       opRejects = sp.TSet(sp.TAddress), ctrlApprovals = sp.TSet(sp.TAddress), ctrlRejects = sp.TSet(sp.TAddress), 
                       tokenId = sp.TNat, status = sp.TBool)), authOps = sp.TSet(sp.TString), limit =  sp.TNat, 
                       restriction =  sp.TInt, openProposals = sp.TInt))
        self.init(controllers = controllers, operators = operators, proposals = sp.big_map(), 
                  authOps = {"mint", "burn", "add_minter", "remove_minter", "update_allowance"}, 
                  limit = 1, restriction = 10, openProposals = 0)
        
    '''
    Verification utils
    '''
    
    def checkSigner(self):
        sp.verify(self.data.operators.contains(sp.sender) | self.data.controllers.contains(sp.sender), 
            message = MasterMinter_Error.unreferencedSigner)
    
    def checkNewProposal(self, proposalId):
        sp.verify(~self.data.proposals.contains(proposalId), MasterMinter_Error.alreadyReferenced)
    
    def checkProposal(self, proposalId):
        sp.verify(self.data.proposals.contains(proposalId), MasterMinter_Error.unreferencedProposal)
        
    def checkRestriction(self):
        sp.verify(self.data.restriction > self.data.openProposals, MasterMinter_Error.openLimit)
    
    def checkOpen(self, proposalId):
        sp.verify(~self.data.proposals[proposalId].status, MasterMinter_Error.proposalClosed)
    
    def checkOperation(self, operation):
        sp.verify(self.data.authOps.contains(operation), MasterMinter_Error.unauthorizedOp)
        
    '''
    Main entrypoints
    '''

    @sp.entry_point
    def createProposal(self, params):
        sp.set_type(params, sp.TRecord(address = sp.TAddress, amount = sp.TNat, contractAddr = sp.TAddress, 
                    operation = sp.TString, proposalId = sp.TString, tokenId = sp.TNat))
        self.checkSigner()
        self.checkOperation(params.operation)
        self.checkRestriction()
        self.checkNewProposal(params.proposalId)
        self.data.proposals[params.proposalId] = sp.record(address = params.address, 
            amount = params.amount, operation = params.operation, contractAddr = params.contractAddr, 
            opApprovals = sp.set(t = sp.TAddress), opRejects = sp.set(t = sp.TAddress), 
            ctrlApprovals = sp.set(t = sp.TAddress), ctrlRejects = sp.set(t = sp.TAddress), 
            tokenId = params.tokenId, status = False)
        sp.if self.data.operators.contains(sp.sender):
            self.data.proposals[params.proposalId].opApprovals.add(sp.sender)
        sp.else:
            self.data.proposals[params.proposalId].ctrlApprovals.add(sp.sender)
        self.data.openProposals += 1

    @sp.entry_point
    def accept(self, params):
        sp.set_type(params, sp.TString)
        self.checkSigner()
        self.checkProposal(params)
        self.checkOpen(params)
        sp.if self.data.operators.contains(sp.sender):
            sp.verify(~self.data.proposals[params].opApprovals.contains(sp.sender), MasterMinter_Error.proposalAccepted)
            self.data.proposals[params].opApprovals.add(sp.sender)
        sp.else:
            sp.verify(~self.data.proposals[params].ctrlApprovals.contains(sp.sender), MasterMinter_Error.proposalAccepted)
            self.data.proposals[params].ctrlApprovals.add(sp.sender)
        sp.if ((sp.len(self.data.proposals[params].opApprovals) >= self.data.limit) & (sp.len(self.data.proposals[params].ctrlApprovals) >= self.data.limit)):
            sp.if self.data.proposals[params].operation == "mint":
                self._processMintBurn(params, "mint")
            sp.else:
                sp.if self.data.proposals[params].operation == "burn":
                    self._processMintBurn(params, "burn")
                sp.else:
                    sp.if self.data.proposals[params].operation == "update_allowance":
                        self._processUpdateAllowance(params)
                    sp.else:
                        sp.if self.data.proposals[params].operation == "add_minter":
                            self._processAddMinter(params)
                        sp.else:
                            self._processRemoveMinter(params)
            self.data.proposals[params].status = True
            self.data.openProposals -= 1

    @sp.entry_point
    def reject(self, params):
        sp.set_type(params, sp.TString)
        self.checkSigner()
        self.checkProposal(params)
        self.checkOpen(params)
        sp.if self.data.operators.contains(sp.sender):
            self.data.proposals[params].opRejects.add(sp.sender)
            sp.if self.data.proposals[params].opApprovals.contains(sp.sender):
                self.data.proposals[params].opApprovals.remove(sp.sender)
        sp.else:
            self.data.proposals[params].ctrlRejects.add(sp.sender)
            sp.if self.data.proposals[params].ctrlApprovals.contains(sp.sender):
                self.data.proposals[params].ctrlApprovals.remove(sp.sender)
        self.data.proposals[params].status = True
        self.data.openProposals -= 1
        
    '''
    Lugh coin operations
    '''
    
    def _processMintBurn(self, proposalId, ePoint):
        transferParamsRecord = sp.record(address = self.data.proposals[proposalId].address, 
            amount = self.data.proposals[proposalId].amount, token_id = self.data.proposals[proposalId].tokenId)
        c = sp.contract(
                t = sp.TRecord(address = sp.TAddress, amount = sp.TNat, token_id = sp.TNat), 
                address = self.data.proposals[proposalId].contractAddr, 
                entry_point = ePoint
                ).open_some()
        sp.transfer(transferParamsRecord, sp.mutez(0), c)

    def _processAddMinter(self, proposalId):
        transferParamsRecord = sp.record(address = self.data.proposals[proposalId].address, 
            allowance = self.data.proposals[proposalId].amount, token_id = self.data.proposals[proposalId].tokenId)
        c = sp.contract(
                t = sp.TRecord(address = sp.TAddress, allowance = sp.TNat, token_id = sp.TNat), 
                address = self.data.proposals[proposalId].contractAddr, 
                entry_point = "add_minter"
                ).open_some()
        sp.transfer(transferParamsRecord, sp.mutez(0), c)

    def _processUpdateAllowance(self, proposalId):
        transferParamsRecord = sp.record(address = self.data.proposals[proposalId].address, 
            allowance = self.data.proposals[proposalId].amount)
        c = sp.contract(
                t = sp.TRecord(address = sp.TAddress, allowance = sp.TNat), 
                address = self.data.proposals[proposalId].contractAddr, 
                entry_point = "update_allowance"
                ).open_some()
        sp.transfer(transferParamsRecord, sp.mutez(0), c)

    def _processRemoveMinter(self, proposalId):
        transferParamsRecord = sp.record(address = self.data.proposals[proposalId].address)
        c = sp.contract(
                t = sp.TRecord(address = sp.TAddress), 
                address = self.data.proposals[proposalId].contractAddr, 
                entry_point = "remove_minter"
                ).open_some()
        sp.transfer(transferParamsRecord, sp.mutez(0), c)

if "templates" not in __name__:
    @sp.add_test(name = "MasterMinter")
    def test():

        scenario = sp.test_scenario()
        scenario.h1("MasterMinter Contract")

        # sp.test_account generates ED25519 key-pairs deterministically:
        opFirstSigner = sp.test_account("opFirstSigner")
        opSecondSigner = sp.test_account("opSecondSigner")
        ctrlFirstSigner = sp.test_account("ctrlFirstSigner")
        ctrlSecondSigner = sp.test_account("ctrlSecondSigner")
        reserve = sp.test_account("reserve")
        new_minter = sp.test_account("new_minter")
        contractAddr = sp.address("KT1-distantContractToCall-1234")

        # Let's display the accounts:
        scenario.h2("Accounts")
        scenario.show([opFirstSigner, opSecondSigner, ctrlFirstSigner, ctrlSecondSigner, reserve])

        c1 = MasterMinter(operators = sp.set([opFirstSigner.address, opSecondSigner.address]), 
            controllers = sp.set([ctrlFirstSigner.address, ctrlSecondSigner.address]))

        scenario += c1
        
        scenario.h2("#m01 - opFirstSigner tries to create a transfer proposal")
        scenario += c1.createProposal(proposalId = "mm01", address = reserve.address, amount = 1000000000, operation = "transfer", contractAddr = contractAddr, tokenId = 0).run(sender = opFirstSigner, valid = False)
        scenario.h2("#m02 - opFirstSigner creates a mint proposal")
        scenario += c1.createProposal(proposalId = "mm01", address = reserve.address, amount = 1000000000, operation = "mint", contractAddr = contractAddr, tokenId = 0).run(sender = opFirstSigner)
        scenario.h2("#m03 - opFirstSigner accepts proposal")
        scenario += c1.accept("mm01").run(sender = opFirstSigner, valid=False)
        scenario.h2("#m04 - ctrlSecondSigner accepts unreferenced proposalId")
        scenario += c1.accept("mm02").run(sender = ctrlSecondSigner, valid=False)
        scenario.h2("#m05 - opSecondSigner accepts proposal")
        scenario += c1.accept("mm01").run(sender = opSecondSigner)
        scenario.h2("#m06 - ctrlSecondSigner accepts proposal")
        scenario += c1.accept("mm01").run(sender = ctrlSecondSigner)
        scenario.h2("#m07 - ctrlFirstSigner accepts proposal")
        scenario += c1.accept("mm01").run(sender = ctrlFirstSigner, valid=False)
        
        scenario.h2("#m08 - opSecondSigner creates a burn proposal with referenced proposalId")
        scenario += c1.createProposal(proposalId = "mm01", address = reserve.address, amount = 200000000, operation = "burn", contractAddr = contractAddr, tokenId = 0).run(sender = opSecondSigner, valid=False)
        scenario.h2("#m09 - opSecondSigner creates a burn proposal")
        scenario += c1.createProposal(proposalId = "mb01", address = reserve.address, amount = 200000000, operation = "burn", contractAddr = contractAddr, tokenId = 0).run(sender = opSecondSigner)
        scenario.h2("#m10 - opSecondSigner rejects burn proposal")
        scenario += c1.reject("mb01").run(sender = opSecondSigner)
        scenario.h2("#m11 - opFirstSigner rejects proposal")
        scenario += c1.reject("mb01").run(sender = opFirstSigner, valid=False)
        scenario.h2("#m12 - ctrlFirstSigner rejects proposal")
        scenario += c1.reject("mb01").run(sender = ctrlFirstSigner, valid=False)
        scenario.h2("#m13 - ctrlSecondSigner rejects proposal")
        scenario += c1.reject("mb01").run(sender = ctrlSecondSigner, valid=False)
        scenario.h2("#m14 - opSecondSigner creates a burn proposal")
        scenario += c1.createProposal(proposalId = "mb02", address = reserve.address, amount = 1000000000, operation = "burn", contractAddr = contractAddr, tokenId = 0).run(sender = opSecondSigner)
        scenario.h2("#m15 - ctrlSecondSigner accepts proposal")
        scenario += c1.accept("mb02").run(sender = ctrlSecondSigner)
        scenario.h2("#m16 - opSecondSigner creates an over burn proposal")
        scenario += c1.createProposal(proposalId = "mb03", address = reserve.address, amount = 900000000000, operation = "burn", contractAddr = contractAddr, tokenId = 0).run(sender = opSecondSigner)
        scenario.h2("#m17 - ctrlSecondSigner accepts proposal")
        scenario += c1.accept("mb03").run(sender = ctrlSecondSigner)

        scenario.h2("#m18 - opSecondSigner creates an addMinter proposal with 10k EUR-L allowance")
        scenario += c1.createProposal(proposalId = "madd01", address = new_minter.address, amount = 10000000000, operation = "add_minter", contractAddr = contractAddr, tokenId = 0).run(sender = opSecondSigner)
        scenario.h2("#m19 - ctrlSecondSigner accepts proposal")
        scenario += c1.accept("madd01").run(sender = ctrlSecondSigner)

        scenario.h2("#m20 - opSecondSigner creates a removeMinter proposal")
        scenario += c1.createProposal(proposalId = "mrm01", address = new_minter.address, amount = 0, operation = "remove_minter", contractAddr = contractAddr, tokenId = 0).run(sender = opSecondSigner)
        scenario.h2("#m21 - ctrlSecondSigner accepts proposal")
        scenario += c1.accept("mrm01").run(sender = ctrlSecondSigner)
