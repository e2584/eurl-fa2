import smartpy as sp

'''
A multi fungible assets FA2 contract
Developped and introduced for Lugh EURL euro pegged coin
Support direct gasless transfer i.e. transfer_gasless
Support TZIP17 permits 
(see more at https://gitlab.com/tezos/tzip/-/blob/master/proposals/tzip-17/tzip-17.md)
'''

'''
Authored by Sceme.io
'''

class FA2_config:
    def __init__(self,
                 debug_mode                         = False,
                 single_asset                       = False,
                 non_fungible                       = False,
                 add_mutez_transfer                 = True,
                 readable                           = True,
                 force_layouts                      = True,
                 support_operator                   = True,
                 assume_consecutive_token_ids       = True,
                 store_total_supply                 = True,
                 lazy_entry_points                  = False,
                 allow_self_transfer                = False,
                 use_token_metadata_offchain_view   = False
                 ):

        if debug_mode:
            self.my_map = sp.map
        else:
            self.my_map = sp.big_map

        self.use_token_metadata_offchain_view = use_token_metadata_offchain_view
        self.single_asset = single_asset
        self.non_fungible = non_fungible
        self.readable = readable
        self.force_layouts = force_layouts
        self.support_operator = support_operator
        self.assume_consecutive_token_ids = assume_consecutive_token_ids
        self.store_total_supply = store_total_supply
        self.add_mutez_transfer = add_mutez_transfer
        self.lazy_entry_points = lazy_entry_points
        self.allow_self_transfer = allow_self_transfer

        name = "LUGH"
        self.name = name

token_id_type = sp.TNat

class Error_message:
    def __init__(self, config):
        self.config = config
        self.prefix = "LUGH_"
    def make(self, s): return (self.prefix + s)
    def token_undefined(self):       return self.make("TOKEN_UNDEFINED")
    def locked(self):                return self.make("ADDRESS_LOCKED")
    def insufficient_balance(self):  return self.make("INSUFFICIENT_BALANCE")
    def not_operator(self):          return self.make("NOT_OPERATOR")
    def operators_unsupported(self): return self.make("OPERATORS_UNSUPPORTED")
    def not_owner(self):             return self.make("NOT_OWNER")
    def not_admin(self):             return self.make("NOT_ADMIN")
    def not_admin_or_operator(self): return self.make("NOT_ADMIN_OR_OPERATOR")
    def not_fees_manager(self):      return self.make("NOT_FEES_MANAGER")
    def not_master_minter(self):     return self.make("NOT_MASTER_MINTER")
    def not_minter(self):            return self.make("NOT_MINTER")
    def allowance_limit(self):       return self.make("REACHED_ALLOWANCE_LIMIT")
    def burn_limit(self):            return self.make("BURN_TOO_HIGH")
    def unreferenced_minter(self):   return self.make("UNREFERENCED_MINTER")
    def already_minter(self):        return self.make("ALREADY_MINTER")
    def invalid_contract_id(self):   return self.make("INVALID_CONTRACT_ID")
    def invalid_counter(self):       return self.make("INVALID_COUNTER")
    def paused(self):                return self.make("PAUSED")
    def unauthorized_transfer(self): return self.make("UNAUTHORIZED_TRANSFER_PROCESSING")
    def dup_permit(self):            return self.make("DUP_PERMIT")
    def no_permit(self):             return self.make("NO_PERMIT")
    def not_expired(self):           return self.make("PERMIT_NOT_EXPIRED")
    def not_permitted(self):         return self.make("NOT_PERMITTED")
    def max_expiry(self):            return self.make("MAX_SECONDS_EXCEEDED")
    def not_sender(self):            return self.make("NOT_AUTHORIZED")
    def revoked(self):               return self.make("PERMIT_REVOKED")
    def expired(self):               return self.make("PERMIT_EXPIRED")
    def missigned(self):             return self.make("MISSIGNED")
    def no_holder(self):             return self.make("UNREFERENCED_PERMIT_HOLDER")

class Batch_transfer:
    def __init__(self, config):
        self.config = config
    def get_transfer_type(self):
        tx_type = sp.TRecord(to_ = sp.TAddress,
                             token_id = token_id_type,
                             amount = sp.TNat)
        if self.config.force_layouts:
            # It is recommended to force layout for a better integration in wallet
            tx_type = tx_type.layout(
                ("to_", ("token_id", "amount"))
            )
        transfer_type = sp.TRecord(from_ = sp.TAddress,
                                   txs = sp.TList(tx_type)).layout(
                                       ("from_", "txs"))
        return transfer_type
    def get_type(self):
        return sp.TList(self.get_transfer_type())
    def item(self, from_, txs):
        v = sp.record(from_ = from_, txs = txs)
        return sp.set_type_expr(v, self.get_transfer_type())

class Operator_param:
    def __init__(self, config):
        self.config = config
    def get_type(self):
        t = sp.TRecord(
            owner = sp.TAddress,
            operator = sp.TAddress,
            token_id = token_id_type)
        if self.config.force_layouts:
            t = t.layout(("owner", ("operator", "token_id")))
        return t
    def make(self, owner, operator, token_id):
        r = sp.record(owner = owner,
                      operator = operator,
                      token_id = token_id)
        return sp.set_type_expr(r, self.get_type())

class Ledger_key:
    def __init__(self, config):
        self.config = config
    def make(self, user, token):
        user = sp.set_type_expr(user, sp.TAddress)
        token = sp.set_type_expr(token, token_id_type)
        if self.config.single_asset:
            result = user
        else:
            result = sp.pair(user, token)
        if self.config.readable:
            return result
        else:
            return sp.pack(result)

class Ledger_value:
    # Lock can be updated by Admin to block an account sendings for a given token_id
    def get_type():
        return sp.TRecord(balance = sp.TNat, lock = sp.TBool)
    def make(balance, lock):
        return sp.record(balance = balance, lock = lock)

class Operator_set:
    def __init__(self, config):
        self.config = config
    def inner_type(self):
        return sp.TRecord(owner = sp.TAddress,
                          operator = sp.TAddress,
                          token_id = token_id_type
                          ).layout(("owner", ("operator", "token_id")))
    def key_type(self):
        if self.config.readable:
            return self.inner_type()
        else:
            return sp.TBytes
    def make(self):
        return self.config.my_map(tkey = self.key_type(), tvalue = sp.TUnit)
    def make_key(self, owner, operator, token_id):
        metakey = sp.record(owner = owner,
                            operator = operator,
                            token_id = token_id)
        metakey = sp.set_type_expr(metakey, self.inner_type())
        if self.config.readable:
            return metakey
        else:
            return sp.pack(metakey)
    def add(self, set, owner, operator, token_id):
        set[self.make_key(owner, operator, token_id)] = sp.unit
    def remove(self, set, owner, operator, token_id):
        del set[self.make_key(owner, operator, token_id)]
    def is_member(self, set, owner, operator, token_id):
        return set.contains(self.make_key(owner, operator, token_id))

class Balance_of:
    def request_type():
        return sp.TRecord(
            owner = sp.TAddress,
            token_id = token_id_type).layout(("owner", "token_id"))
    def response_type():
        return sp.TList(
            sp.TRecord(
                request = Balance_of.request_type(),
                balance = sp.TNat).layout(("request", "balance")))
    def entry_point_type():
        return sp.TRecord(
            callback = sp.TContract(Balance_of.response_type()),
            requests = sp.TList(Balance_of.request_type())
        ).layout(("requests", "callback"))

class Token_meta_data:
    def __init__(self, config):
        self.config = config

    def get_type(self):
        return sp.TRecord(token_id = sp.TNat, token_info = sp.TMap(sp.TString, sp.TBytes))

    def set_type_and_layout(self, expr):
        sp.set_type(expr, self.get_type())

class Token_id_set:
    def __init__(self, config):
        self.config = config
    def empty(self):
        if self.config.assume_consecutive_token_ids:
            return sp.nat(0)
        else:
            return sp.set(t = token_id_type)
    def add(self, metaset, v):
        if self.config.assume_consecutive_token_ids:
            sp.verify(metaset == v, message = "Token-IDs should be consecutive")
            metaset.set(sp.max(metaset, v + 1))
        else:
            metaset.add(v)
    def contains(self, metaset, v):
        if self.config.assume_consecutive_token_ids:
            return (v < metaset)
        else:
            return metaset.contains(v)
    def cardinal(self, metaset):
        if self.config.assume_consecutive_token_ids:
            return metaset
        else:
            return sp.len(metaset)

def mutez_transfer(contract, params):
    sp.verify(sp.sender == contract.data.administrator)
    sp.set_type(params.destination, sp.TAddress)
    sp.set_type(params.amount, sp.TMutez)
    sp.send(params.destination, params.amount)

class FA2_core(sp.Contract):
    def __init__(self, config, metadata, **extra_storage):
        self.config = config
        self.error_message = Error_message(self.config)
        self.operator_set = Operator_set(self.config)
        self.operator_param = Operator_param(self.config)
        self.token_id_set = Token_id_set(self.config)
        self.ledger_key = Ledger_key(self.config)
        self.token_meta_data = Token_meta_data(self.config)
        self.batch_transfer    = Batch_transfer(self.config)
        if  self.config.add_mutez_transfer:
            self.transfer_mutez = sp.entry_point(mutez_transfer)
        if config.lazy_entry_points:
            self.add_flag("lazy-entry-points")
        self.add_flag("initial-cast")
        self.exception_optimization_level = "default-line"
        # Init with token_id i.e. EURL
        eurl_md = self.make_metadata(
            name = "Lugh Euro pegged stablecoin",
            decimals = 6,
            symbol= "EURL",
            thumbnailUri = "ipfs://QmcqsYQn8pTxQr3P1dYpgYxQa6GQPmoBTSWQ8bpuFEuaqe")
        self.init(
            ledger = self.config.my_map(tvalue = Ledger_value.get_type()),
            token_metadata = self.config.my_map(l = {0 : sp.record(token_id = 0,
                            token_info = eurl_md) }, tkey = sp.TNat, tvalue = self.token_meta_data.get_type()),
            operators = self.operator_set.make(),
            all_tokens = 1,
            master_minted = sp.map(l = {0: 0}, tkey = sp.TNat, tvalue = sp.TNat),
            metadata = metadata,
            minters = sp.map(tvalue = sp.TRecord(allowance = sp.TNat, minted = sp.TNat, token_id = sp.TNat)),
            total_supply = self.config.my_map(l = {0 : 0}, tkey = sp.TNat, tvalue = sp.TNat),
            permits = sp.big_map(tkey = sp.TAddress, 
                                tvalue = sp.TRecord(counter = sp.TNat,
                                                 user_expiry = sp.TOption(sp.TNat), 
                                                 user_permits = sp.TMap(k = sp.TBytes, 
                                                 v = sp.TRecord(expiry = sp.TOption(sp.TNat), created_at = sp.TTimestamp)))),
            default_expiry = 345600, #4 days
            max_expiry = 2678400, #31 days
            **extra_storage
        )

    # If rights manager contract, thus call to validate identities i.e. sender & receiver, minter...
    def check_rights(self, params):
        sp.if self.data.rights_manager.is_some():
            c = sp.contract(
                    t = sp.TRecord(amount = sp.TNat, balance = sp.TNat, f = sp.TAddress, 
                                   t = sp.TAddress, operation = sp.TString, token_id = sp.TNat), 
                    address = self.data.rights_manager.open_some(),
                    entry_point = "checkRights"
                    ).open_some()
            sp.transfer(params, sp.mutez(0), c)

    # Calls fees manager to build fees and callback payload
    def process_fees(self, params):
        self.data.transfer_operation += 1
        c = sp.contract(
                t = sp.TRecord(amount = sp.TNat, f = sp.TAddress, flat =sp.TBool, storage = sp.TBool, 
                                self_support = sp.TBool, t = sp.TAddress, relay = sp.TAddress, 
                                token_id = sp.TNat, xtz = sp.TBool), 
                address = self.data.fees_manager, 
                entry_point = "calculateFees"
                ).open_some()
        sp.transfer(params, sp.mutez(0), c)
    
    def process_sig(self, params):
        sp.verify(sp.check_signature(params.pub_key, params.sig, params.b), 
                  message = sp.pair(self.error_message.missigned(), params.b))

    # Processing funds mouvements & balance updates for a given transfer
    def _transfer(self, params):
        sp.set_type(params, sp.TRecord(amount = sp.TNat, f = sp.TAddress, fees = sp.TNat, relay = sp.TAddress, 
                                       self_support = sp.TBool, t = sp.TAddress, token_id = sp.TNat))
        # Verifiy that fees manager is not corrupted and process unexpected transfer payload
        sp.verify(self.data.transfer_operation > 0, message = self.error_message.unauthorized_transfer())
        from_user = self.ledger_key.make(params.f, params.token_id)
        to_user = self.ledger_key.make(params.t, params.token_id)
        fees_faucet = self.ledger_key.make(self.data.fees_faucet, params.token_id)
        sp.if params.self_support:
            # sp.sender (relay) pays fees
            sp.verify(
                (self.data.ledger[from_user].balance >= params.amount),
                message = self.error_message.insufficient_balance())
            self.data.ledger[from_user].balance = sp.as_nat(
                self.data.ledger[from_user].balance - params.amount)
            relay = self.ledger_key.make(params.relay, params.token_id)
            sp.verify(
                self.data.ledger.contains(relay) &
                (self.data.ledger[relay].balance >= params.fees),
                message = self.error_message.insufficient_balance())
            self.data.ledger[relay].balance = sp.as_nat(
                self.data.ledger[relay].balance - params.fees)
        sp.else:
            # from_ (signer) pays fees
            sp.verify(
                (self.data.ledger[from_user].balance >= (params.amount + params.fees)),
                message = self.error_message.insufficient_balance())
            self.data.ledger[from_user].balance = sp.as_nat(
                self.data.ledger[from_user].balance - (params.amount + params.fees))
        sp.if self.data.ledger.contains(to_user):
            self.data.ledger[to_user].balance += params.amount
        sp.else:
            self.data.ledger[to_user] = Ledger_value.make(params.amount, False)
        sp.if self.data.ledger.contains(fees_faucet):
            self.data.ledger[fees_faucet].balance += params.fees
        sp.else:
            self.data.ledger[fees_faucet] = Ledger_value.make(params.fees, False)
        # Decrement list of transfers to be processed
        self.data.transfer_operation = sp.as_nat(self.data.transfer_operation - 1)

    # List of transfers. Allows operators & permits.
    @sp.entry_point
    def transfer(self, params):
        sp.verify( ~self.is_paused() | self.is_administrator(sp.sender), message = self.error_message.paused() )
        sp.set_type(params, self.batch_transfer.get_type())
        sp.for transfer in params:
           current_from = transfer.from_
           sp.for tx in transfer.txs:
                sender_verify = ((self.is_administrator(sp.sender)) |
                                (current_from == sp.sender))
                message = self.error_message.not_owner()
                if self.config.support_operator:
                    message = self.error_message.not_operator()
                    sender_verify |= (self.operator_set.is_member(self.data.operators,
                                                                  current_from,
                                                                  sp.sender,
                                                                  tx.token_id))
                if self.config.allow_self_transfer:
                    sender_verify |= (sp.sender == sp.self_address)
                # Build permit key
                permit_key = sp.blake2b(sp.pack(sp.pair(tx.amount, sp.pair(current_from, sp.pair(tx.to_, tx.token_id)))))
                #Handle permits
                sp.if self._is_permit(current_from, permit_key):
                    effective_expiry = self.get_expiry(current_from, permit_key)
                    sp.verify(sp.as_nat(sp.now - self.data.permits[current_from].user_permits[permit_key].created_at) < effective_expiry, message = self.error_message.expired())
                    self._delete_permit(current_from, permit_key)
                sp.else:
                    sp.verify(sender_verify, message = message)
                sp.verify(
                    self.data.token_metadata.contains(tx.token_id),
                    message = self.error_message.token_undefined()
                )
                sp.if (tx.amount > 0):
                    sp.if self.is_administrator(sp.sender):
                        params_transfer = sp.record(amount = tx.amount, f = current_from, 
                                                    fees = 0, relay = sp.sender,
                                                    self_support = False, t = tx.to_, token_id = tx.token_id)
                        # Increment nb tx to be processed
                        self.data.transfer_operation += 1
                        self._transfer(params_transfer)
                    sp.else: 
                        from_user = self.ledger_key.make(current_from, tx.token_id)
                        sp.verify(~self.data.ledger[from_user].lock , message = self.error_message.locked())
                        self.check_rights(sp.record(amount = tx.amount, balance = self.data.ledger[from_user].balance, 
                            f = current_from, t = tx.to_, operation = "transfer", token_id = tx.token_id))
                        to_user = self.ledger_key.make(tx.to_, tx.token_id)
                        # Process fees without applying any flat fees and leaving from_ to pay fees (not sp.sender)
                        self.process_fees(sp.record(amount = tx.amount, f = current_from, 
                                                    flat = False, relay = sp.sender,
                                                    self_support = False,
                                                    storage = self.data.ledger.contains(to_user), 
                                                    t = tx.to_, token_id = tx.token_id, xtz = True))
                sp.else:
                    pass

    # Enable relay account to push tx for another account i.e. f
    @sp.entry_point
    def transfer_gasless(self, params):
        tx_type = sp.TRecord(to_ = sp.TAddress,
                             token_id = token_id_type,
                             amount = sp.TNat).layout(
                ("to_", ("token_id", "amount")))
        payload_type = sp.TRecord(from_ = sp.TKey,
                                  txs = sp.TList(tx_type), sig = sp.TSignature).layout(
                                  ("from_", ("txs", "sig")))
        sp.set_type(params, sp.TRecord(flat_fees = sp.TBool, support_fees = sp.TBool, transfers = sp.TList(payload_type)))
        sp.verify(~self.is_paused() , message = self.error_message.paused())
        sp.for transfer in params.transfers:
            pk_address = sp.to_address(sp.implicit_account(sp.hash_key(transfer.from_)))
            sp.if self.data.permits.contains(pk_address):
                b = sp.pack(sp.pair(sp.self_address, sp.pair(self.data.permits[pk_address].counter, sp.pair(pk_address, transfer.txs))))
                self.process_sig(sp.record(pub_key = transfer.from_, sig = transfer.sig, b = b))
                self.data.permits[pk_address].counter += 1 
            sp.else: 
                b = sp.pack(sp.pair(sp.self_address, sp.pair(0, sp.pair(pk_address, transfer.txs))))
                self.process_sig(sp.record(pub_key = transfer.from_, sig = transfer.sig, b = b))
                self.data.permits[pk_address] = sp.record(user_expiry = sp.none, user_permits = {}, counter = 1)
            sp.for tx in transfer.txs:
                from_user = self.ledger_key.make(pk_address, tx.token_id)
                sp.verify(~self.data.ledger[from_user].lock , message = self.error_message.locked())
                self.check_rights(sp.record(amount = tx.amount, balance = self.data.ledger[from_user].balance, 
                                            f = pk_address, t = tx.to_, operation = "transfer", token_id = tx.token_id))
                sp.if (tx.amount > 0):
                    to_user = self.ledger_key.make(tx.to_, tx.token_id)
                    self.process_fees(sp.record(amount = tx.amount, f = pk_address, 
                                                flat = params.flat_fees, relay = sp.sender,
                                                self_support = params.support_fees,
                                                storage = self.data.ledger.contains(to_user), t = tx.to_, 
                                                token_id = tx.token_id, xtz = False))
                sp.else:
                    pass

    # Deposit a permit i.e. a list of payload blake2b hashes
    @sp.entry_point
    def permit(self, params):
        sp.set_type(params, sp.TPair(sp.TKey, sp.TPair(sp.TSignature, sp.TBytes)))
        sp.verify( ~self.is_paused(), message = self.error_message.paused())
        pk_address = sp.to_address(sp.implicit_account(sp.hash_key(sp.fst(params)))) 
        permit_key = sp.snd(sp.snd(params))
        permit_exists = self.data.permits.contains(pk_address) & self.data.permits[pk_address].user_permits.contains(permit_key)
        effective_expiry = self.get_expiry(pk_address, permit_key)
        sp.verify(~(permit_exists & (sp.as_nat(sp.now - self.data.permits[pk_address].user_permits[permit_key].created_at) < effective_expiry)), message = self.error_message.expired())
        sp.verify(~permit_exists, message = self.error_message.dup_permit())
        sp.if self.data.permits.contains(pk_address):
            b = sp.pack(sp.pair(sp.pair(sp.chain_id, sp.self_address), sp.pair(self.data.permits[pk_address].counter, permit_key)))
            self.process_sig(sp.record(pub_key = sp.fst(params), sig = sp.fst(sp.snd(params)), b = b))
            self.data.permits[pk_address].user_permits[permit_key] = sp.record(created_at = sp.now, expiry = sp.none)
            self.data.permits[pk_address].counter += 1
            # Delete all user permits older than 31 days
            keys = self.data.permits[pk_address].user_permits.keys()
            sp.for key in keys:
                sp.if sp.as_nat(sp.now - self.data.permits[pk_address].user_permits[key].created_at) > self.data.max_expiry:
                    del self.data.permits[pk_address].user_permits[key]
        sp.else:
            b = sp.pack(sp.pair(sp.pair(sp.chain_id, sp.self_address), sp.pair(sp.as_nat(0), permit_key)))
            self.process_sig(sp.record(pub_key = sp.fst(params), sig = sp.fst(sp.snd(params)), b = b))
            user_permits = {}
            user_permits[permit_key] = sp.record(created_at = sp.now, expiry = sp.none)
            self.data.permits[pk_address] = sp.record(user_expiry = sp.none, user_permits = user_permits, counter = 1)

    # Set a duration before expiry for a given permit
    @sp.entry_point
    def set_expiry(self, params):
        sp.set_type(params, sp.TRecord(seconds=sp.TNat, permit=sp.TOption(sp.TBytes))).layout(("seconds", "permit"))
        sp.verify( ~self.is_paused(), message = self.error_message.paused())
        sp.verify(params.seconds <= self.data.max_expiry, message = self.error_message.max_expiry())
        sp.if params.permit.is_some():
            permit_key = params.permit.open_some()
            sp.verify(self.data.permits.contains(sp.sender) & self.data.permits[sp.sender].user_permits.contains(permit_key), message = self.error_message.no_permit())
            permit_submission_timestamp = self.data.permits[sp.sender].user_permits[permit_key].created_at
            sp.if self.data.permits[sp.sender].user_permits[permit_key].expiry.is_some():
                permit_expiry = self.data.permits[sp.sender].user_permits[permit_key].expiry.open_some()
                sp.verify(sp.as_nat(sp.now - permit_submission_timestamp) < permit_expiry, message = self.error_message.revoked())
            sp.else:
                sp.if self.data.permits[sp.sender].user_expiry.is_some():
                    user_expiry = self.data.permits[sp.sender].user_expiry.open_some()
                    sp.verify(sp.as_nat(sp.now - permit_submission_timestamp) < user_expiry, message = self.error_message.revoked())
                sp.else:
                    sp.verify(sp.as_nat(sp.now - permit_submission_timestamp) < self.data.default_expiry, message = self.error_message.revoked())
            sp.if params.seconds > 0:
                self.data.permits[sp.sender].user_permits[permit_key].expiry = sp.some(params.seconds)
            sp.else:
                self._delete_permit(sp.sender, permit_key)
        sp.else:
            sp.verify(self.data.permits.contains(sp.sender), message = self.error_message.no_holder())
            self.data.permits[sp.sender].user_expiry = sp.some(params.seconds)

    # Retrieve expiry duration for a given permit key
    def get_expiry(self, address, permit_key):
        effective_expiry = sp.local("effective_expiry", 0)
        sp.if self.data.permits.contains(address) & self.data.permits[address].user_permits.contains(permit_key) & self.data.permits[address].user_permits[permit_key].expiry.is_some():
            effective_expiry.value = self.data.permits[address].user_permits[permit_key].expiry.open_some()
        sp.else:
            sp.if self.data.permits.contains(address) & self.data.permits[address].user_expiry.is_some():
                effective_expiry.value = self.data.permits[address].user_expiry.open_some()
            sp.else:
                effective_expiry.value = self.data.default_expiry
        return effective_expiry.value

    # Process permit deletion
    def _delete_permit(self, address, permit_key):
        sp.if self.data.permits.contains(address) & self.data.permits[address].user_permits.contains(permit_key):
            del self.data.permits[address].user_permits[permit_key]


    # Check if permit exists
    def _is_permit(self, address, permit_key):
        return self.data.permits.contains(address) & self.data.permits[address].user_permits.contains(permit_key)

    # Fees manager callback to complete transfer + fees
    @sp.entry_point
    def process_transfer(self, params):
        sp.verify(sp.sender == self.data.fees_manager, message = self.error_message.not_fees_manager())
        self._transfer(params)

    @sp.entry_point
    def balance_of(self, params):
        sp.verify( ~self.is_paused(), message = self.error_message.paused())
        sp.set_type(params, Balance_of.entry_point_type())
        def f_process_request(req):
            user = self.ledger_key.make(req.owner, req.token_id)
            sp.verify(self.data.token_metadata.contains(req.token_id), 
                      message = self.error_message.token_undefined())
            sp.if self.data.ledger.contains(user):
                balance = self.data.ledger[user].balance
                sp.result(
                    sp.record(
                        request = sp.record(
                            owner = sp.set_type_expr(req.owner, sp.TAddress),
                            token_id = sp.set_type_expr(req.token_id, sp.TNat)),
                            balance = balance))
            sp.else:
                sp.result(
                    sp.record(
                        request = sp.record(
                            owner = sp.set_type_expr(req.owner, sp.TAddress),
                            token_id = sp.set_type_expr(req.token_id, sp.TNat)),
                            balance = 0))
        res = sp.local("responses", params.requests.map(f_process_request))
        destination = sp.set_type_expr(params.callback, sp.TContract(Balance_of.response_type()))
        sp.transfer(res.value, sp.mutez(0), destination)

    @sp.offchain_view(pure = True)
    def get_balance(self, req):
        """This is the `get_balance` view defined in TZIP-12."""
        sp.set_type(
            req, sp.TRecord(
                owner = sp.TAddress,
                token_id = sp.TNat
            ).layout(("owner", "token_id")))
        user = self.ledger_key.make(req.owner, req.token_id)
        sp.verify(self.data.token_metadata.contains(req.token_id), message = self.error_message.token_undefined())
        sp.result(self.data.ledger[user].balance)

    @sp.entry_point
    def update_operators(self, params):
        sp.set_type(params, sp.TList(
            sp.TVariant(
                add_operator = self.operator_param.get_type(),
                remove_operator = self.operator_param.get_type()
            )
        ))
        if self.config.support_operator:
            sp.for update in params:
                with update.match_cases() as arg:
                    with arg.match("add_operator") as upd:
                        sp.verify(
                            (upd.owner == sp.sender) | self.is_administrator(sp.sender),
                            message = self.error_message.not_admin_or_operator()
                        )
                        self.operator_set.add(self.data.operators,
                                              upd.owner,
                                              upd.operator,
                                              upd.token_id)
                    with arg.match("remove_operator") as upd:
                        sp.verify(
                            (upd.owner == sp.sender) | self.is_administrator(sp.sender),
                            message = self.error_message.not_admin_or_operator()
                        )
                        self.operator_set.remove(self.data.operators,
                                                 upd.owner,
                                                 upd.operator,
                                                 upd.token_id)
        else:
            sp.failwith(self.error_message.operators_unsupported())

    def is_paused(self):
        return sp.bool(False)

    def is_administrator(self, sender):
        return sp.bool(False)

class FA2_administrator(FA2_core):
    def is_administrator(self, sender):
        return sender == self.data.administrator

    @sp.entry_point
    def set_rights_manager(self, params):
        sp.verify(self.is_administrator(sp.sender), message = self.error_message.not_admin())
        self.data.rights_manager = params

    @sp.entry_point
    def set_fees_manager(self, params):
        sp.verify(self.is_administrator(sp.sender), message = self.error_message.not_admin())
        self.data.fees_manager = params

    @sp.entry_point
    def set_fees_faucet(self, params):
        sp.verify(self.is_administrator(sp.sender), message = self.error_message.not_admin())
        self.data.fees_faucet = params

    @sp.entry_point
    def set_lock(self, params):
        sp.set_type(params, sp.TRecord(address = sp.TAddress, lock = sp.TBool, token_id = sp.TNat))
        sp.verify(self.is_administrator(sp.sender), message = self.error_message.not_admin())
        user = self.ledger_key.make(params.address, params.token_id)
        sp.if self.data.ledger.contains(user):
            self.data.ledger[user].lock = params.lock
        sp.else:
            self.data.ledger[user] = Ledger_value.make(0, params.lock)

class FA2_pause(FA2_core):
    def is_paused(self):
        return self.data.paused

    @sp.entry_point
    def set_pause(self, params):
        sp.verify(self.is_administrator(sp.sender), message = self.error_message.not_admin())
        self.data.paused = params

class FA2_change_metadata(FA2_core):
    @sp.entry_point
    def set_metadata(self, k, v):
        sp.verify(self.is_administrator(sp.sender), message = self.error_message.not_admin())
        self.data.metadata[k] = v

class FA2_owner(FA2_core):
    def is_owner(self, sender):
        return sender == self.data.owner

    @sp.entry_point
    def set_administrator(self, params):
        sp.verify(self.is_owner(sp.sender), message = self.error_message.not_owner())
        self.data.administrator = params

    @sp.entry_point
    def set_owner(self, params):
        sp.verify(self.is_owner(sp.sender), message = self.error_message.not_owner())
        self.data.owner = params

    @sp.entry_point
    def set_master_minter(self, params):
        sp.verify(self.is_owner(sp.sender), message = self.error_message.not_owner())
        self.data.master_minter = params
    
    # Reference a new token_id & metadata
    @sp.entry_point
    def create(self, params):
        sp.verify(self.is_owner(sp.sender), message = self.error_message.not_owner())
        self.token_id_set.add(self.data.all_tokens, params.token_id)
        self.data.token_metadata[params.token_id] = sp.record(
            token_id    = params.token_id,
            token_info  = params.meta_data
        )
        self.data.total_supply[params.token_id] = 0
        self.data.master_minted[params.token_id] = 0
    
    @sp.entry_point
    def update_metadata(self, params):
        sp.verify(self.is_owner(sp.sender), message = self.error_message.not_owner())
        sp.verify(self.data.token_metadata.contains(params.token_id), message = self.error_message.token_undefined())
        self.data.token_metadata[params.token_id] = sp.record(
            token_id    = params.token_id,
            token_info  = params.meta_data
        )

class FA2_mint(FA2_core):
    def is_master_minter(self, sender):
        return sender == self.data.master_minter

    # Add a delegatted minter
    @sp.entry_point
    def add_minter(self, params):
        sp.set_type(params, sp.TRecord(address = sp.TAddress, allowance = sp.TNat, token_id = sp.TNat))
        sp.verify(self.is_master_minter(sp.sender), message = self.error_message.not_master_minter())
        sp.verify(~self.data.minters.contains(params.address), self.error_message.already_minter()) 
        self.data.minters[params.address] = sp.record(allowance = params.allowance, 
                                                      minted = 0, token_id = params.token_id)
    # Remove a delegatted minter
    @sp.entry_point
    def remove_minter(self, params):
        sp.set_type(params, sp.TAddress)
        sp.verify(self.is_master_minter(sp.sender), message = self.error_message.not_master_minter())
        sp.verify(self.data.minters.contains(params), self.error_message.unreferenced_minter()) 
        del self.data.minters[params]
    
    # Update delegatted minter allowance
    @sp.entry_point
    def update_allowance(self, params):
        sp.set_type(params, sp.TRecord(address = sp.TAddress, allowance = sp.TNat))
        sp.verify(self.is_master_minter(sp.sender), message = self.error_message.not_master_minter())
        sp.verify(self.data.minters.contains(params.address), self.error_message.unreferenced_minter()) 
        self.data.minters[params.address].allowance = params.allowance
    
    @sp.entry_point
    def mint(self, params):
        sp.set_type(params, sp.TRecord(address = sp.TAddress, amount = sp.TNat, token_id = sp.TNat))
        sp.verify(self.data.token_metadata.contains(params.token_id), message = self.error_message.token_undefined())
        sp.verify((self.is_master_minter(sp.sender) | (self.data.minters.contains(sp.sender) 
                  & (self.data.minters[sp.sender].token_id == params.token_id))), message = self.error_message.not_minter())
        sp.verify(self.is_master_minter(sp.sender) | (self.data.minters[sp.sender].allowance >= params.amount), 
                  message = self.error_message.allowance_limit())
        sp.if self.data.minters.contains(sp.sender):
            self.check_rights(sp.record(amount = params.amount, balance = self.data.minters[sp.sender].minted, 
                            f = sp.sender, t = params.address, operation = "mint", token_id = params.token_id))
            self.data.minters[sp.sender].allowance = sp.as_nat(self.data.minters[sp.sender].allowance - params.amount)
            self.data.minters[sp.sender].minted += params.amount
        sp.else:
            self.check_rights(sp.record(amount = params.amount, balance = self.data.master_minted[params.token_id], 
                            f = sp.sender, t = params.address, operation = "mint", token_id = params.token_id))
            self.data.master_minted[params.token_id] += params.amount
        user = self.ledger_key.make(params.address, params.token_id)
        sp.if self.data.ledger.contains(user):
            self.data.ledger[user].balance += params.amount
        sp.else:
            self.data.ledger[user] = Ledger_value.make(params.amount, False)
        self.data.total_supply[params.token_id] += params.amount
    
    @sp.entry_point
    def burn(self, params):
        sp.set_type(params, sp.TRecord(address = sp.TAddress, amount = sp.TNat, token_id = sp.TNat))
        sp.verify(self.data.token_metadata.contains(params.token_id), message = self.error_message.token_undefined())
        sp.verify((self.is_master_minter(sp.sender) | (self.data.minters.contains(sp.sender) 
                  & (self.data.minters[sp.sender].token_id == params.token_id))), message = self.error_message.not_minter())
        sp.verify(self.is_master_minter(sp.sender) | (self.data.minters[sp.sender].minted >= params.amount), 
                  message = self.error_message.burn_limit())
        user = self.ledger_key.make(params.address, params.token_id)
        sp.verify((self.data.ledger[user].balance >= params.amount),
                        message = self.error_message.insufficient_balance())
        sp.if self.data.minters.contains(sp.sender):
            self.check_rights(sp.record(amount = params.amount, balance = self.data.minters[sp.sender].minted, 
                            f = sp.sender, t = params.address, operation = "burn", token_id = params.token_id))
            self.data.minters[sp.sender].minted = sp.as_nat(self.data.minters[sp.sender].minted - params.amount)
        sp.else:
            self.check_rights(sp.record(amount = params.amount, balance = self.data.master_minted[params.token_id], 
                            f = sp.sender, t = params.address, operation = "burn", token_id = params.token_id))
            sp.if self.data.master_minted[params.token_id] > params.amount:
                self.data.master_minted[params.token_id] = sp.as_nat(self.data.master_minted[params.token_id] - params.amount)
            sp.else:
                self.data.master_minted[params.token_id] = 0
        self.data.ledger[user].balance = sp.as_nat(self.data.ledger[user].balance - params.amount)
        self.data.total_supply[params.token_id] = sp.as_nat(self.data.total_supply[params.token_id] - params.amount)

class FA2_token_metadata(FA2_core):
    def set_token_metadata_view(self):
        def token_metadata(self, tok):
            """
            Return the token-metadata URI for the given token.

            For a reference implementation, dynamic-views seem to be the
            most flexible choice.
            """
            sp.set_type(tok, sp.TNat)
            sp.result(self.data.token_metadata[tok])

        self.token_metadata = sp.offchain_view(pure = True, doc = "Get Token Metadata")(token_metadata)

    def make_metadata(self, symbol, name, decimals, thumbnailUri):
        "Helper function to build metadata JSON bytes values."
        return (sp.map(l = {
            # Remember that michelson wants map already in ordered
            "decimals" : sp.utils.bytes_of_string("%d" % decimals),
            "is_boolean_amount" : sp.utils.bytes_of_string("%r" % False),
            "is_transferable" : sp.utils.bytes_of_string("%r" % True),
            "name" : sp.utils.bytes_of_string(name),
            "should_prefer_symbol": sp.utils.bytes_of_string("%r" % True),
            "symbol" : sp.utils.bytes_of_string(symbol),
            "thumbnailUri" : sp.utils.bytes_of_string(thumbnailUri)
        }))


class FA2(FA2_change_metadata, FA2_token_metadata, FA2_owner, FA2_mint, 
          FA2_administrator, FA2_pause, FA2_core):

    @sp.offchain_view(pure = True)
    def count_tokens(self):
        """Get how many tokens are in this FA2 contract.
        """
        sp.result(self.token_id_set.cardinal(self.data.all_tokens))

    @sp.offchain_view(pure = True)
    def does_token_exist(self, tok):
        "Ask whether a token ID is exists."
        sp.set_type(tok, sp.TNat)
        sp.result(self.data.token_metadata.contains(tok))

    @sp.offchain_view(pure = True)
    def all_tokens(self):
        if self.config.assume_consecutive_token_ids:
            sp.result(sp.range(0, self.data.all_tokens))
        else:
            sp.result(self.data.all_tokens.elements())

    @sp.offchain_view(pure = True)
    def total_supply(self, tok):
        sp.result(self.data.total_supply[tok])

    @sp.offchain_view(pure = True)
    def is_operator(self, query):
        sp.set_type(query,
                    sp.TRecord(token_id = sp.TNat,
                               owner = sp.TAddress,
                               operator = sp.TAddress).layout(
                                   ("owner", ("operator", "token_id"))))
        sp.result(
            self.operator_set.is_member(self.data.operators,
                                        query.owner,
                                        query.operator,
                                        query.token_id)
        )

    def __init__(self, config, metadata, admin, master_minter, fees_faucet, fees_manager, owner):
        # Let's show off some meta-programming:
        if config.assume_consecutive_token_ids:
            self.all_tokens.doc = """
            This view is specified (but optional) in the standard.

            This contract is built with assume_consecutive_token_ids =
            True, so we return a list constructed from the number of tokens.
            """
        else:
            self.all_tokens.doc = """
            This view is specified (but optional) in the standard.

            This contract is built with assume_consecutive_token_ids =
            False, so we convert the set of tokens from the storage to a list
            to fit the expected type of TZIP-16.
            """
        list_of_views = [
            self.get_balance
            , self.does_token_exist
            , self.count_tokens
            , self.all_tokens
            , self.is_operator
        ]

        if config.store_total_supply:
            list_of_views = list_of_views + [self.total_supply]
        if config.use_token_metadata_offchain_view:
            self.set_token_metadata_view()
            list_of_views = list_of_views + [self.token_metadata]

        metadata_base = {
            "version": config.name
            , "description" : (
                "Euro-pegged digital asset"
            )
            , "interfaces": ["TZIP-012", "TZIP-016"]
            , "authors": [
                "Sceme.io"
            ]
            , "homepage": "https://lugh.io"
            , "views": list_of_views
            , "source": {
                "tools": ["SmartPy"]
                , "location": "https://gitlab.com/sceme/EURLv2"
            }
            , "permissions": {
                "operator":
                "owner-or-operator-transfer" if config.support_operator else "owner-transfer"
                , "receiver": "owner-no-hook"
                , "sender": "owner-no-hook"
            }
            , "fa2-smartpy": {
                "configuration" :
                dict([(k, getattr(config, k)) for k in dir(config) if "__" not in k and k != 'my_map'])
            }
        }
        self.init_metadata("metadata_base", metadata_base)
        FA2_core.__init__(self, config, metadata, paused = False, administrator = admin, 
                          fees_faucet = fees_faucet, fees_manager = fees_manager, 
                          master_minter = master_minter, owner = owner, rights_manager = sp.none, 
                          transfer_operation = sp.as_nat(0))

## ## Tests
##
## ### Auxiliary Consumer Contract
##
## This contract is used by the tests to be on the receiver side of
## callback-based entry-points.
## It stores facts about the results in order to use `scenario.verify(...)`
## (cf.
##  [documentation](https://smartpy.io/docs/scenarios/testing)).
class View_consumer(sp.Contract):
    def __init__(self, contract):
        self.contract = contract
        self.init(last_sum = 0,
                  operator_support =  not contract.config.support_operator)

    @sp.entry_point
    def reinit(self):
        self.data.last_sum = 0
        # It's also nice to make this contract have more than one entry point.

    @sp.entry_point
    def receive_balances(self, params):
        sp.set_type(params, Balance_of.response_type())
        self.data.last_sum = 0
        sp.for resp in params:
            self.data.last_sum += resp.balance

## ### Generation of Test Scenarios
##
## Tests are also parametrized by the `FA2_config` object.
## The best way to visualize them is to use the online IDE
## (<https://www.smartpy.io/ide/>).
def add_test(config, is_default = True):
    @sp.add_test(name = config.name, is_default = is_default)
    def test():
        scenario = sp.test_scenario()
        scenario.h1("FA2 Contract Name: " + config.name)
        scenario.table_of_contents()
        # sp.test_account generates ED25519 key-pairs deterministically:
        admin = sp.test_account("Administrator")
        master_minter = sp.test_account("MasterMinter")
        owner = sp.test_account("Owner")
        alice = sp.test_account("Alice")
        bob   = sp.test_account("Robert")
        carl   = sp.test_account("Carl")
        new_token_minter = sp.test_account("NewTokenMinter")
        rights_manager = sp.test_account("RightsManager")
        fees_manager = sp.test_account("FeesManager")
        fees_faucet = sp.test_account("FeesFaucet")
        relay = sp.test_account("Relay")
        # Let's display the accounts:
        scenario.h2("Accounts")
        scenario.show([admin, alice, bob, carl, fees_faucet])
        c1 = FA2(config = config,
                 metadata = sp.utils.metadata_of_url("ipfs://QmcfGdhb5UtzmikPTmQYNdRnXfBYkJD1UezBgajs18ynUN"),
                 admin = admin.address,
                 fees_faucet = fees_faucet.address,
                 fees_manager = fees_manager.address,
                 master_minter = master_minter.address,
                 owner = owner.address)
        scenario += c1
        scenario.h2("Initial Minting")
        scenario.p("The administrator tries to mint 100 tokens to Alice.")
        c1.mint(address = alice.address,
                            amount = 100,
                            token_id = 0).run(sender = admin, valid = False)
        scenario.p("The master minter mints 100 tokens to Alice.")
        c1.mint(address = alice.address,
                            amount = 100,
                            token_id = 0).run(sender = master_minter)
        c1.mint(address = alice.address,
                            amount = 50,
                            token_id = 0).run(sender = master_minter)
        scenario.p("The master minter burns 50 tokens to Alice.")
        c1.burn(address = alice.address,
                            amount = 50,
                            token_id = 0).run(sender = master_minter)
        scenario.h2("Transfers Alice -> Bob")
        c1.transfer(
            [
                c1.batch_transfer.item(from_ = alice.address,
                                    txs = [
                                        sp.record(to_ = bob.address,
                                                  amount = 5,
                                                  token_id = 0),
                                        sp.record(to_ = bob.address,
                                                  amount = 1,
                                                  token_id = 0),
                                        sp.record(to_ = bob.address,
                                                  amount = 2,
                                                  token_id = 0),
                                    ]),
                c1.batch_transfer.item(from_ = alice.address,
                                    txs = [
                                        sp.record(to_ = bob.address,
                                                  amount = 0,
                                                  token_id = 0),
                                        sp.record(to_ = bob.address,
                                                  amount = 1,
                                                  token_id = 0),
                                        sp.record(to_ = bob.address,
                                                  amount = 1,
                                                  token_id = 0),
                                    ])
            ]).run(sender = alice)
        c1.process_transfer(amount = 5, f = alice.address, fees = 0, relay = alice.address, self_support = False, t = bob.address, token_id = 0).run(sender = fees_manager)
        c1.process_transfer(amount = 1, f = alice.address, fees = 0, relay = alice.address, self_support = False, t = bob.address, token_id = 0).run(sender = fees_manager)
        c1.process_transfer(amount = 2, f = alice.address, fees = 0, relay = alice.address, self_support = False, t = bob.address, token_id = 0).run(sender = fees_manager)
        c1.process_transfer(amount = 1, f = alice.address, fees = 0, relay = alice.address, self_support = False, t = bob.address, token_id = 0).run(sender = fees_manager)
        c1.process_transfer(amount = 1, f = alice.address, fees = 0, relay = alice.address, self_support = False, t = bob.address, token_id = 0).run(sender = fees_manager)
        scenario.verify(
            c1.data.ledger[c1.ledger_key.make(alice.address, 0)].balance == 90)
        scenario.verify(
            c1.data.ledger[c1.ledger_key.make(bob.address, 0)].balance == 10)
        c1.transfer(
            [
                c1.batch_transfer.item(from_ = alice.address,
                                    txs = [
                                        sp.record(to_ = bob.address,
                                                  amount = 10,
                                                  token_id = 0)
                                    ])
            ]).run(sender = alice)
        c1.process_transfer(amount = 10, f = alice.address, fees = 0, relay = alice.address, self_support = False, t = bob.address, token_id = 0).run(sender = fees_manager)
        c1.transfer(
            [
                c1.batch_transfer.item(from_ = alice.address,
                                    txs = [
                                        sp.record(to_ = bob.address,
                                                  amount = 11,
                                                  token_id = 0)
                                    ])
            ]).run(sender = alice)
        c1.process_transfer(amount = 11, f = alice.address, fees = 0, relay = alice.address, self_support = False, t = bob.address, token_id = 0).run(sender = fees_manager)
        scenario.verify(
            c1.data.ledger[c1.ledger_key.make(alice.address, 0)].balance == 90 - 10 - 11
        )
        scenario.verify(
            c1.data.ledger[c1.ledger_key.make(bob.address, 0)].balance
            == 10 + 10 + 11)
        
        scenario.h2("Try to mint another token")
        c1.mint(address = bob.address,
                            amount = 100,
                            token_id = 1).run(sender = admin, valid = False)
        
        scenario.h2("Other Basic Permission Tests")
        scenario.h3("Bob cannot transfer Alice's tokens.")
        c1.transfer(
            [
                c1.batch_transfer.item(from_ = alice.address,
                                    txs = [
                                        sp.record(to_ = bob.address,
                                                  amount = 10,
                                                  token_id = 0)])
            ]).run(sender = bob, valid = False)
        scenario.h3("Admin can transfer anything.")
        c1.transfer(
            [
                c1.batch_transfer.item(from_ = alice.address,
                                    txs = [
                                        sp.record(to_ = bob.address,
                                                  amount = 5,
                                                  token_id = 0)])
            ]).run(sender = admin)
        c1.process_transfer(amount = 5, f = alice.address, fees = 0, relay = admin.address, self_support = False, t = bob.address, token_id = 0).run(sender = fees_manager, valid = False)
        c1.transfer(
            [
                c1.batch_transfer.item(from_ = alice.address,
                                    txs = [
                                        sp.record(to_ = bob.address,
                                                  amount = 5,
                                                  token_id = 0)])
            ]).run(sender = admin)
        c1.transfer(
            [
                c1.batch_transfer.item(from_ = bob.address,
                                    txs = [
                                        sp.record(to_ = alice.address,
                                                  amount = 11,
                                                  token_id = 0)])
            ]).run(sender = admin)
        scenario.h3("Even Admin cannot transfer too much.")
        c1.transfer(
            [
                c1.batch_transfer.item(from_ = alice.address,
                                    txs = [
                                        sp.record(to_ = bob.address,
                                                  amount = 1000,
                                                  token_id = 0)])
            ]).run(sender = admin, valid = False)
        scenario.h3("Consumer Contract for Callback Calls.")
        consumer = View_consumer(c1)
        scenario += consumer
        scenario.p("Consumer virtual address: "
                   + consumer.address.export())
        scenario.h2("Balance-of.")
        def arguments_for_balance_of(receiver, reqs):
            return (sp.record(
                callback = sp.contract(
                    Balance_of.response_type(),
                    receiver.address,
                    entry_point = "receive_balances").open_some(),
                requests = reqs))
        c1.balance_of(arguments_for_balance_of(consumer, [
            sp.record(owner = alice.address, token_id = 0)
        ]))
        scenario.verify(consumer.data.last_sum == 70)
        scenario.h2("Operators")
        if not c1.config.support_operator:
            scenario.h3("This version was compiled with no operator support")
            scenario.p("Calls should fail even for the administrator:")
            c1.update_operators([]).run(sender = admin, valid = False)
        else:
            scenario.p("This version was compiled with operator support.")
            scenario.p("Calling 0 updates should work:")
            c1.update_operators([]).run()
            scenario.h3("Operator Accounts")
            op0 = sp.test_account("Operator0")
            op1 = sp.test_account("Operator1")
            op2 = sp.test_account("Operator2")
            scenario.show([op0, op1, op2])
            scenario.p("Admin can change Alice's operator.")
            c1.update_operators([
                sp.variant("add_operator", c1.operator_param.make(
                    owner = alice.address,
                    operator = op1.address,
                    token_id = 0))
            ]).run(sender = admin)
            scenario.p("Operator1 can now transfer Alice's tokens 0 and 2")
            c1.transfer(
                [
                    c1.batch_transfer.item(from_ = alice.address,
                                        txs = [
                                            sp.record(to_ = bob.address,
                                                      amount = 2,
                                                      token_id = 0)])
                ]).run(sender = op1)
            c1.process_transfer(amount = 2, f = alice.address, fees = 0, relay = op1.address, self_support = False, t = bob.address, token_id = 0).run(sender = fees_manager)
            scenario.p("Alice can remove their operator")
            c1.update_operators([
                sp.variant("remove_operator", c1.operator_param.make(
                    owner = alice.address,
                    operator = op1.address,
                    token_id = 0)),
                sp.variant("remove_operator", c1.operator_param.make(
                    owner = alice.address,
                    operator = op1.address,
                    token_id = 0))
            ]).run(sender = alice)
            scenario.p("Operator1 cannot transfer Alice's tokens any more")
            c1.transfer(
                [
                    c1.batch_transfer.item(from_ = alice.address,
                                        txs = [
                                            sp.record(to_ = op1.address,
                                                      amount = 2,
                                                      token_id = 0)])
                ]).run(sender = op1, valid = False)
            scenario.p("Bob can add Operator0.")
            c1.update_operators([
                sp.variant("add_operator", c1.operator_param.make(
                    owner = bob.address,
                    operator = op0.address,
                    token_id = 0))
            ]).run(sender = bob)
            scenario.p("Operator0 can transfer Bob's tokens '0'")
            c1.transfer(
                [
                    c1.batch_transfer.item(from_ = bob.address,
                                        txs = [
                                            sp.record(to_ = alice.address,
                                                      amount = 1,
                                                      token_id = 0)])
                ]).run(sender = op0)
            c1.process_transfer(amount = 1, f = bob.address, fees = 0, relay = op0.address, self_support = False, t = alice.address, token_id = 0).run(sender = fees_manager)
            scenario.p("Bob cannot add Operator0 for Alice's tokens.")
            c1.update_operators([
                sp.variant("add_operator", c1.operator_param.make(
                    owner = alice.address,
                    operator = op0.address,
                    token_id = 0
                ))
            ]).run(sender = bob, valid = False)
            scenario.p("Alice can also add Operator0 for their tokens 0.")
            c1.update_operators([
                sp.variant("add_operator", c1.operator_param.make(
                    owner = alice.address,
                    operator = op0.address,
                    token_id = 0
                ))
            ]).run(sender = alice, valid = True)
            scenario.p("Operator0 can now transfer Bob's and Alice's 0-tokens.")
            c1.transfer(
                [
                    c1.batch_transfer.item(from_ = bob.address,
                                        txs = [
                                            sp.record(to_ = alice.address,
                                                      amount = 1,
                                                      token_id = 0)])
                ]).run(sender = op0)
            c1.process_transfer(amount = 1, f = bob.address, fees = 0, relay = op0.address, self_support = False, t = alice.address, token_id = 0).run(sender = fees_manager)
            c1.transfer(
                [
                    c1.batch_transfer.item(from_ = alice.address,
                                        txs = [
                                            sp.record(to_ = bob.address,
                                                      amount = 1,
                                                      token_id = 0)])
                ]).run(sender = op0)
            c1.process_transfer(amount = 1, f = alice.address, fees = 0, relay = op0.address, self_support = False, t = bob.address, token_id = 0).run(sender = fees_manager)
            scenario.p("Bob adds Operator2 as second operator for 0-tokens.")
            c1.update_operators([
                sp.variant("add_operator", c1.operator_param.make(
                    owner = bob.address,
                    operator = op2.address,
                    token_id = 0
                ))
            ]).run(sender = bob, valid = True)
            scenario.p("Operator0 and Operator2 can transfer Bob's 0-tokens.")
            c1.transfer(
                [
                    c1.batch_transfer.item(from_ = bob.address,
                                        txs = [
                                            sp.record(to_ = alice.address,
                                                      amount = 1,
                                                      token_id = 0)])
                ]).run(sender = op0)
            c1.process_transfer(amount = 1, f = bob.address, fees = 0, relay = op0.address, self_support = False, t = alice.address, token_id = 0).run(sender = fees_manager)
            c1.transfer(
                [
                    c1.batch_transfer.item(from_ = bob.address,
                                        txs = [
                                            sp.record(to_ = alice.address,
                                                      amount = 1,
                                                      token_id = 0)])
                ]).run(sender = op2)
            c1.process_transfer(amount = 1, f = bob.address, fees = 0, relay = op2.address, self_support = False, t = alice.address, token_id = 0).run(sender = fees_manager)
            
            scenario.h2("Gasless transfers")
            tx_type = sp.TRecord(to_ = sp.TAddress,
                             token_id = token_id_type,
                             amount = sp.TNat).layout(
                ("to_", ("token_id", "amount")))
            payload_type = sp.TRecord(from_ = sp.TKey,
                                                txs = sp.TList(tx_type), sig = sp.TSignature).layout(
                                                ("from_", ("txs", "sig")))
            scenario.p("Relay tries to broadcast feelees transfer for Alice to Bob with wrong signature")
            txs = sp.list([sp.record(to_ = bob.address, token_id = 0, amount = 10)], t = tx_type)
            b = sp.pack(sp.pair(c1.address, sp.pair(0, sp.pair(alice.address, txs))))
            sig = sp.make_signature(
                    bob.secret_key, 
                    b, 
                    message_format = 'Raw')
            c1.transfer_gasless(
                transfers = sp.list([sp.record(from_ = alice.public_key, txs = txs, sig = sig)], t = payload_type),
                support_fees = False, flat_fees = True
            ).run(sender = relay, valid = False)

            scenario.p("Relay broadcasts feelees transfer for Alice to Bob")
            sig = sp.make_signature(
                    alice.secret_key, 
                    b, 
                    message_format = 'Raw')
            c1.transfer_gasless(
                transfers = sp.list([sp.record(from_ = alice.public_key, txs = txs, sig = sig)], t = payload_type),
                support_fees = False, flat_fees = True
            ).run(sender = relay)
            c1.process_transfer(amount = 10, f = alice.address, fees = 1, relay = relay.address, self_support = False, t = bob.address, token_id = 0).run(sender = fees_manager)
            
            scenario.p("Relay tries to broadcast same transaction i.e. replay attack")
            c1.transfer_gasless(
                transfers = sp.list([sp.record(from_ = alice.public_key, txs = txs, sig = sig)], t = payload_type),
                support_fees = False, flat_fees = True
            ).run(sender = relay, valid = False)

            scenario.p("Relay tries to broadcast feelees transfer for Bob to Alice while supporting fees")
            txs = sp.list([sp.record(to_ = alice.address, token_id = 0, amount = 10), sp.record(to_ = carl.address, token_id = 0, amount = 2)], t = tx_type)
            b = sp.pack(sp.pair(c1.address, sp.pair(0, sp.pair(bob.address, txs))))
            sig = sp.make_signature(
                    bob.secret_key, 
                    b, 
                    message_format = 'Raw')
            c1.transfer_gasless(
                transfers = sp.list([sp.record(from_ = bob.public_key, txs = txs, sig = sig)], t = payload_type),
                support_fees = True, flat_fees = False
            ).run(sender = relay)
            c1.process_transfer(amount = 10, f = bob.address, fees = 1, relay = relay.address, self_support = True, t = alice.address, token_id = 0).run(sender = fees_manager, valid = False)
            
            scenario.p("Master minter mints 10 tokens to Relay")
            c1.mint(address = relay.address,
                            amount = 10,
                            token_id = 0).run(sender = master_minter)

            scenario.p("Relay broadcasts feelees transfer for Bob to Alice while supporting fees")
            b = sp.pack(sp.pair(c1.address, sp.pair(1, sp.pair(bob.address, txs))))
            sig = sp.make_signature(
                    bob.secret_key, 
                    b, 
                    message_format = 'Raw')
            c1.transfer_gasless(
                transfers = sp.list([sp.record(from_ = bob.public_key, txs = txs, sig = sig)], t = payload_type),
                support_fees = True, flat_fees = False
            ).run(sender = relay)
            c1.process_transfer(amount = 10, f = bob.address, fees = 1, relay = relay.address, self_support = True, t = alice.address, token_id = 0).run(sender = fees_manager)
            c1.process_transfer(amount = 2, f = bob.address, fees = 1, relay = relay.address, self_support = True, t = carl.address, token_id = 0).run(sender = fees_manager)

            scenario.h2("Permits")
            scenario.p("Relay sets a permit for a transfer of 10 from Alice to Bob")
            tx_params = sp.pack(sp.pair(10, sp.pair(alice.address, sp.pair(bob.address, 0))))
            params_hash = sp.blake2b(tx_params)
            b = sp.pack(sp.pair(sp.pair(sp.chain_id_cst("0x9caecab9"), c1.address), sp.pair(1, params_hash)))
            s = sp.make_signature(alice.secret_key, b, message_format = 'Raw')
            c1.permit(sp.pair(alice.public_key, sp.pair(s, params_hash))).run(sender = relay, now = sp.timestamp(1637085600), chain_id=sp.chain_id_cst("0x9caecab9"))
            
            scenario.p("Relay consumes permit")
            c1.transfer(
            [
                c1.batch_transfer.item(from_ = alice.address,
                                    txs = [
                                        sp.record(to_ = bob.address,
                                                  amount = 10,
                                                  token_id = 0)
                                    ])
            ]).run(sender = relay, now = sp.timestamp(1637090700))
            c1.process_transfer(amount = 10, f = alice.address, fees = 0, relay = relay.address, self_support = False, t = bob.address, token_id = 0).run(sender = fees_manager)
            scenario.verify(c1.data.permits.contains(alice.address) & (~c1.data.permits[alice.address].user_permits.contains(params_hash)))

            scenario.p("Relay tries to set a permit for a transfer of 5 from Bob to Alice")
            tx_params = sp.pack(sp.pair(5, sp.pair(bob.address, sp.pair(alice.address, 0))))
            params_hash = sp.blake2b(tx_params)
            b = sp.pack(sp.pair(sp.pair(sp.chain_id_cst("0x9caecab9"), c1.address), sp.pair(3, params_hash)))
            s = sp.make_signature(bob.secret_key, b, message_format = 'Raw')
            c1.permit(sp.pair(bob.public_key, sp.pair(s, params_hash))).run(sender = relay, now = sp.timestamp(1637090700), chain_id=sp.chain_id_cst("0x9caecab9"), valid = False)

            scenario.p("Relay sets a permit for a transfer of 5 from Bob to Alice")
            tx_params = sp.pack(sp.pair(5, sp.pair(bob.address, sp.pair(alice.address, 0))))
            params_hash = sp.blake2b(tx_params)
            b = sp.pack(sp.pair(sp.pair(sp.chain_id_cst("0x9caecab9"), c1.address), sp.pair(2, params_hash)))
            s = sp.make_signature(bob.secret_key, b, message_format = 'Raw')
            c1.permit(sp.pair(bob.public_key, sp.pair(s, params_hash))).run(sender = relay, now = sp.timestamp(1637090700), chain_id=sp.chain_id_cst("0x9caecab9"))
            
            scenario.p("Relay tries to set expirency for Bob's permit")
            c1.set_expiry(seconds = 86400, permit = sp.some(params_hash)).run(sender = relay, now = sp.timestamp(1637090760), chain_id=sp.chain_id_cst("0x9caecab9"), valid = False)
            
            scenario.p("Bob sets expirency for his permit")
            c1.set_expiry(seconds = 86400, permit = sp.some(params_hash)).run(sender = bob, now = sp.timestamp(1637090800))

            scenario.p("Relay tries to consume permit (expiry over due)")
            c1.transfer(
            [
                c1.batch_transfer.item(from_ = bob.address,
                                    txs = [
                                        sp.record(to_ = alice.address,
                                                  amount = 5,
                                                  token_id = 0)
                                    ])
            ]).run(sender = relay, now = sp.timestamp(1637177201), valid = False)

            scenario.p("Bob removes his permit")
            c1.set_expiry(seconds = 0, permit = sp.some(params_hash)).run(sender = bob)

            scenario.p("Relay sets a permit for a transfer of 5 from Bob to Alice")
            tx_params = sp.pack(sp.pair(5, sp.pair(bob.address, sp.pair(alice.address, 0))))
            params_hash = sp.blake2b(tx_params)
            b = sp.pack(sp.pair(sp.pair(sp.chain_id_cst("0x9caecab9"), c1.address), sp.pair(3, params_hash)))
            s = sp.make_signature(bob.secret_key, b, message_format = 'Raw')
            c1.permit(sp.pair(bob.public_key, sp.pair(s, params_hash))).run(sender = relay, now = sp.timestamp(1637090700), chain_id=sp.chain_id_cst("0x9caecab9"))

            scenario.p("Bob sets expirency for his permit")
            c1.set_expiry(seconds = 86400, permit = sp.some(params_hash)).run(sender = bob, now = sp.timestamp(1637090800))

            scenario.p("Relay consumes permit")
            c1.transfer(
            [
                c1.batch_transfer.item(from_ = bob.address,
                                    txs = [
                                        sp.record(to_ = alice.address,
                                                  amount = 5,
                                                  token_id = 0)
                                    ])
            ]).run(sender = relay, now = sp.timestamp(1637123900))
            c1.process_transfer(amount = 5, f = bob.address, fees = 0, relay = relay.address, self_support = False, t = alice.address, token_id = 0).run(sender = fees_manager)
            scenario.verify(c1.data.permits.contains(bob.address) & (~c1.data.permits[bob.address].user_permits.contains(params_hash)))
            
            scenario.h2("Create new token")
            scenario.p("Admin tries to create a new token reference")
            new_token_meta_data = sp.map(l = {
                "decimals" : sp.utils.bytes_of_string("%d" % 6),
                "is_boolean_amount" : sp.utils.bytes_of_string("%r" % False),
                "is_transferable" : sp.utils.bytes_of_string("%r" % True),
                "name" : sp.utils.bytes_of_string("Some other euro coin"),
                "should_prefer_symbol": sp.utils.bytes_of_string("%r" % True),
                "symbol" : sp.utils.bytes_of_string("MEUR"),
                "thumbnailUri" : sp.utils.bytes_of_string("ipfs://QmcqsYQn8pTxQr3P1dYpgYxQa6GQPmoBTSWQ8bpuFEuaqe")
            })
            c1.create(token_id = 1, meta_data = new_token_meta_data).run(sender = admin, valid = False)
            scenario.p("Owner tries to create a new token reference with same token id")
            c1.create(token_id = 0, meta_data = new_token_meta_data).run(sender = owner, valid = False)
            scenario.p("Owner tries to create a new token reference with non consecutive token id")
            c1.create(token_id = 2, meta_data = new_token_meta_data).run(sender = owner, valid = False)
            scenario.p("Owner creates a new token reference")
            c1.create(token_id = 1, meta_data = new_token_meta_data).run(sender = owner)

            scenario.h2("Add new minter on token id 1")
            scenario.p("The master minter adds new token minter.")
            c1.add_minter(address = new_token_minter.address,
                            allowance = 110,
                            token_id = 1).run(sender = master_minter)
            scenario.p("New token minter tries to mints 100 token 0")
            c1.mint(address = alice.address,
                            amount = 100,
                            token_id = 0).run(sender = new_token_minter, valid = False)
            scenario.p("New token minter mints 100 tokens")
            c1.mint(address = alice.address,
                            amount = 100,
                            token_id = 1).run(sender = new_token_minter)
            scenario.p("New token minter tries to mint 20 tokens")
            c1.mint(address = alice.address,
                            amount = 20,
                            token_id = 1).run(sender = new_token_minter, valid = False)
            scenario.p("New token minter tries to update alowance of 20 tokens for new token minter")
            c1.update_allowance(address = new_token_minter.address,
                            allowance = 20).run(sender = new_token_minter, valid = False)
            scenario.p("Master minter updates alowance of 20 tokens for new token minter")
            c1.update_allowance(address = new_token_minter.address,
                            allowance = 20).run(sender = master_minter)
            scenario.p("New token minter mints 20 tokens")
            c1.mint(address = alice.address,
                            amount = 20,
                            token_id = 1).run(sender = new_token_minter)
            scenario.p("Master minter mints 100 tokens")
            c1.mint(address = alice.address,
                            amount = 100,
                            token_id = 1).run(sender = master_minter)
            scenario.p("New token minter tries to overburn 130 tokens")
            c1.burn(address = alice.address,
                            amount = 130,
                            token_id = 1).run(sender = new_token_minter, valid = False)
            scenario.p("New token minter burns 120 tokens")
            c1.burn(address = alice.address,
                            amount = 120,
                            token_id = 1).run(sender = new_token_minter)
                
            scenario.table_of_contents()

def global_parameter(env_var, default):
    try:
        if os.environ[env_var] == "true" :
            return True
        if os.environ[env_var] == "false" :
            return False
        return default
    except:
        return default

def environment_config():
    return FA2_config(
        debug_mode = global_parameter("debug_mode", False),
        single_asset = global_parameter("single_asset", False),
        non_fungible = global_parameter("non_fungible", False),
        add_mutez_transfer = global_parameter("add_mutez_transfer", True),
        readable = global_parameter("readable", True),
        force_layouts = global_parameter("force_layouts", True),
        support_operator = global_parameter("support_operator", True),
        assume_consecutive_token_ids =
            global_parameter("assume_consecutive_token_ids", True),
        store_total_supply = global_parameter("store_total_supply", False),
        lazy_entry_points = global_parameter("lazy_entry_points", False),
        allow_self_transfer = global_parameter("allow_self_transfer", False),
        use_token_metadata_offchain_view = global_parameter("use_token_metadata_offchain_view", True),
    )

if "templates" not in __name__:
    add_test(environment_config())
