import smartpy as sp

# A collection of error messages used in the contract.
class Reserve_Error:
    def make(s): return ("RESERVE_" + s)

    unreferencedSigner              = make("UnreferencedSigner")
    alreadyReferenced               = make("ProposalIdAlreadyReferenced")
    unreferencedProposal            = make("ProposalIdUnreferenced")
    openLimit                       = make("OpenProposalsLimitReached")
    proposalClosed                  = make("ProposalAlreadyClosed")
    proposalAccepted                = make("ProposalAlreadyAccepted")

class Reserve(sp.Contract):
    def __init__(self, signers):
        self.init_type(t = sp.TRecord(signers = sp.TSet(sp.TAddress), proposals = sp.TBigMap(sp.TString, sp.TRecord(f = sp.TAddress, 
            amount = sp.TNat, t = sp.TAddress, contractAddr = sp.TAddress, approvals = sp.TSet(sp.TAddress), 
            rejects = sp.TSet(sp.TAddress), tokenId = sp.TNat, status = sp.TBool)), limit =  sp.TNat, 
            restriction =  sp.TNat, openProposals = sp.TNat))
        self.init(signers = signers, proposals = sp.big_map(), limit = 2, restriction = 20, openProposals = 0)
        
    '''
    Verification utils
    '''
    
    def checkSigner(self):
        sp.verify(self.data.signers.contains(sp.sender), message = Reserve_Error.unreferencedSigner)
    
    def checkNewProposal(self, proposalId):
        sp.verify(~self.data.proposals.contains(proposalId), message = Reserve_Error.alreadyReferenced)
    
    def checkProposal(self, proposalId):
        sp.verify(self.data.proposals.contains(proposalId), message = Reserve_Error.unreferencedProposal)
    
    def checkRestriction(self):
        sp.verify(self.data.restriction > self.data.openProposals, message = Reserve_Error.openLimit)
        
    def checkOpen(self, proposalId):
        sp.verify(~self.data.proposals[proposalId].status, message = Reserve_Error.proposalClosed)

        
    '''
    Main entrypoints
    '''

    @sp.entry_point
    def createProposal(self, params):
        sp.set_type(params, sp.TRecord(amount = sp.TNat, contractAddr = sp.TAddress, f = sp.TAddress, proposalId = sp.TString, 
            t = sp.TAddress, tokenId = sp.TNat))
        self.checkSigner()
        self.checkRestriction()
        self.checkNewProposal(params.proposalId)
        self.data.proposals[params.proposalId] = sp.record(f = params.f, amount = params.amount, t = params.t, 
            contractAddr = params.contractAddr, approvals = sp.set(t = sp.TAddress), rejects = sp.set(t = sp.TAddress), 
            tokenId = params.tokenId, status = False)
        self.data.proposals[params.proposalId].approvals.add(sp.sender)
        self.data.openProposals += 1

    @sp.entry_point
    def accept(self, params):
        sp.set_type(params, sp.TString)
        self.checkSigner()
        self.checkProposal(params)
        self.checkOpen(params)
        sp.verify(~self.data.proposals[params].approvals.contains(sp.sender), message = Reserve_Error.proposalAccepted)
        self.data.proposals[params].approvals.add(sp.sender)
        sp.if sp.len(self.data.proposals[params].approvals) == self.data.limit:
            self._transfer(params)
            self.data.proposals[params].status = True
            self.data.openProposals = sp.as_nat(self.data.openProposals - 1)

    @sp.entry_point
    def reject(self, params):
        sp.set_type(params, sp.TString)
        self.checkSigner()
        self.checkProposal(params)
        self.checkOpen(params)
        self.data.proposals[params].rejects.add(sp.sender)
        sp.if self.data.proposals[params].approvals.contains(sp.sender):
            self.data.proposals[params].approvals.remove(sp.sender)
        self.data.proposals[params].status = True
        self.data.openProposals = sp.as_nat(self.data.openProposals - 1)
        
    '''
    Lugh coin transfer
    '''
    
    def _transfer(self, proposalId):
        tx_type = sp.TRecord(amount = sp.TNat,
                             to_ = sp.TAddress,
                             token_id = sp.TNat)
        tx_type = tx_type.layout(
                ("to_", ("token_id", "amount"))
            )
        transfer_type = sp.TRecord(from_ = sp.TAddress,
                                   txs = sp.TList(tx_type))
        transfers_type = sp.TRecord(item = sp.TList(transfer_type))
        transferParamsRecord = sp.record(item = [sp.record(
                                            from_ = self.data.proposals[proposalId].f, 
                                            txs = [sp.record(
                                                              amount = self.data.proposals[proposalId].amount, 
                                                              to_ = self.data.proposals[proposalId].t, 
                                                              token_id = self.data.proposals[proposalId].tokenId
                                                  )]
                                         )])
        c = sp.contract(
                t = transfers_type, 
                address = self.data.proposals[proposalId].contractAddr, 
                entry_point = "transfer"
                ).open_some()
        sp.transfer(transferParamsRecord, sp.mutez(0), c)

if "templates" not in __name__:
    @sp.add_test(name = "Reserve")
    def test():

        scenario = sp.test_scenario()
        scenario.h1("Reserve Contract")

        # sp.test_account generates ED25519 key-pairs deterministically:
        firstSigner = sp.test_account("firstSigner")
        secondSigner = sp.test_account("secondSigner")
        thirdSigner = sp.test_account("thirdSigner")
        reserve = sp.test_account("Reserve")
        alice = sp.test_account("Alice")
        bob = sp.test_account("Bob")
        contractAddr = sp.address("KT1-distantContractToCall-1234")

        # Let's display the accounts:
        scenario.h2("Accounts")
        scenario.show([firstSigner, secondSigner, thirdSigner, reserve, alice, bob])

        c1 = Reserve(signers = sp.set([firstSigner.address, secondSigner.address, thirdSigner.address]))

        scenario += c1
        
        scenario.h2("#r01 - firstSigner creates a proposal")
        scenario += c1.createProposal(proposalId = "rt01", f = reserve.address, t = alice.address, amount = 20000, contractAddr = contractAddr, tokenId = 0).run(sender = firstSigner)
        scenario.h2("#r02 - firstSigner accepts proposal")
        scenario += c1.accept("rt01").run(sender = firstSigner, valid=False)
        scenario.h2("#r03 - secondSigner rejects proposal")
        scenario += c1.reject("rt01").run(sender = secondSigner)
        scenario.h2("#r04 - thirdSigner accepts proposal")
        scenario += c1.accept("rt01").run(sender = thirdSigner, valid=False)
        scenario.h2("#r05 - thirdSigner tries to create a proposal")
        scenario += c1.createProposal(proposalId = "rt01", f = reserve.address, t = alice.address, amount = 10000, contractAddr = contractAddr, tokenId = 0).run(sender = thirdSigner, valid=False)
        scenario.h2("#r06 - thirdSigner creates a proposal")
        scenario += c1.createProposal(proposalId = "rt02", f = reserve.address, t = alice.address, amount = 10000, contractAddr = contractAddr, tokenId = 0).run(sender = thirdSigner)
        scenario.h2("#r07 - secondSigner accepts proposal")
        scenario += c1.accept("rt02").run(sender = secondSigner)
        scenario.h2("#r08 - firstSigner accepts proposal")
        scenario += c1.accept("rt02").run(sender = firstSigner, valid=False)
        scenario.h2("#r09 - firstSigner creates a proposal")
        scenario += c1.createProposal(proposalId = "rt03", f = reserve.address, t = bob.address, amount = 10000, contractAddr = contractAddr, tokenId = 0).run(sender = firstSigner)
        scenario.h2("#r10 - secondSigner rejects proposal")
        scenario += c1.reject("rt03").run(sender = secondSigner)
        scenario.h2("#r11 - firstSigner rejects proposal")
        scenario += c1.reject("rt03").run(sender = firstSigner, valid=False)
        scenario.h2("#r12 - firstSigner creates a proposal for over transferring")
        scenario += c1.createProposal(proposalId = "rt04", f = reserve.address, t = bob.address, amount = 900000, contractAddr = contractAddr, tokenId = 0).run(sender = firstSigner)
        scenario.h2("#r13 - secondSigner accepts proposal")
        scenario += c1.accept("rt04").run(sender = secondSigner)
