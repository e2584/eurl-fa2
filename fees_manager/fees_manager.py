import smartpy as sp

class FeesManager(sp.Contract):
    def __init__(self, admin, gasFee, gaslessFee, storageFee, threshold):
        self.init_type(t = sp.TRecord(administrator = sp.TAddress, gasFee = sp.TNat, gaslessFee = sp.TNat, storageFee = sp.TNat, threshold = sp.TNat))
        self.scale = 10000
        self.init(administrator = admin, gasFee = gasFee, gaslessFee = gaslessFee, storageFee = storageFee, threshold = threshold)  

    '''
    Internal functions
    '''
        
    def notAdmin(self):
        sp.verify((sp.sender == self.data.administrator), message = "NotAdmin")  

    def _callback(self, params):
        c = sp.contract(
            t = sp.TRecord(amount = sp.TNat, f = sp.TAddress, fees = sp.TNat, relay = sp.TAddress, 
                           self_support = sp.TBool, t = sp.TAddress, token_id = sp.TNat), 
            address = sp.sender, 
            entry_point = "process_transfer"
            ).open_some()
        sp.transfer(params, sp.mutez(0), c)

    '''
    Main entrypoints
    '''

    @sp.entry_point
    def updateThreshold(self, params):
        self.notAdmin()
        sp.set_type(params, sp.TRecord(tokenId = sp.TNat, value = sp.TNat))    
        self.data.threshold = params.value

    @sp.entry_point
    def updateStorageFee(self, params):
        self.notAdmin()
        sp.set_type(params, sp.TRecord(tokenId = sp.TNat, value = sp.TNat))    
        self.data.storageFee = params.value

    @sp.entry_point
    def updateGasFee(self, params):
        self.notAdmin()
        sp.set_type(params, sp.TRecord(tokenId = sp.TNat, value = sp.TNat))    
        self.data.gasFee = params.value

    @sp.entry_point
    def updateGaslessFee(self, params):
        self.notAdmin()
        sp.set_type(params, sp.TRecord(tokenId = sp.TNat, value = sp.TNat))    
        self.data.gaslessFee = params.value

    @sp.entry_point
    def calculateFees(self, params):
        sp.set_type(params, sp.TRecord(amount = sp.TNat, f = sp.TAddress, flat =sp.TBool, storage = sp.TBool, 
                                   self_support = sp.TBool, t = sp.TAddress, relay = sp.TAddress, 
                                   token_id = sp.TNat, xtz = sp.TBool))
        sp.if params.xtz:
            feesParams = sp.record(amount = params.amount, f = params.f, fees = params.amount * self.data.gasFee / self.scale, 
                                   relay = params.relay, self_support = params.self_support, 
                                   t = params.t, token_id = params.token_id)
            self._callback(feesParams)
        sp.else:
            sp.if params.flat:
                sp.if params.storage:
                    sp.if ((params.amount * self.data.gaslessFee / self.scale) > (self.data.threshold + self.data.storageFee)):
                        feesParams = sp.record(amount = params.amount, f = params.f, fees = params.amount * self.data.gaslessFee / self.scale, 
                                               relay = params.relay, self_support = params.self_support, 
                                               t = params.t, token_id = params.token_id)
                        self._callback(feesParams)
                    sp.else:
                        feesParams = sp.record(amount = params.amount, f = params.f, fees = (self.data.threshold + self.data.storageFee),
                                              relay = params.relay, self_support = params.self_support, 
                                              t = params.t, token_id = params.token_id)
                        self._callback(feesParams)
                sp.else:
                    sp.if ((params.amount * self.data.gaslessFee / self.scale) > self.data.threshold):
                        feesParams = sp.record(amount = params.amount, f = params.f, fees = params.amount * self.data.gaslessFee / self.scale, 
                                               relay = params.relay, self_support = params.self_support, 
                                               t = params.t, token_id = params.token_id)
                        self._callback(feesParams)
                    sp.else:
                        feesParams = sp.record(amount = params.amount, f = params.f, fees = self.data.threshold, 
                                   relay = params.relay, self_support = params.self_support, 
                                   t = params.t, token_id = params.token_id)
                        self._callback(feesParams)
            sp.else:
                feesParams = sp.record(amount = params.amount, f = params.f, fees = params.amount * self.data.gaslessFee / self.scale, 
                                       relay = params.relay, self_support = params.self_support, 
                                       t = params.t, token_id = params.token_id)
                self._callback(feesParams)

if "templates" not in __name__:
    @sp.add_test(name = "FeesManager")
    def test():

        scenario = sp.test_scenario()
        scenario.h1("FeesManager Contract")

        # sp.test_account generates ED25519 key-pairs
        admin = sp.test_account("Admin")
        alice = sp.test_account("Alice")
        bob   = sp.test_account("Bob")
        carl   = sp.test_account("Carl")
        relay   = sp.test_account("Relay")
        lugh_contract = sp.test_account("lughContract")

        scenario.h2("Accounts")
        scenario.show([alice, bob, carl, relay, admin])

        c1 = FeesManager(admin = admin.address, gasFee = 2, gaslessFee = 0, storageFee = 140000, threshold = 30000)

        scenario += c1
        scenario.h2("#case01 - Calculate fees for Gas based transfer")
        scenario += c1.calculateFees(amount = 19000000000, f = alice.address, t = bob.address, flat = True, relay = alice.address, self_support = False, storage = True, token_id = 0, xtz = True).run(sender = lugh_contract)
        scenario.h2("#case02 - Calculate fees for Gasless transfer")
        scenario += c1.calculateFees(amount = 180000000, f = alice.address, t = bob.address, flat = True, relay = relay.address, self_support = False, storage = True, token_id = 0, xtz = False).run(sender = lugh_contract)
        scenario.h2("#case03 - Calculate fees for Gasless transfer")
        scenario += c1.calculateFees(amount = 5500000000, f = alice.address, t = bob.address, flat = True, relay = relay.address, self_support = False, storage = False, token_id = 0, xtz = False).run(sender = lugh_contract)
        scenario.h2("#case04 - Calculate fees for Gasless transfer")
        scenario += c1.calculateFees(amount = 1200000, f = alice.address, t = bob.address, flat = False, relay = relay.address, self_support = True, storage = False, token_id = 0, xtz = False).run(sender = lugh_contract)
        scenario.h2("#case05 - Bob tries to update Fee")
        scenario += c1.updateGaslessFee(tokenId = 0, value = 8).run(sender = bob, valid = False)
        scenario.h2("#case06 - Admin updates Fee")
        scenario += c1.updateGaslessFee(tokenId = 0, value = 20).run(sender = admin)
        scenario.h2("#case07 - Admin updates Threshold")
        scenario += c1.updateThreshold(tokenId = 0, value = 4000).run(sender = admin)
        scenario.h2("#case08 - Calculate fees for Gasless transfer")
        scenario += c1.calculateFees(amount = 180000000, f = alice.address, t = bob.address, flat = False, relay = relay.address, self_support = True, storage = False, token_id = 0, xtz = False).run(sender = lugh_contract)
