# EURL FA2

EURL FA2 is a Smartpy integration of Lugh Euro stablecoin.

<img src="figures/g_arch_eurl.png" width="400">

## Key features

### Gasless

EURL FA2 introduces a new entrypoint i.e. **transfer_gasless**. It enables a relay node e.g. [Lugh](https://www.lugh.io/), to pay XTZ fees for a sender and apply fees in EURL if needed (see Fees manager). Accordingly utils such as *process_sig*, *process_fees* are defined to support gasless operations validation and security.

### Permits

EURL FA2 integrates the proposal [TZIP-017](https://tzip.tezosagora.org/proposal/tzip-17/) i.e. a standard for a pre-sign or "Permit" interface: a lightweight on-chain emulation of tzn accounts.

### Minting agents support

Token minting can now be delegatted to third parties accounts, by the administrator.

### Fees manager

A fees management contract is introduced, to dettach fees calculation from main token contract. Thus, fees rules can easily changed and swapped.

